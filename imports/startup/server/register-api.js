import { createApolloServer } from "meteor/apollo";
import { makeExecutableSchema } from "graphql-tools";
import merge from 'lodash/merge';
import DealSchema from '../../api/deals/Deals.graphql';
import DealsResolvers from '../../api/deals/resolvers';
import DealTypeSchema from '../../api/DealType/DealType.graphql';
import DealTypeResolvers from '../../api/DealType/resolvers';
import UsersSchema from '../../api/users/User.graphql';
import UsersResolvers from '../../api/users/resolvers';
import StoresSchema from '../../api/stores/Stores.graphql';
import StoresResolvers from '../../api/stores/resolvers';
import CuisineListSchema from '../../api/CuisineList/CuisineList.graphql'
import CuisineListResolvers from '../../api/CuisineList/resolvers';
import FavoritesSchema from '../../api/Favorites/Favorites.graphql'
import FavoritesResolvers from '../../api/Favorites/resolvers';
import WelcomeSchema from '../../api/WelcomeSection/WelcomeSection.graphql';
import WelcomeResolvers from '../../api/WelcomeSection/resolvers';
import FAQSchema from '../../api/FAQ/FAQ.graphql';
import FAQResolvers from '../../api/FAQ/resolvers';
import AboutSchema from '../../api/AboutPage/About.graphql';
import AboutResolvers from '../../api/AboutPage/resolvers';
import { WebApp } from 'meteor/webapp';
// dgjddsdddddddsacdxddsdsdsgdkgjdgdddsdddfddgddsdhg

const typeDefs = [
    DealSchema,
    UsersSchema,
    StoresSchema,
    CuisineListSchema,
    DealTypeSchema,
    FavoritesSchema,
    WelcomeSchema,
    FAQSchema,
    AboutSchema
];

const resolvers = merge( 
    DealsResolvers,
    UsersResolvers,
    StoresResolvers,
    CuisineListResolvers,
    DealTypeResolvers,
    FavoritesResolvers,
    WelcomeResolvers,
    FAQResolvers,
    AboutResolvers
);

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

createApolloServer({ 
    schema, 
});

Meteor.startup( () => {
    WebApp.rawConnectHandlers.use( ( request, response, next ) => {
      // We need to echo the origin provided in the request
      const origin = request.headers.origin;
  
      if ( origin ) response.setHeader( 'Access-Control-Allow-Origin', origin );
  
      // For the preflight
      if ( request.method == 'OPTIONS' ) {
        response.setHeader( 'Access-Control-Allow-Headers', request.headers[ 'access-control-request-headers' ] );
  
        response.end();
      } else next();
    });
});