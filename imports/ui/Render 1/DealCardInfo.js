import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import ThumbUp from '@material-ui/icons/ThumbUp';
import ThumbDown from '@material-ui/icons/ThumbDown';
import Share from '@material-ui/icons/Share';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
import LocationSearch from './LocationSearch';
import LocationSearchMobile from './LocationSearchMobile';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { Typography } from '@material-ui/core';
import LinearProgress from '@material-ui/core/LinearProgress';
import TimeLapse from '@material-ui/icons/Timelapse';
import Calender from '@material-ui/icons/Today';
import {Coupon} from './DailyIcons/Icons';
import moment from 'moment';
import {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, SundayFill, MondayFill, TuesdayFill, WednesdayFill, ThursdayFill, FridayFill, SaturdayFill} from './DailyIcons/Icons';
import Fade from '@material-ui/core/Fade';

const styles = theme => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  card: {
    width: 50 + "vw",
    height: 90 + "vh"
  },
  cardmobile: {
    width: 80 + "vw"
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  input: {
    margin: theme.spacing.unit,
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  bar: {
    backgroundColor: "rgb(67, 160, 71)"
  },
  barSecondary: {
    backgroundColor: "rgb(239, 83, 80)"
  },
});

class DealCardInfo extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        open: false,
        value: 0,
        checked: false
    };

    componentDidMount(){
      setTimeout(() => {
        this.setState({checked: true})
      }, 100);
    }

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        const { checked } = this.state;

        return (
            <React.Fragment>
              <Fade in={checked}>
              <div style={{height: this.props.height, overflowY: "scroll", overflowX: "hidden"}}>
                <Grid container direction="column" alignItems="center" justify="center" style={{padding: 20}}>
                  <Grid item xs={12}>
                    <Typography variant="overline" style={{fontSize: 20}}>
                        {this.props.name}
                    </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Grid container direction="row" style={{width: "80vw"}}>
                      <Grid item xs={2} style={{textAlign: "right"}}>
                        <ThumbUp style={{color: "rgb(55, 71, 79)"}}/>
                      </Grid>
                      <Grid item xs={8}>
                        <div className={classes.paper}>
                          <LinearProgress variant="determinate" value={70} classes={{barColorPrimary: classes.bar, colorPrimary: classes.barSecondary}} />
                        </div>
                      </Grid>
                      <Grid item xs={2} style={{textAlign: "left"}}>
                        <ThumbDown style={{color: "rgb(55, 71, 79)"}}/>
                      </Grid>
                    </Grid>
                  </Grid>
                  <Grid item xs={12} style={{marginTop: 20}}>
                    <div style={{backgroundColor: "#E0E0E0", borderRadius: "20px", width: "80vw", height: 64, boxShadow:  "0px 0px 20px -5px rgba(0,0,0,0.75)"}}>
                      <Grid container direction="row">
                        <Grid item xs={4}>
                          <Button style={{width: "100%", height: 64, borderRadius: 0, borderTopLeftRadius: 20, borderBottomLeftRadius: 20}}>
                            <ThumbUp style={{color: "rgb(55, 71, 79)"}} />
                          </Button>
                        </Grid>
                        <Grid item xs={4}>
                          <Button style={{width: "100%", height: 64, borderRadius: 0}}>
                            <ThumbDown style={{color: "rgb(55, 71, 79)"}}/>
                          </Button>
                        </Grid>
                        <Grid item xs={4}>
                          <Button style={{width: "100%", height: 64, borderRadius: 0, borderTopRightRadius: 20, borderBottomRightRadius: 20}}>
                            <Share style={{color: "rgb(55, 71, 79)"}}/>
                          </Button>
                        </Grid>
                      </Grid>
                    </div>
                  </Grid>
                  <Grid item xs={12} style={{marginTop: 20}}>
                    <Button style={{background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)', boxShadow:  "0px 0px 20px -5px rgba(0,0,0,0.75)", borderRadius: 20, width: "80vw", color: "#FFFFFF", height: 64}}>
                      Activate Deal
                    </Button>
                  </Grid>
                </Grid>
                <Divider variant="middle"/>
                <Grid container direction="column" style={{padding: 20, paddingTop: 5, paddingBottom: 5}}>
                  <Grid item xs={12}>
                    <Typography variant="overline" style={{fontSize: 15, textAlign: "center"}}>
                      Deal Highlights
                    </Typography>
                  </Grid>
                  {(this.props.DealExp == false && this.props.DayWorks == false && this.props.HasCoupon == false) ? (
                    <Grid item xs={12}>
                      <Typography align="center" variant="overline" style={{fontSize: 10, color: "rgba(0, 0, 0, 0.54)"}}>
                        None
                      </Typography>
                    </Grid>
                  ):(null)}
                  {this.props.DealExp == true ? (
                    <div>
                      <Grid item xs={12}>
                        <Grid container direction="row" alignItems="center" justify="center">
                          <Grid item xs={1} align="center">
                            <TimeLapse/>
                          </Grid>
                          <Grid item xs={11} className={classes.paper}>
                            <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                              This deal has an expiration
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  ):(null)}
                  {this.props.DayWorks == true ? (
                    <div>
                      <Grid item xs={12}>
                        <Grid container direction="row" alignItems="center" justify="center">
                          <Grid item xs={1} align="center">
                            <Calender/>
                          </Grid>
                          <Grid item xs={11} className={classes.paper}>
                            <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                              This deal only works at certain times
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  ):(null)}
                  {this.props.HasCoupon == true ? (
                    <div>
                      <Grid item xs={12}>
                        <Grid container direction="row" alignItems="center" justify="center">
                          <Grid item xs={1} align="center">
                            <Coupon/>
                          </Grid>
                          <Grid item xs={11} className={classes.paper}>
                            <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                              This deal requires a coupon/code
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  ):(null)}
                </Grid>
                
                {this.props.DayWorks == true ? (
                    <div>
                      <Divider variant="middle"/>
                      <Grid container direction="column" style={{padding: 20, paddingTop: 5, paddingBottom: 5}}>
                        <Grid item xs={12}>
                          <Grid container direction="row" alignItems="center" justify="center">
                            <Grid item xs={12} className={classes.paper}>
                              <Typography variant="overline" style={{fontSize: 15, textAlign: "center"}}>
                                Active days and time
                              </Typography>
                            </Grid>
                            <Grid item xs={12}>
                              <Grid container direction="row">
                                {this.props.s == "true" ? (<Grid item xs style={{textAlign: "center"}}><SundayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Sunday/></Grid>)}
                                {this.props.m == "true" ? (<Grid item xs style={{textAlign: "center"}}><MondayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Monday/></Grid>)}
                                {this.props.tu == "true" ? (<Grid item xs style={{textAlign: "center"}}><TuesdayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Tuesday/></Grid>)}
                                {this.props.w == "true" ? (<Grid item xs style={{textAlign: "center"}}><WednesdayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Wednesday/></Grid>)}
                                {this.props.th == "true" ? (<Grid item xs style={{textAlign: "center"}}><ThursdayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Thursday/></Grid>)}
                                {this.props.f == "true" ? (<Grid item xs style={{textAlign: "center"}}><FridayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Friday/></Grid>)}
                                {this.props.sa == "true" ? (<Grid item xs style={{textAlign: "center"}}><SaturdayFill/></Grid>):(<Grid item xs style={{textAlign: "center"}}><Saturday/></Grid>)}
                              </Grid>
                            </Grid>
                            <Grid item xs={12}>
                              <Typography variant="overline" style={{fontSize: 12, textAlign: "center", color: "rgba(0, 0, 0, 0.54)", paddingTop: 5}}>
                                From {moment(this.props.TimeOfDay).format('h:mm a')} to {moment(this.props.TimeOfDayTo).format('h:mm a')}
                              </Typography>
                            </Grid>
                          </Grid>
                        </Grid>
                      </Grid>
                    </div>
                  ):(null)}
                <Divider variant="middle"/>
                <Grid container direction="column" spacing={16} style={{padding: 20, paddingTop: 5, paddingBottom: 5}}>
                    <Grid item xs={12}>
                      <Typography variant="overline" style={{fontSize: 15, textAlign: "center"}}>
                        More Info
                      </Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                        Price Range: {this.props.PriceRange}
                      </Typography>
                    </Grid>
                    {this.props.DealExp == true ? (
                      <Grid item xs={12}>
                        <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                          Starts On: {moment(this.props.DealExpirationDateStart).format('MMMM Do YYYY')} at {moment(this.props.DealExpirationTimeStart).format('h:mm a')}
                        </Typography>
                      </Grid>
                  ):(null)}
                  {this.props.DealExp == true ? (
                      <Grid item xs={12}>
                        <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                          Expires On: {moment(this.props.DealExpirationDate).format('MMMM Do YYYY')} at {moment(this.props.DealExpirationTime).format('h:mm a')}
                        </Typography>
                      </Grid>
                  ):(null)}
                  {this.props.HasCoupon == true ? (
                      <Grid item xs={12}>
                        <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                          Coupon Code: {this.props.CouponCode}
                        </Typography>
                      </Grid>
                  ):(null)}
                  {this.props.HasCoupon == true ? (
                      <Grid item xs={12}>
                        <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                          Coupon Details: {this.props.CouponDetails}
                        </Typography>
                      </Grid>
                  ):(null)}
                  <Grid item xs={12}>
                    <Typography variant="caption" style={{fontSize: 15, textAlign: "left"}}>
                      Details: {this.props.DealDetails}
                    </Typography>
                  </Grid>
                </Grid>
              </div>
              </Fade>
            </React.Fragment>
        );
    }
}

DealCardInfo.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(DealCardInfo);

// const DealModalWrapped = withStyles(styles)(DealStyleWrap);

export default DealStyleWrap