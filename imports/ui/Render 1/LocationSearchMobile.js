import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import Grid from '@material-ui/core/Grid';
import PlacesAutocomplete, {
    geocodeByAddress,
    geocodeByPlaceId,
    getLatLng,
  } from 'react-places-autocomplete';
import Input from '@material-ui/core/TextField';  
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import Downshift from "downshift";
import Checkbox from '@material-ui/core/Checkbox';
import Flexbox from 'flexbox-react';
import InfoIcon from '@material-ui/icons/Info';
import DropdownIcon from '@material-ui/icons/ArrowDropDown';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Select from '@material-ui/core/Select';
import DatePicker from 'material-ui-pickers/DatePicker';
import DateFnsUtils from 'material-ui-pickers/utils/date-fns-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/MuiPickersUtilsProvider';
import TimePicker  from 'material-ui-pickers/TimePicker';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withApollo } from "react-apollo";
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from './Snackbar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, SundayFill, MondayFill, TuesdayFill, WednesdayFill, ThursdayFill, FridayFill, SaturdayFill} from '../DailyIcons/Icons';
import keycode from 'keycode';
import Chip from '@material-ui/core/Chip'
import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';
import Help from '@material-ui/icons/Help';
import InputAdornment from '@material-ui/core/InputAdornment';
import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import DealCheckModal from './DealCheck';
import MediaQuery from 'react-responsive';
import Fab from '@material-ui/core/Fab';


const createDeal = gql`
    mutation createDeal($name: String!, $TypeCuisine: [String!]!, $TimeOfDay: String!, $TimeOfDayTo: String!, $FoodTitle: String!, $PriceRange: String!, $DealExpirationDate: String, $DealExpirationTime: String, $DealExpirationDateStart: String, $DealExpirationTimeStart: String, $StoreName: String!, $LocationAddress: String!, $StorePlaceId: String!, $DealDetails: String, $HasCoupon: Boolean!, $DayWorks: Boolean!, $DealExp: Boolean!, $CouponCode: String, $CouponDetails: String, $TimeStamp: String! $s: String, $m: String, $tu: String, $w: String, $th: String, $f: String, $sa: String) {
        createDeal(name: $name, TypeCuisine: $TypeCuisine, TimeOfDay: $TimeOfDay, TimeOfDayTo: $TimeOfDayTo, FoodTitle: $FoodTitle, PriceRange: $PriceRange, DealExpirationDate: $DealExpirationDate, DealExpirationTime: $DealExpirationTime, DealExpirationDateStart: $DealExpirationDateStart, DealExpirationTimeStart: $DealExpirationTimeStart, StoreName: $StoreName, LocationAddress: $LocationAddress, StorePlaceId: $StorePlaceId, DealDetails: $DealDetails, HasCoupon: $HasCoupon, DayWorks: $DayWorks, DealExp: $DealExp, CouponCode: $CouponCode, CouponDetails: $CouponDetails, TimeStamp: $TimeStamp, s: $s, m: $m, tu: $tu, w: $w, th: $th, f: $f, sa: $sa) {
            _id
        }
    },
    
`;

const createStore = gql `
    mutation createStore($StorePlaceId: String, $Photo: [String], $PriceLevel: String, $StoreName: String, $StorePhone: String, $StoreHours: [StoreHoursInput], $Website: String, $StoreAddress: String, $TimeStamp: String) {
        createStore(StorePlaceId: $StorePlaceId, Photo: $Photo, StoreName: $StoreName, StorePhone: $StorePhone, StoreHours: $StoreHours, Website: $Website, PriceLevel: $PriceLevel, StoreAddress: $StoreAddress, TimeStamp: $TimeStamp){
            _id
        }
    }
`
  
const cuisineListQuery = gql`
  query CuisineList($name: String!) {
    cuisineLists(name: $name) {
      _id
      name
    }
  }
`;

const cuisineListsWithoutInputQuery = gql`
  query CuisineList {
    cuisineListsWithoutInput {
      _id
      name
    }
  }
`;

const dealTypeListsQuery = gql`
  query DealType($name: String!) {
    dealTypeLists(name: $name) {
      _id
      name
    }
  }
`;
const dealTypeListsWithoutInputQuery = gql`
  query DealType {
    dealTypeListsWithoutInput {
      _id
      name
    }
  }
`;

const tutorialSteps = [
  {
    label: 'Enter store name and address.',
  },
  {
    label: 'Select day and time of week the deal works. (e.g. M-F and Noon)'
  },
  {
    label: 'Select date and time deal expires.'
  },
  {
    label: 'Does this require a coupon to work? Fill out coupon info accepted at location.'
  },
  {
    label: 'Write more details about coupon here.'
  },
];

const styles = theme => ({
  root: {
    width: 95 + "vw",
    height: 100 + "%",
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    width: 95 + "vw",
    paddingLeft: theme.spacing.unit * 4,
    marginBottom: 20,
    backgroundColor: theme.palette.background.default,
  },
  img: {
    height: 255,
    maxWidth: 400,
    overflow: 'hidden',
    width: '100%',
  },
  rootdownshift: {
    flexGrow: 1,
    height: 250,
  },
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  inputRoot: {
    flexWrap: 'wrap',
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  paper2: {
    position: 'fixed',
    zIndex: 1,
    margin: 20,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`,
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  inputRoot: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

const Items = ({
    
    data: { loading, cuisineLists },
    selectedItem,
    highlightedIndex,
    getItemProps
}) =>
    loading ? null : (
      <div>
        {cuisineLists.slice(0, 5).map((book, index) => (
          <MenuItem
              {...getItemProps({ item: book.name })}
              key={book._id}
              selected={highlightedIndex === index}
              component="div"
              style={{
              fontWeight: (selectedItem || '').indexOf(book.name) > -1 ? 500 : 400,
              }}
          >
              {book.name}
          </MenuItem>
        ))}
      </div>
);
  
const FetchItems = graphql(cuisineListQuery, {
    options: ({ name }) => ({ variables: { name } })
})(Items);

FetchItems.propTypes = {
    highlightedIndex: PropTypes.number,
    index: PropTypes.number,
    itemProps: PropTypes.object,
    selectedItem: PropTypes.array,
};

const ItemsWithoutInput = ({
    
    data: { loading, cuisineListsWithoutInput },
    selectedItem,
    highlightedIndex,
    getItemProps
}) =>
    loading ? null : (
      <div>
        {cuisineListsWithoutInput.map((book, index) => (
          <MenuItem
              {...getItemProps({ item: book.name })}
              key={book._id}
              selected={highlightedIndex === index}
              component="div"
              style={{
              fontWeight: (selectedItem || '').indexOf(book.name) > -1 ? 500 : 400,
              }}
          >
              {book.name}
          </MenuItem>
        ))}
      </div>
);
  
const FetchItemsWithoutInput = graphql(cuisineListsWithoutInputQuery)(ItemsWithoutInput);

FetchItemsWithoutInput.propTypes = {
highlightedIndex: PropTypes.number,
index: PropTypes.number,
itemProps: PropTypes.object,
selectedItem: PropTypes.array,
};
  
function renderInput(inputProps) {
      const { InputProps, classes, ref, ...other } = inputProps;
    
      return (
        <Input
            InputProps={{
                inputRef: ref,
                classes: {
                root: classes.inputRoot,
                input: classes.inputInput
                },
                ...InputProps,
          }}
          {...other}
          fullWidth
        />
    );
}

const DealItems = ({
    data: { loading, dealTypeLists },
    selectedItem,
    highlightedIndex,
    getItemProps
}) =>
    loading ? null : (
      <div>
        {dealTypeLists.slice(0, 5).map((book, index) => (
          <MenuItem
              {...getItemProps({ item: book.name })}
              key={book._id}
              selected={highlightedIndex === index}
              component="div"
              style={{
              fontWeight: (selectedItem || '').indexOf(book.name) > -1 ? 500 : 400,
              }}
          >
              {book.name}
          </MenuItem>
        ))}
      </div>
);
  
const DealFetchItems = graphql(dealTypeListsQuery, {
    options: ({ name }) => ({ variables: { name } })
})(DealItems);

DealFetchItems.propTypes = {
highlightedIndex: PropTypes.number,
index: PropTypes.number,
itemProps: PropTypes.object,
selectedItem: PropTypes.string,
};

const DealItemsWithoutInput = ({
    data: { loading, dealTypeListsWithoutInput },
    selectedItem,
    highlightedIndex,
    getItemProps
}) =>
    loading ? null : (
      <div>
        {dealTypeListsWithoutInput.slice(0, 5).map((book, index) => (
          <MenuItem
              {...getItemProps({ item: book.name })}
              key={book._id}
              selected={highlightedIndex === index}
              component="div"
              style={{
              fontWeight: (selectedItem || '').indexOf(book.name) > -1 ? 500 : 400,
              }}
          >
              {book.name}
          </MenuItem>
        ))}
      </div>
);
  
const DealItemsWithoutInputFetchItems = graphql(dealTypeListsWithoutInputQuery)(DealItemsWithoutInput);

DealItemsWithoutInputFetchItems.propTypes = {
highlightedIndex: PropTypes.number,
index: PropTypes.number,
itemProps: PropTypes.object,
selectedItem: PropTypes.string,
};
  
function renderDealInput(inputProps) {
      const { InputProps, classes, ref, ...other } = inputProps;
    
      return (
        <Input
            InputProps={{
                inputRef: ref,
                classes: {
                root: classes.inputRoot,
                input: classes.inputInput
                },
                ...InputProps,
          }}
          {...other}
          fullWidth
        />
    );
}

class SwipeableTextMobileStepper extends React.Component {
    constructor(props) {
        super(props)
        this.state = { anchorCuisineEl: null, anchorDealEl: null, TimeStamp: new Date(), handleDay: false, TimeOfDayTo: '', handleExpiration: false, openCuisineTip: false, inputValue: '', selectedItem: [], dealinputValue: '', dealselectedItem: '', DealExp: false, DayWorks: false, CouponRequired: false, StorePlaceId: '', Website: '', StoreHours: [], StorePhone: '', s: false, m: false, tu: false, w: false, th: false, f: false, sa: false, error: false, SnackOpen: false, SnackType: "info", SnackError: null, address: '', placeId: '', StoreName: '', activeStep: 0, name: '', TypeCuisine: '', TimeOfDay: '', FoodTitle: '', PriceRange: '', DealExpirationDate: '', DealExpirationTime: '', DealExpirationDateStart: '', DealExpirationTimeStart: '', DealDetails: '', HasCoupon: false, CouponCode: '', CouponDetails: '', photo: [] };
        this.handleChange = this.handleChange.bind(this);
        this.handleDealChange = this.handleDealChange.bind(this);
    };

    handleOpenCoupon = () => {
        this.setState({ CouponRequired: true });
        this.setState({ HasCoupon: true });
    };

    handleCloseCoupon = () => {
        this.setState({ CouponRequired: false });
        this.setState({ HasCoupon: false });
        this.setState({ CouponCode: ''});
        this.setState({ CouponDetails: ''});
    };

    handleOpenDay = () => {
        this.setState({ DayWorks: true });
    };

    handleCloseDay = () => {
        this.setState({ DayWorks: false })
        this.setState({ s: false });
        this.setState({ m: false });
        this.setState({ tu: false });
        this.setState({ w: false });
        this.setState({ th: false });
        this.setState({ f: false });
        this.setState({ sa: false });
        this.setState({ TimeOfDay: '', TimeOfDayTo: '' });
    };

    handleOpenExp = () => {
        this.setState({ DealExp: true });
    };

    handleCloseExp = () => {
        this.setState({ DealExp: false })
        this.setState({ DealExpirationDate: '', DealExpirationDateStart: '' });
        this.setState({ DealExpirationTime: '', DealExpirationTimeStart: '' });
    };

    handleNext = () => {
        this.setState(prevState => ({
        activeStep: prevState.activeStep + 1,
        }));
        if (this.state.CouponRequired === true && this.state.CouponCode === '' && this.state.CouponDetails === '') {
            this.handleCloseCoupon();
        }
        else if (this.state.DayWorks === true && this.state.TimeOfDay === '' && this.state.TimeOfDayTo === '' && this.state.s === false && this.state.m === false && this.state.tu === false && this.state.w === false && this.state.th === false && this.state.f === false && this.state.sa === false) {
            this.handleCloseDay();
        }
        else if (this.state.DealExp === true && this.state.DealExpirationDate === '' && this.state.DealExpirationTime === '' && this.state.DealExpirationDateStart === '' && this.state.DealExpirationTimeStart === '') {
            this.handleCloseExp();
        }
    };

    handleBack = () => {
        this.setState(prevState => ({
        activeStep: prevState.activeStep - 1,
        }));
    };

    handleStepChange = activeStep => {
        this.setState({ activeStep });
    };
    handleLocationChange = (address, placeId) => {
        this.setState({ address });
        this.setState({ placeId });
    };

    handleLocaitonSelect = (address, placeId) => {
        this.setState({ address });
        this.setState({ placeId });
        this.StoreInfo(placeId);
    };

    StoreInfo = (placeId) => {
        Meteor.call('getData', placeId, (error, results) => {
            
            if (error) {
             console.log("error");
           } else {
            this.setState({StorePlaceId: results.data.result.place_id});
            this.setState({StoreName: results.data.result.name});
            this.setState({Website: results.data.result.website});
            this.setState({StorePhone: results.data.result.formatted_phone_number});
            for (var i = 0; i != 1; i++) {
                var newArray = this.state.photo.slice();    
                newArray.push(results.data.result.photos[i].photo_reference);   
                this.setState({photo: newArray});
            }
            if (results.data.result.opening_hours.periods === undefined) {
                console.log("Hours not defined");
            }
            else {
                // this.setState({StoreHours: results.data.result.opening_hours.periods});
                for (var i = 0; i < 7; i++) {
                    var newArray2 = this.state.StoreHours.slice();
                    for (var j = 0; j < 7; j++) {
                        if (results.data.result.opening_hours.periods[j] == undefined) {
                            var openDay = i;
                            var openTime = "Closed";
                            break;
                        } 
                        //console.log(results.data.result.opening_hours.periods[j].open.day);
                        if (i === results.data.result.opening_hours.periods[j].open.day) {
                            var openDay = results.data.result.opening_hours.periods[j].open.day;
                            var openTime = results.data.result.opening_hours.periods[j].open.time;
                            break;
                        }
                    }
                    for (var k = 0; k < 7; k++) {
                        if (results.data.result.opening_hours.periods[k] == undefined) {
                            var closeDay = i;
                            var closeTime = "Closed";
                            break;
                            
                        } 
                        //console.log(results.data.result.opening_hours.periods[k].close.day);
                        if (i === results.data.result.opening_hours.periods[k].close.day) {
                            var closeDay = results.data.result.opening_hours.periods[k].close.day;
                            var closeTime = results.data.result.opening_hours.periods[k].close.time;
                            break;
                        }
                    }
                    newArray2.push({"open": {"day": openDay, "time": openTime}, "close": {"day": closeDay, "time": closeTime}});
                    //console.log(newArray2);
                    this.setState({StoreHours: newArray2});
                }
            }
            
            // console.log(this.state.Website);
            // console.log(results.data.result.formatted_phone_number)
            //console.log(results.data.result.opening_hours.weekday_text[5])
           }
        });
    };

    storeCreate = () => {
        this.props
            .createStore({
                variables:{
                    StorePlaceId: this.state.StorePlaceId,
                    StoreName: this.state.StoreName,
                    StorePhone: this.state.StorePhone,
                    StoreHours: this.state.StoreHours,
                    Website: this.state.Website,
                    StoreAddress: this.state.address,
                    PriceLevel: this.state.PriceRange,
                    Photo: this.state.photo,
                    TimeStamp: this.state.TimeStamp
                }
            })
            .catch((error => {
                console.log(error);
        }));
    };
    

    submitForm = () => {
        const SnackTypeT = "error";
        const SnackError = "Required field is empty"
        if ((this.state.PriceRange  === '' || this.state.StoreName  === '' || this.state.address === '') && (this.state.dealselectedItem.length == 0 || this.state.selectedItem.length == 0))
        {
            this.handleOpenSnack(SnackTypeT, SnackError);
            this.setState({ error: true });
        }
        else
        {
            this.storeCreate();
            this.props
            .createDeal({
                variables:{
                    name: this.state.dealselectedItem,
                    StoreName: this.state.StoreName,
                    TypeCuisine: this.state.selectedItem,
                    TimeOfDay: this.state.TimeOfDay,
                    TimeOfDayTo: this.state.TimeOfDayTo,
                    FoodTitle: this.state.FoodTitle,
                    PriceRange: this.state.PriceRange,
                    DealExpirationDate: this.state.DealExpirationDate,
                    DealExpirationTime: this.state.DealExpirationTime,
                    DealExpirationTimeStart: this.state.DealExpirationTimeStart,
                    DealExpirationDateStart: this.state.DealExpirationDateStart,
                    LocationAddress: this.state.address,
                    StorePlaceId: this.state.placeId,
                    DealDetails: this.state.DealDetails, 
                    HasCoupon: this.state.HasCoupon,
                    DayWorks: this.state.DayWorks,
                    DealExp: this.state.DealExp,
                    CouponCode: this.state.CouponCode, 
                    CouponDetails: this.state.CouponDetails,
                    TimeStamp: this.state.TimeStamp,
                    s: this.state.s,
                    m: this.state.m,
                    tu: this.state.tu,
                    w: this.state.w,
                    th: this.state.th,
                    f: this.state.f,
                    sa: this.state.sa,

                }
            })
            .then(this.props.action)
            .catch((error => {
                console.log(error);
            }));
        }
        
    };

    handleOpenSnack = (SnackTypeT, SnackError) => {
        this.setState({ SnackOpen: true, SnackType: SnackTypeT, SnackError });
    };

    handleCloseSnack = (event, reason) => {
        if (reason === "clickaway") {
          return;
        }
        this.setState({ SnackOpen: false });
    }

    handleDateChangeStart = (date) => {
        this.setState({ DealExpirationDateStart: date });
    };

    handleTimeChangeStart = (date) => {
        this.setState({ DealExpirationTimeStart: date });
    };

    handleDateChange = (date) => {
        this.setState({ DealExpirationDate: date });
    };

    handleTimeChange = (date) => {
        this.setState({ DealExpirationTime: date });
    };

    handleCheckChange = name => event => {
        this.setState({ [name]: event.target.checked });
    };

    handleKeyDown = event => {
        const { inputValue, selectedItem } = this.state;
        if (selectedItem.length && !inputValue.length && keycode(event) === 'backspace') {
            this.setState({
            selectedItem: selectedItem.slice(0, selectedItem.length - 1),
            });
        }
    };

    handleInputChange = event => {
        this.setState({ inputValue: event.target.value });
    };

    handleChange = item => {
        let { selectedItem } = this.state;

        if (selectedItem.indexOf(item) === -1) {
            selectedItem = [...selectedItem, item];
        }

        this.setState({
            inputValue: '',
            selectedItem,
        });
        
    };
    
    handleDelete = item => () => {
        this.setState(state => {
            const selectedItem = [...state.selectedItem];
            selectedItem.splice(selectedItem.indexOf(item), 1);
            return { selectedItem };
        });
    };

    handleDealKeyDown = event => {
        const { dealinputValue, dealselectedItem } = this.state;
        if (dealselectedItem.length && !dealinputValue.length && keycode(event) === 'backspace') {
            this.setState({
            dealselectedItem: dealselectedItem.slice(0, dealselectedItem.length - 1),
            });
        }
    };

    handleDealInputChange = event => {
        this.setState({ dealinputValue: event.target.value });
    };

    handleDealChange = item => {
        // let { dealselectedItem } = this.state;

        // if (dealselectedItem.indexOf(item) === -1) {
        //     dealselectedItem = [...dealselectedItem, item];
        // }

        this.setState({
            dealinputValue: '',
            dealselectedItem: item,
        });
        
    };
    
    handleDealDelete = () => {
        this.setState({dealselectedItem: ''});
    };

    handleCuisineClick = event => {
        this.setState({ anchorCuisineEl: event.currentTarget });
    };

    handleCuisineClose = () => {
        this.setState({ anchorCuisineEl: null });
    };

    handleDealClick = event => {
        this.setState({ anchorDealEl: event.currentTarget });
    };

    handleDealClose = () => {
        this.setState({ anchorDealEl: null });
    };

    handleMainModalClose = () => {
        this.props.action()
    };

    // handleCuisineTooltipClose = () => {
    //     this.setState({ openCuisineTip: false });
    // };

    // handleCuisineTooltipOpen = () => {
    //     this.setState({ openCuisineTip: true });
    // };

    render() {
        const { classes, theme } = this.props;
        const { activeStep } = this.state;
        const { inputValue, selectedItem, dealinputValue, dealselectedItem } = this.state;

        const maxSteps = tutorialSteps.length;

        const submitBtn = (this.state.activeStep === maxSteps - 1) ? (
            <Fab variant="extended" onClick={this.submitForm} style={{backgroundColor: "#1565c0", width: 91.9667, color: "#fff"}}> 
                Submit
            </Fab>
        ):(
            <Fab variant="extended" onClick={this.handleNext} disabled={activeStep === maxSteps - 1} style={{backgroundColor: "#ff8a65"}}> 
                Next
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Fab>            
        )

        const CouponRequired = this.state.CouponRequired;
        const DayWorks = this.state.DayWorks;
        const DealExp = this.state.DealExp;
        const { anchorCuisineEl, anchorDealEl } = this.state;
        return (
            <React.Fragment>
                {/* <MediaQuery query="(min-height: 568px)"> */}
                <div className={classes.root}>
                    <Flexbox flexDirection="column" minHeight="80vh" minWidth="95vw">
                        <Flexbox element="header">
                            <Grid container justify="center" alignItems="center">
                                <Grid item xs={12}>
                                    <MobileStepper
                                    steps={maxSteps}
                                    position="static"
                                    activeStep={activeStep}
                                    style={{backgraoundColor: "#fff", width: 100 + "%", justifyContent: "center"}}
                                    variant="progress"
                                    />
                                </Grid>
                            </Grid>
                        </Flexbox>
                        <Flexbox display="flex" flexWrap="wrap" justifyContent="space-around" minHeight="70vh" flexGrow={1}>
                            <SwipeableViews
                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={this.state.activeStep}
                            onChangeIndex={this.handleStepChange}
                            enableMouseEvents
                            >
                                <div key="Store Location">
                                    <div style={{margin: 20}}>
                                        <Grid container justify="flex-start"> 
                                        <PlacesAutocomplete
                                        value={this.state.address}
                                        onChange={this.handleLocationChange}
                                        onSelect={this.handleLocaitonSelect}
                                        >
                                            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => ( 
                                                <Grid item xs={12}>
                                                    <div style={{margin: 10}}>                
                                                        <Input
                                                        error={this.state.error}
                                                        {...getInputProps({
                                                            label: 'Store Location',
                                                            className: 'location-search-input',
                                                        })}
                                                        required
                                                        fullWidth>
                                                        </Input>
                                                        <div>
                                                            
                                                            <Paper className={classes.paper2} square>
                                                                {loading && <Typography variant="subtitle1">Loading...</Typography>}
                                                                {suggestions.map(suggestion => {
                                                                    const className = suggestion.active
                                                                    ? 'suggestion-item--active'
                                                                    : 'suggestion-item';
                                                                    // inline style for demonstration purpose
                                                                    const style = suggestion.active
                                                                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                                                    return (
                                                                    <MenuItem
                                                                        {...getSuggestionItemProps(suggestion, {
                                                                        className,
                                                                        style,
                                                                        })}
                                                                    >
                                                                        <span>{suggestion.description}</span>
                                                                    </MenuItem>
                                                                    );
                                                                })}
                                                            </Paper>
                                                        </div>
                                                    </div>
                                                </Grid>
                                            )}
                                            </PlacesAutocomplete>
                                            <Grid item xs={12}>
                                                <div style={{margin: 10}} className={classes.inputRoot}>
                                                    <Downshift
                                                    id="downshift-multiple"
                                                    inputValue={inputValue}
                                                    onChange={this.handleChange}
                                                    selectedItem={selectedItem}
                                                    >
                                                        {({
                                                            getInputProps,
                                                            getItemProps,
                                                            isOpen,
                                                            inputValue: inputValue2,
                                                            selectedItem: selectedItem2,
                                                            highlightedIndex
                                                        }) => (
                                                            <div className={classes.container}>
                                                            <Input
                                                                label="Cuisine/Food type"
                                                                error={this.state.error}
                                                                onClick={this.handleCuisineClick}
                                                                aria-owns={anchorCuisineEl ? 'simple-menu' : undefined}
                                                                aria-haspopup="true"
                                                                InputProps={{
                                                                    classes: {
                                                                        root: classes.inputRoot,
                                                                        input: classes.inputInput
                                                                    },
                                                                    startAdornment: (
                                                                        <Grid container direction="row" alignItems="center" justify="center">
                                                                            <Grid item xs={10}>
                                                                                <div style={{height: 90, overflowY: "auto", width: 100 + "%", top: 31, position: "relative"}}>
                                                                                    {selectedItem.map(item => (
                                                                                        <Chip
                                                                                        key={item}
                                                                                        tabIndex={-1}
                                                                                        label={item}
                                                                                        className={classes.chip}
                                                                                        onDelete={this.handleDelete(item)}
                                                                                        />
                                                                                    ))}
                                                                                </div>
                                                                            </Grid>
                                                                            <Grid item xs={2}>
                                                                                <div style={{textAlign: "right"}}>
                                                                                    <IconButton>
                                                                                        <DropdownIcon/>
                                                                                    </IconButton>
                                                                                </div>
                                                                                
                                                                            </Grid>
                                                                        </Grid>
                                                                    ),
                                                                }}
                                                                required
                                                                fullWidth
                                                            />
                                                                <Popover
                                                                anchorEl={anchorCuisineEl}
                                                                open={Boolean(anchorCuisineEl)}
                                                                onClose={this.handleCuisineClose}
                                                                style={{width: 100 + "vw", height: 250}}
                                                                id="simple-popper"
                                                                anchorOrigin={{
                                                                    vertical: 'bottom',
                                                                    horizontal: 'center',
                                                                }}
                                                                transformOrigin={{
                                                                    vertical: 'top',
                                                                    horizontal: 'center',
                                                                }}
                                                                >
                                                                    <div className={classes.container} style={{width: 89.162 + "vw"}}>
                                                                        <div style={{padding: 10, width: 100 + "%"}}>
                                                                            {renderInput({
                                                                                classes,
                                                                                error: this.state.error,
                                                                                InputProps: getInputProps({
                                                                                onChange: this.handleInputChange,
                                                                                onKeyDown: this.handleKeyDown,
                                                                                }),
                                                                                label: 'Search',
                                                                            })}
                                                                        </div>
                                                                        {inputValue === '' ? (
                                                                            <div>
                                                                                <FetchItemsWithoutInput
                                                                                name={inputValue2}
                                                                                selectedItem={selectedItem2}
                                                                                highlightedIndex={highlightedIndex}
                                                                                getItemProps={getItemProps}
                                                                                />
                                                                            </div>
                                                                        ):(
                                                                            <div>
                                                                                <FetchItems
                                                                                name={inputValue2}
                                                                                selectedItem={selectedItem2}
                                                                                highlightedIndex={highlightedIndex}
                                                                                getItemProps={getItemProps}
                                                                                />
                                                                            </div>
                                                                        )}
                                                                    </div>
                                                                </Popover>
                                                            </div>
                                                        )}
                                                    
                                                    </Downshift>
                                                </div>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <div style={{margin: 10}} className={classes.inputRoot}>
                                                    <Downshift
                                                    id="downshift-multiple2"
                                                    inputValue={dealinputValue}
                                                    onChange={this.handleDealChange}
                                                    selectedItem={dealselectedItem}
                                                    >
                                                    {({
                                                        getInputProps,
                                                        getItemProps,
                                                        isOpen,
                                                        inputValue: inputValue2,
                                                        selectedItem: selectedItem2,
                                                        highlightedIndex
                                                    }) => (
                                                        <div className={classes.container}>
                                                            <Input
                                                                label="Deal type"
                                                                error={this.state.error}
                                                                onClick={this.handleDealClick}
                                                                aria-owns={anchorDealEl ? 'simple-menu' : undefined}
                                                                aria-haspopup="true"
                                                                style={{height: 60}}
                                                                InputProps={{
                                                                    classes: {
                                                                        root: classes.inputRoot,
                                                                        input: classes.inputInput
                                                                    },
                                                                    startAdornment: (
                                                                        <Grid container direction="row" alignItems="center" justify="center">
                                                                            <Grid item xs={10}>
                                                                                <div style={{height: 40, overflowY: "auto", width: 100 + "%"}}>
                                                                                    {dealselectedItem === "" ?
                                                                                    (
                                                                                        null
                                                                                    ):
                                                                                    (
                                                                                        <Chip
                                                                                        tabIndex={-1}
                                                                                        label={dealselectedItem}
                                                                                        className={classes.chip}
                                                                                        onDelete={this.handleDealDelete}
                                                                                        />
                                                                                    )}
                                                                                </div>
                                                                            </Grid>
                                                                            <Grid item xs={2}>
                                                                                <div style={{textAlign: "right"}}>
                                                                                    <IconButton>
                                                                                        <DropdownIcon/>
                                                                                    </IconButton>
                                                                                </div>
                                                                                
                                                                            </Grid>
                                                                        </Grid>
                                                                    ),
                                                                    // endAdornment: (
                                                                    // <InputAdornment variant="filled" position="end">
                                                                    //     <IconButton>
                                                                    //         <DropdownIcon/>
                                                                    //     </IconButton>
                                                                    // </InputAdornment>
                                                                    // ),
                                                                }}
                                                                required
                                                                fullWidth
                                                            />
                                                                <Popover
                                                                anchorEl={anchorDealEl}
                                                                open={Boolean(anchorDealEl)}
                                                                onClose={this.handleDealClose}
                                                                style={{width: 100 + "vw", height: 250}}
                                                                id="simple-popper"
                                                                anchorOrigin={{
                                                                    vertical: 'bottom',
                                                                    horizontal: 'center',
                                                                }}
                                                                transformOrigin={{
                                                                    vertical: 'top',
                                                                    horizontal: 'center',
                                                                }}
                                                                >
                                                                    <div className={classes.container} style={{width: 89.162 + "vw"}}>
                                                                        <div style={{padding: 10, width: 100 + "%"}}>
                                                                            {renderDealInput({
                                                                                classes,
                                                                                error: this.state.error,
                                                                                InputProps: getInputProps({
                                                                                onChange: this.handleDealInputChange,
                                                                                onKeyDown: this.handleDealKeyDown,
                                                                                }),
                                                                                label: 'Search',
                                                                            })}
                                                                        </div>
                                                                    {inputValue2 === '' ? (
                                                                            <div>
                                                                                <DealItemsWithoutInputFetchItems
                                                                                name={inputValue2}
                                                                                selectedItem={selectedItem2}
                                                                                highlightedIndex={highlightedIndex}
                                                                                getItemProps={getItemProps}
                                                                                />
                                                                            </div>
                                                                        ):(
                                                                            <div>
                                                                                <DealFetchItems
                                                                                name={inputValue2}
                                                                                selectedItem={selectedItem2}
                                                                                highlightedIndex={highlightedIndex}
                                                                                getItemProps={getItemProps}
                                                                                />
                                                                            </div>
                                                                        )}
                                                                    </div>
                                                                </Popover>
                                                            </div>
                                                    )}
                                                    </Downshift>
                                                </div>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <div style={{margin: 10}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="name-readonly">Price Range</InputLabel>
                                                        <Select
                                                            value={this.state.PriceRange}
                                                            required
                                                            error={this.state.error}
                                                            onChange={e => {this.setState({ PriceRange: e.target.value })}}
                                                        >
                                                            <MenuItem value={"$"}>$</MenuItem>
                                                            <MenuItem value={"$$"}>$$</MenuItem>
                                                            <MenuItem value={"$$$"}>$$$</MenuItem>
                                                            <MenuItem value={"$$$$"}>$$$$</MenuItem>
                                                        </Select>
                                                    </FormControl>
                                                </div>
                                            </Grid>
                                            {/* <Grid item xs={12}>
                                                <div style={{margin: 10}}>
                                                    <Input fullWidth error={this.state.error} required type="text" label="Food/Bev Name" value={this.state.FoodTitle} onChange={e => {this.setState({ FoodTitle: e.target.value })}} />
                                                </div>
                                            </Grid> */}
                                        </Grid>
                                    </div>    
                                </div>
                                <div key="Deal Details">
                                    <div style={{margin: 20}}>
                                        <Grid container> 
                                            <Grid container justify="space-around"> 
                                                <Grid item xs={12}>
                                                    <Input type="text" label="Describe Deal" multiline rowsMax="4" fullWidth  required error={this.state.error} InputLabelProps={{shrink: true,}} value={this.state.DealDetails} onChange={e => {this.setState({ DealDetails: e.target.value })}} />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </div> 
                                <div key="Deal Time">
                                    <div style={{margin: 20}}>
                                        {DayWorks ? (
                                            <div>
                                                <Grid container justify="space-around"> 
                                                    <Grid item xs={12}>
                                                        <div style={{margin: 10}}>
                                                            <Grid container direction="column" alignItems="center" justify="center"> 
                                                                <Grid item xs={12}>
                                                                    <div style={{margin: 10, alignContent: "center"}}>
                                                                        <FormLabel component="legend">Select day(s) of week deal works</FormLabel>
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.s} icon={<Sunday/>} onChange={this.handleCheckChange('s')} checkedIcon={<SundayFill/>}/>
                                                                            }
                                                                        />
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.m} icon={<Monday />} onChange={this.handleCheckChange('m')} checkedIcon={<MondayFill />}/>
                                                                            }
                                                                        />
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.tu} icon={<Tuesday />} onChange={this.handleCheckChange('tu')} checkedIcon={<TuesdayFill />}/>
                                                                            }
                                                                        />
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.w} icon={<Wednesday />} onChange={this.handleCheckChange('w')} checkedIcon={<WednesdayFill />}/>
                                                                            }
                                                                        />
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.th} icon={<Thursday />} onChange={this.handleCheckChange('th')} checkedIcon={<ThursdayFill />}/>
                                                                            }
                                                                        />
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.f} icon={<Friday />} onChange={this.handleCheckChange('f')} checkedIcon={<FridayFill />}/>
                                                                            }
                                                                        />
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox checked={this.state.sa} icon={<Saturday />} onChange={this.handleCheckChange('sa')} checkedIcon={<SaturdayFill />}/>
                                                                            }
                                                                        />
                                                                    </div>
                                                                </Grid>
                                                            </Grid>
                                                        </div>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <div style={{margin: 10}}>
                                                            {/* <Input error={this.state.error} required type="text" label="Time of Day" value={this.state.TimeOfDay} onChange={e => {this.setState({ TimeOfDay: e.target.value })}} /> */}
                                                            {/* <FormControl fullWidth style={{minWidth: 120, margin: 10}}>
                                                                <InputLabel htmlFor="name-readonly">Time of Day</InputLabel>
                                                                <Select
                                                                    value={this.state.TimeOfDay}
                                                                    required
                                                                    error={this.state.error}
                                                                    onChange={e => {this.setState({ TimeOfDay: e.target.value })}}
                                                                >
                                                                    <MenuItem value={"Morning"}>Morning</MenuItem>
                                                                    <MenuItem value={"Noon"}>Noon</MenuItem>
                                                                    <MenuItem value={"Afternoon"}>Afternoon</MenuItem>
                                                                    <MenuItem value={"Evening"}>Evening</MenuItem>
                                                                    <MenuItem value={"Midnight"}>Midnight</MenuItem>
                                                                </Select>
                                                            </FormControl> */}
                                                            <Grid container justify="space-around">
                                                                <Grid item xs={12}>
                                                                    <Typography variant="h6" align="center" style={{ color: "rgba(0, 0, 0, 0.54)"}}>
                                                                        Time Range
                                                                    </Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <div style={{margin: 10}}>
                                                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                            <div className="pickers">
                                                                                <TimePicker
                                                                                    autoOk
                                                                                    label="From"
                                                                                    value={this.state.TimeOfDay}
                                                                                    onChange={(date) => this.setState({ TimeOfDay: date })}
                                                                                    error={false}
                                                                                />
                                                                            </div>    
                                                                        </MuiPickersUtilsProvider>
                                                                    </div>
                                                                </Grid>
                                                                <Grid item>
                                                                    <div style={{margin: 10}}>    
                                                                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                            <div className="pickers">
                                                                                <TimePicker
                                                                                    autoOk
                                                                                    label="To"
                                                                                    value={this.state.TimeOfDayTo}
                                                                                    onChange={(date) => this.setState({ TimeOfDayTo: date })}
                                                                                    error={false}
                                                                                />
                                                                            </div>    
                                                                        </MuiPickersUtilsProvider>
                                                                    </div>
                                                                </Grid>
                                                            </Grid>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container direction="column" justify="flex-end" alignItems="center">
                                                    <Grid item>
                                                        <div style={{textAlign: "center"}}>
                                                            {CouponRequired && activeStep === 3 ? (
                                                                <Button onClick={this.handleCloseCoupon}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                            {DayWorks && activeStep === 1 ? (
                                                                <Button onClick={this.handleCloseDay}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                            {DealExp && activeStep === 2 ? (
                                                                <Button onClick={this.handleCloseExp}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        ):(
                                            <div>
                                                <Grid container alignItems="center" justify="center">
                                                    <Grid item>
                                                        <div style={{marginBottom: 10}}>
                                                            <Typography variant="h2" align="center">
                                                                Does this deal only work on certain days?
                                                            </Typography>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center" justify="center" spacing={16}>
                                                    <Grid item>
                                                        <Button variant="outlined" onClick={this.handleNext} disabled={activeStep === maxSteps - 1} style={{ color: "#ff8a65", width: 56.9}}> 
                                                            No
                                                        </Button>  
                                                    </Grid>
                                                    <Grid item>
                                                        <Button variant="outlined" onClick={this.handleOpenDay} style={{color:"#1565c0"}}> 
                                                            Yes
                                                        </Button> 
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        )}
                                    </div>
                                    { (this.state.StorePlaceId != '' || this.state.address != '') && this.state.dealselectedItem != '' ? (
                                        <DealCheckModal  action={this.handleMainModalClose} activeStep={this.state.activeStep} StorePlaceId={this.state.StorePlaceId} address={this.state.address} DealType={this.state.dealselectedItem} />
                                    ):(
                                        null
                                    )}
                                    
                                </div>
                                <div key="Deal Expiration Details">
                                    <div style={{margin: 20}}>
                                        {DealExp ? (
                                            <div>
                                                <Grid container justify="space-around">
                                                    <Grid item>
                                                        <div style={{margin: 10}}>
                                                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                <div className="pickers">
                                                                    <DatePicker
                                                                        label="Deal Start Date"
                                                                        value={this.state.DealExpirationDateStart}
                                                                        onChange={this.handleDateChangeStart}
                                                                        animateYearScrolling={false}
                                                                        error={false}
                                                                    />
                                                                </div>    
                                                            </MuiPickersUtilsProvider>
                                                        </div>
                                                    </Grid>
                                                    <Grid item>
                                                        <div style={{margin: 10}}>    
                                                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                <div className="pickers">
                                                                    <TimePicker
                                                                        autoOk
                                                                        label="Deal Start Time"
                                                                        value={this.state.DealExpirationTimeStart}
                                                                        onChange={this.handleTimeChangeStart}
                                                                        error={false}
                                                                    />
                                                                </div>    
                                                            </MuiPickersUtilsProvider>
                                                        </div>
                                                    </Grid>
                                                    <Grid item>
                                                        <div style={{margin: 10}}>
                                                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                <div className="pickers">
                                                                    <DatePicker
                                                                        label="Deal Expiration Date"
                                                                        value={this.state.DealExpirationDate}
                                                                        onChange={this.handleDateChange}
                                                                        animateYearScrolling={false}
                                                                        error={false}
                                                                    />
                                                                </div>    
                                                            </MuiPickersUtilsProvider>
                                                        </div>
                                                    </Grid>
                                                    <Grid item>
                                                        <div style={{margin: 10}}>    
                                                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                                <div className="pickers">
                                                                    <TimePicker
                                                                        autoOk
                                                                        label="Deal Expiration Time"
                                                                        value={this.state.DealExpirationTime}
                                                                        onChange={this.handleTimeChange}
                                                                        error={false}
                                                                    />
                                                                </div>    
                                                            </MuiPickersUtilsProvider>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container direction="column" justify="flex-end" alignItems="center">
                                                    <Grid item>
                                                        <div style={{textAlign: "center"}}>
                                                            {CouponRequired && activeStep === 3 ? (
                                                                <Button onClick={this.handleCloseCoupon}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                            {DayWorks && activeStep === 1 ? (
                                                                <Button onClick={this.handleCloseDay}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                            {DealExp && activeStep === 2 ? (
                                                                <Button onClick={this.handleCloseExp}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        ):(
                                            <div>
                                                <Grid container alignItems="center" justify="center">
                                                    <Grid item>
                                                        <div style={{marginBottom: 10}}>
                                                            <Typography variant="h2" align="center">
                                                                Does deal expire?
                                                            </Typography>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center" justify="center" spacing={16}>
                                                    <Grid item>
                                                        <Button variant="outlined"  onClick={this.handleNext} disabled={activeStep === maxSteps - 1} style={{ color: "#ff8a65", width: 56.9}}> 
                                                            No
                                                        </Button>  
                                                    </Grid>
                                                    <Grid item>
                                                        <Button variant="outlined" onClick={this.handleOpenExp} style={{color:"#1565c0"}}> 
                                                            Yes
                                                        </Button> 
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        )}
                                    </div>
                                    
                                </div>
                                <div key="Coupon Details" className={classes.container}>
                                    <div style={{margin: 20}}>
                                        {CouponRequired ? (
                                            <div>
                                                <Grid container justify="space-around" alignItems="center"> 
                                                    <Grid item xs={12}>
                                                        <div style={{margin: 10}}>
                                                            <Input type="text" fullWidth label="Coupon Link/Code" value={this.state.CouponCode} onChange={e => {this.setState({ CouponCode: e.target.value })}} />
                                                        </div>
                                                    </Grid>
                                                    <Grid item xs={12}>
                                                        <div style={{margin: 10}}>
                                                            <Input type="text" multiline fullWidth rowsMax="4" label="Coupon Details" value={this.state.CouponDetails} onChange={e => {this.setState({ CouponDetails: e.target.value })}} />
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container direction="column" justify="flex-end" alignItems="center">
                                                    <Grid item>
                                                        <div style={{textAlign: "center"}}>
                                                            {CouponRequired && activeStep === 3 ? (
                                                                <Button onClick={this.handleCloseCoupon}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                            {DayWorks && activeStep === 1 ? (
                                                                <Button onClick={this.handleCloseDay}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                            {DealExp && activeStep === 2 ? (
                                                                <Button onClick={this.handleCloseExp}>Clear/Not Required</Button>
                                                            ):(
                                                                null
                                                            )}
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        ):(
                                            <div>
                                                <Grid container alignItems="center" justify="center">
                                                    <Grid item>
                                                        <div style={{marginBottom: 10}}>
                                                            <Typography variant="h2" align="center">
                                                                Does deal require coupon or code?
                                                            </Typography>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                                <Grid container alignItems="center" justify="center" spacing={16}>
                                                    <Grid item>
                                                        <Button variant="outlined"  onClick={this.handleNext} disabled={activeStep === maxSteps - 1} style={{ color: "#ff8a65", width: 56.9}}> 
                                                            No
                                                        </Button>  
                                                    </Grid>
                                                    <Grid item>
                                                        <Button variant="outlined" onClick={this.handleOpenCoupon} style={{color:"#1565c0"}}> 
                                                            Yes
                                                        </Button> 
                                                    </Grid>
                                                </Grid>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </SwipeableViews>
                        </Flexbox>
                        <Flexbox element="footer">
                            <Grid container direction="column">
                                <Grid item>
                                    <Grid container justify="space-around" alignItems="center">
                                        <Grid item>
                                            <Fab variant="extended" onClick={this.handleBack} disabled={activeStep === 0} style={{backgroundColor: "#ff8a65"}}>
                                                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                                                Back
                                            </Fab>
                                        </Grid>
                                        <Grid item>
                                            {submitBtn}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Flexbox>
                    </Flexbox>
                    <Snackbar
                        anchorOrigin={{
                            vertical: "top",
                            horizontal: "left"
                        }}
                        open={this.state.SnackOpen}
                        autoHideDuration={6000}
                        onClose={this.handleCloseSnack}
                        >
                        <MySnackbarContentWrapper
                            onClose={this.handleCloseSnack}
                            variant={this.state.SnackType}
                            message={this.state.SnackError}
                        />
                    </Snackbar>
                </div>
                {/* </MediaQuery>
                <MediaQuery query="(max-height: 567px)">
                    <Typography variant="h3">
                        nah B! this form only works in Portrait.
                    </Typography>
                </MediaQuery> */}
            </React.Fragment>
        );
    }
}

SwipeableTextMobileStepper.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const SwipeableTextMobileStepperStyled = withStyles(styles, { withTheme: true })(SwipeableTextMobileStepper);

const final = graphql(createDeal, {
    name: "createDeal",
    options: {
        refetchQueries: [
            'Deals'
        ]
    }
})(withApollo(SwipeableTextMobileStepperStyled));

export default graphql(createStore, {
    name: "createStore",
    options: {
        refetchQueries: [
            'Stores'
        ]
    }
})(withApollo(final));