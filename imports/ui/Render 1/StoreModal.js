import React from 'react';
import Modal from '@material-ui/core/Modal';
import { Typography } from '@material-ui/core';
import gql from "graphql-tag";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Query } from "react-apollo";
import IconButton from '@material-ui/core/IconButton';
import Favorite from '@material-ui/icons/Favorite';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Divider from '@material-ui/core/Divider';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Phone from '@material-ui/icons/Phone';
import Directions from '@material-ui/icons/Directions';
import Web from '@material-ui/icons/Web';
import Add from '@material-ui/icons/Add';
import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import CircularProgress from '@material-ui/core/CircularProgress';
import CloseIcon from '@material-ui/icons/Close';
import { withApollo } from "react-apollo";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import { Accounts } from "meteor/accounts-base";
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from './Snackbar';
import ReactResizeDetector from 'react-resize-detector';
import ArrowUp from '@material-ui/icons/KeyboardArrowUp';
import ArrowDown from '@material-ui/icons/KeyboardArrowDown';
import { graphql } from 'react-apollo';
import DealCardInfo from './DealCardInfo';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
    card: {
        borderRadius: 20 + "px",
        height: 200,
        width: 200
    },
    card2: {
        borderRadius: 20 + "px",
        height: 100,
        width: 200
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    title: {
        color: theme.palette.primary.light,
    },
      titleBar: {
        background:
          'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
});

const createFavorite = gql`
    mutation createFavorite($userId: String!, $StoreId: String!, $TimeStamp: String!) {
        createFavorite(userId: $userId, StoreId: $StoreId, TimeStamp: $TimeStamp) {
            _id
        }
    },
    
`;

const removeFavorite = gql`
    mutation removeFavorite($_id: String!, $userId: String!) {
        removeFavorite(_id: $_id, userId: $userId) {
            _id
        }
    },
`;

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

class StoreModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false,
            value: 0,
            id: "",
            dealId: '',
            bottom: false
        }
    };

    toggleDrawer = (open) => {
        this.setState({
          bottom: open,
        });
    };

    handleOpen = (_id2) => {
        this.setState({ open: true });
        this.setState({id: _id2});
        this.setState({dealId: ''});
    };

    handleClose = () => {
        this.setState({dealId: ''});
        this.props.action()
    };

    // callSnack = () => {
    //     this.props.snack({sType: this.state.sType, sError: this.state.sError})
    // };

    submitFavorite = StoreId => {
        // this.setState({userId, StoreId});
        const user = Accounts.userId();
        // console.log(user + " tttt " + StoreId)
        this.props
        .createFavorite({
            variables:{
                userId: user,
                StoreId: StoreId,
                TimeStamp: new Date()

            }
        });
        this.props.snack({sType: "success", sError: "Added to favorites"});
    };

    removeFavorites = _id => {
        // this.setState({userId, StoreId});
        const user = Accounts.userId();
        // console.log(user + " tttt " + StoreId)
        this.props
        .removeFavorite({
            variables:{
                _id: _id,
                userId: user
            }
        });
        this.props.snack({sType: "error", sError: "Removed favorite"});
    };

    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        return (
            <React.Fragment>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.props.openModal}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + 'vh'}}
                >
                    <div>
                        <Flexbox flexDirection="column" height="100vh">
                            <Flexbox flexDirection="column" height="100vh">
                                <Flexbox flexGrow={1}>
                                    <Query
                                    variables={{_id: this.props.id}}
                                    query={
                                        gql `
                                        query storeinfoQuery($_id: String!) {
                                            storeinfoQuery(_id: $_id) {
                                                _id
                                                StorePlaceId
                                                StoreName
                                                StorePhone
                                                StoreHours {
                                                close {
                                                    day
                                                    time
                                                    }
                                                open {
                                                    day
                                                    time
                                                    }
                                                }
                                                Website
                                                StoreAddress
                                                PriceLevel
                                                Photo
                                            }
                                            user {
                                                _id
                                            }
                                        }
                                        `
                                    }
                                    >
                                        {({ loading: loadingOne, error, data }) => {
                                        if (loadingOne) {
                                            return (
                                                <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                        <Grid container direction="column" alignItems="center" justify="center">
                                                            <Grid item xs={12}>
                                                                <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                    <CircularProgress/>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                </Flexbox>
                                            )
                                        }

                                        if (error) return <p>Error :(</p>;
                                            return data.storeinfoQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                <Grid item xs={12} sm={6} md={4} lg={3} key={_id2}>
                                                    <div>
                                                        <Card style={{ borderRadius: 0, width: 100 + "vw"}}>
                                                            <CardContent style={{padding: 0}}>
                                                                <Flexbox flexDirection="column" minHeight="100vh">
                                                                    <Flexbox element="header" height="150px">
                                                                        <div style={{height: 150, paddingBottom: 0, width: 100 + "vw"}}>
                                                                            
                                                                            <GridList className={classes.gridList} cols={2.5} cellHeight={150} id={StorePlaceId}>
                                                                                {Photo.map((ref) => (
                                                                                    <GridListTile key={ref} style={{padding: 0, paddingRight: 1, width: 40 + "%"}}>
                                                                                        <img src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&maxheight=200&photoreference=" + ref + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0"} alt={ref} />
                                                                                        <GridListTileBar
                                                                                        classes={{
                                                                                            root: classes.titleBar,
                                                                                            title: classes.title,
                                                                                        }}
                                                                                        />
                                                                                    </GridListTile>
                                                                                ))}
                                                                            </GridList>
                                                                            
                                                                            <MediaQuery query="(min-width: 768px)">
                                                                                <Flexbox flexDirection="column" >
                                                                                    <div style={{ textAlign: "right", position: "relative", bottom: 150, zIndex: 10}}>
                                                                                        <IconButton style={{ fontFamily: 'Raleway', color: "#FFFFFF" }} onClick={this.handleClose}>
                                                                                            <CloseIcon/>
                                                                                        </IconButton>
                                                                                    </div>
                                                                                </Flexbox>
                                                                            </MediaQuery>
                                                                            <div style={{position: "relative", bottom: 198, borderRadius: 0, background: "linear-gradient(to bottom, rgba(0,0,0,1) 0%,rgba(0,0,0,0) 100%)" }}>
                                                                                <Typography variant="overline" noWrap={true} style={{fontSize: 20, color: "#FFFFFF", paddingLeft: 10}}>
                                                                                    {StoreName}
                                                                                </Typography>
                                                                            </div>
                                                                            <div style={{position: "relative", bottom: 118}} >
                                                                                <Grid container direction="row" alignItems="center" spacing={24}>
                                                                                    <Grid item xs>
                                                                                        <div style={{textAlign: "center"}}>
                                                                                            <Fab size="small" style={{backgroundColor: "#fff", zIndex: 1500}}  onClick={() => sideScroll(document.getElementById(StorePlaceId),'left',25,100,10)}>
                                                                                                <ArrowLeft />
                                                                                            </Fab>
                                                                                        </div>
                                                                                    </Grid>
                                                                                    <Grid item xs>
                                                                                        
                                                                                    </Grid>
                                                                                    <Grid item xs>
                                                                                        <div style={{textAlign: "center"}}>
                                                                                            <Fab size="small" style={{backgroundColor: "#fff", zIndex: 1500}} onClick={() => sideScroll(document.getElementById(StorePlaceId),'right',10,150,10)}>
                                                                                                <ArrowRight />
                                                                                            </Fab>
                                                                                        </div>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </div>
                                                                        </div>
                                                                    </Flexbox>


                                                                    {/* asdf;ljalsdfj */}
                                                                    <Flexbox flexGrow={1}>
                                                                        <ReactResizeDetector handleWidth handleHeight>
                                                                            {(width, height) =>
                                                                                <div>
                                                                                    <div style={{width: 100 + "vw"}}>
                                                                                        <div style={{height: 100 + "%"}}>
                                                                                            {this.state.dealId === '' ? (
                                                                                                <div>
                                                                                                    <Query
                                                                                                        variables={{_id: this.props.dealId}}
                                                                                                        query={gql `
                                                                                                            query dealCardQuery($_id: String!) {
                                                                                                            dealCardQuery(_id: $_id) {
                                                                                                                _id
                                                                                                                name
                                                                                                                TypeCuisine
                                                                                                                TimeOfDay
                                                                                                                TimeOfDayTo
                                                                                                                FoodTitle
                                                                                                                PriceRange
                                                                                                                DealExpirationDate
                                                                                                                DealExpirationTime
                                                                                                                DealExpirationDateStart
                                                                                                                DealExpirationTimeStart
                                                                                                                LocationAddress
                                                                                                                StorePlaceId
                                                                                                                DealDetails
                                                                                                                StoreName
                                                                                                                TimeStamp
                                                                                                                HasCoupon
                                                                                                                DayWorks
                                                                                                                DealExp
                                                                                                                CouponCode
                                                                                                                CouponDetails
                                                                                                                s
                                                                                                                m
                                                                                                                tu
                                                                                                                w
                                                                                                                th
                                                                                                                f
                                                                                                                sa
                                                                                                                }
                                                                                                            }
                                                                                                        `}
                                                                                                    >
                                                                                                        {({ loading: loadingTwo, error, data }) => {
                                                                                                        const SnackTypeT = "error";
                                                                                                        const SnackError = "Required field is empty"
                                                                                                        if (loadingTwo) return (null);
                                                                                                        if (error) return `Error : ${error.message}`;
                                                                                                        if (data.dealCardQuery.length == 0) {
                                                                                                            return (
                                                                                                                <div style={{ overflow: "hidden"}}>
                                                                                                                    <Grid container alignItems="center" justify="center">
                                                                                                                        <Grid item>
                                                                                                                            <Typography variant="overline" style={{margin: 35}}>
                                                                                                                                No Deals Found
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                    </Grid>    
                                                                                                                </div>
                                                                                                            )
                                                                                                        }
                                                                                                            return data.dealCardQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, DealExpirationDateStart, DealExpirationTimeStart, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp, HasCoupon, DayWorks, DealExp, CouponCode, CouponDetails, s, m, tu, w, th, f, sa }) => (
                                                                                                                <div key={_id}>
                                                                                                                    <DealCardInfo height={height} name={name} _id={_id} TypeCuisine={TypeCuisine} TimeOfDay={TimeOfDay} FoodTitle={FoodTitle} PriceRange={PriceRange} DealExpirationDate={DealExpirationDate} DealExpirationTime={DealExpirationTime} DealExpirationDateStart={DealExpirationDateStart} DealExpirationTimeStart={DealExpirationTimeStart} LocationAddress={LocationAddress} StorePlaceId={StorePlaceId} DealDetails={DealDetails} StoreName={StoreName} TimeStamp={TimeStamp} HasCoupon={HasCoupon} DayWorks={DayWorks} DealExp={DealExp} CouponCode={CouponCode} CouponDetails={CouponDetails} s={s} m={m} tu={tu} w={w} th={th} f={f} sa={sa}/>
                                                                                                                </div>
                                                                                                            ));
                                                                                                        }}
                                                                                                    </Query>
                                                                                                </div>
                                                                                            ):(
                                                                                                <div>
                                                                                                    <Query
                                                                                                        variables={{_id: this.state.dealId}}
                                                                                                        query={gql `
                                                                                                            query dealCardQuery($_id: String!) {
                                                                                                            dealCardQuery(_id: $_id) {
                                                                                                                _id
                                                                                                                name
                                                                                                                TypeCuisine
                                                                                                                TimeOfDay
                                                                                                                TimeOfDayTo
                                                                                                                FoodTitle
                                                                                                                PriceRange
                                                                                                                DealExpirationDate
                                                                                                                DealExpirationTime
                                                                                                                DealExpirationDateStart
                                                                                                                DealExpirationTimeStart
                                                                                                                LocationAddress
                                                                                                                StorePlaceId
                                                                                                                DealDetails
                                                                                                                StoreName
                                                                                                                TimeStamp
                                                                                                                HasCoupon
                                                                                                                DayWorks
                                                                                                                DealExp
                                                                                                                CouponCode
                                                                                                                CouponDetails
                                                                                                                s
                                                                                                                m
                                                                                                                tu
                                                                                                                w
                                                                                                                th
                                                                                                                f
                                                                                                                sa
                                                                                                                }
                                                                                                            }
                                                                                                        `}
                                                                                                    >
                                                                                                        {({ loading: loadingTwo, error, data }) => {
                                                                                                        const SnackTypeT = "error";
                                                                                                        const SnackError = "Required field is empty"
                                                                                                        if (loadingTwo) return (null);
                                                                                                        if (error) return `Error : ${error.message}`;
                                                                                                        if (data.dealCardQuery.length == 0) {
                                                                                                            return (
                                                                                                                <div style={{ overflow: "hidden"}}>
                                                                                                                    <Grid container alignItems="center" justify="center">
                                                                                                                        <Grid item>
                                                                                                                            <Typography variant="overline" style={{margin: 35}}>
                                                                                                                                No Deals Found
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                    </Grid>    
                                                                                                                </div>
                                                                                                            )
                                                                                                        }
                                                                                                            return data.dealCardQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, DealExpirationDateStart, DealExpirationTimeStart, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp, HasCoupon, DayWorks, DealExp, CouponCode, CouponDetails, s, m, tu, w, th, f, sa }) => (
                                                                                                                <div key={_id}>
                                                                                                                    <DealCardInfo height={height} name={name} _id={_id} TypeCuisine={TypeCuisine} TimeOfDay={TimeOfDay} FoodTitle={FoodTitle} PriceRange={PriceRange} DealExpirationDate={DealExpirationDate} DealExpirationTime={DealExpirationTime} DealExpirationDateStart={DealExpirationDateStart} DealExpirationTimeStart={DealExpirationTimeStart} LocationAddress={LocationAddress} StorePlaceId={StorePlaceId} DealDetails={DealDetails} StoreName={StoreName} TimeStamp={TimeStamp} HasCoupon={HasCoupon} DayWorks={DayWorks} DealExp={DealExp} CouponCode={CouponCode} CouponDetails={CouponDetails} s={s} m={m} tu={tu} w={w} th={th} f={f} sa={sa}/>
                                                                                                                </div>
                                                                                                            ));
                                                                                                        }}
                                                                                                    </Query>
                                                                                                </div>
                                                                                            )}
                                                                                        </div>
                                                                                    </div>
                                                                                    <Modal
                                                                                        anchor="bottom"
                                                                                        open={this.state.bottom}
                                                                                        onClose={() => this.toggleDrawer(false)}
                                                                                        style={{height: 200, width: "100vw"}}
                                                                                        hideBackdrop={true}
                                                                                        disableBackdropClick={true}
                                                                                    >
                                                                                        <div style={{width: "100vw", height: (height + 150 + 130 + "px"), backgroundColor: "#FFFFFF", boxShadow: `0px -3px 8px #000000`}} >
                                                                                            
                                                                                        </div>
                                                                                    </Modal>
                                                                                </div>
                                                                            }
                                                                        </ReactResizeDetector>
                                                                    </Flexbox>
                                                                    <Flexbox element="footer">
                                                                    <div style={{width: 100 + "vw", height: 130, backgroundColor: "#37474f"}}>
                                                                                        <GridList className={classes.gridList} cols={2.5} cellHeight={20}>
                                                                                            <Query
                                                                                                variables={{StorePlaceId: StorePlaceId, address: StoreAddress}}
                                                                                                query={gql `
                                                                                                    query dealCardListQuery($StorePlaceId: String!, $address: String!) {
                                                                                                    dealCardListQuery(StorePlaceId: $StorePlaceId, address: $address) {
                                                                                                            _id
                                                                                                            name
                                                                                                            TypeCuisine
                                                                                                            TimeOfDay
                                                                                                            FoodTitle
                                                                                                            PriceRange
                                                                                                            DealExpirationDate
                                                                                                            DealExpirationTime
                                                                                                            LocationAddress
                                                                                                            StorePlaceId
                                                                                                            DealDetails
                                                                                                            StoreName
                                                                                                            TimeStamp
                                                                                                        }
                                                                                                    }
                                                                                                `}
                                                                                            >
                                                                                                {({ loading: loadingTwo, error, data }) => {
                                                                                                const SnackTypeT = "error";
                                                                                                const SnackError = "Required field is empty"
                                                                                                if (loadingTwo) return <p>Loading...</p>;
                                                                                                if (error) return `Error : ${error.message}`;
                                                                                                if (data.dealCardListQuery.length == 0) {
                                                                                                    return (
                                                                                                        <div style={{ overflow: "hidden"}}>
                                                                                                            <Grid container alignItems="center" justify="center">
                                                                                                                <Grid item>
                                                                                                                    <Typography variant="overline" style={{margin: 35}}>
                                                                                                                        No Deals Found
                                                                                                                    </Typography>
                                                                                                                </Grid>
                                                                                                            </Grid>    
                                                                                                        </div>
                                                                                                    )
                                                                                                }
                                                                                                    return data.dealCardListQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                                                                                        <div style={{padding: 15}} key={_id}>
                                                                                                        <Query
                                                                                                        variables={{StorePlaceId: StorePlaceId, StoreAddress: LocationAddress}}
                                                                                                        query={
                                                                                                            gql `
                                                                                                            query storeHomeQuery($StorePlaceId: String!, $StoreAddress: String!) {
                                                                                                                storeHomeQuery(StorePlaceId: $StorePlaceId, StoreAddress: $StoreAddress) {
                                                                                                                    _id
                                                                                                                    StorePlaceId
                                                                                                                    StoreName
                                                                                                                    StorePhone
                                                                                                                    StoreHours {
                                                                                                                    close {
                                                                                                                        day
                                                                                                                        time
                                                                                                                        }
                                                                                                                    open {
                                                                                                                        day
                                                                                                                        time
                                                                                                                        }
                                                                                                                    }
                                                                                                                    Website
                                                                                                                    StoreAddress
                                                                                                                    PriceLevel
                                                                                                                    Photo
                                                                                                                }
                                                                                                            }
                                                                                                            `
                                                                                                        }
                                                                                                    >
                                                                                                        {({ loading: loadingOne, error, data }) => {
                                                                                                        if (loadingOne) {
                                                                                                            return (
                                                                                                                <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                                                                                        <Grid container direction="column" alignItems="center" justify="center">
                                                                                                                            <Grid item xs={12}>
                                                                                                                                <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                                                                                    <CircularProgress/>
                                                                                                                                </div>
                                                                                                                            </Grid>
                                                                                                                        </Grid>
                                                                                                                </Flexbox>
                                                                                                            )
                                                                                                        }

                                                                                                        if (error) return <p>Error :(</p>;
                                                                                                            return data.storeHomeQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                                                                            <Card key={_id2} className={classes.card2} onClick={() => this.setState({dealId: _id})} style={{backgroundImage: "url(" + "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=" + Photo[0] + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0" + ")"}}>
                                                                                                                <CardContent style={{backgroundColor:  "rgba(18, 17, 16, 0.7)", paddingTop: 10}}>
                                                                                                                    <div style={{height: 100 }}>
                                                                                                                        <Flexbox flexDirection="column" height="50%">
                                                                                                                            <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                                                <Grid item xs={9}>
                                                                                                                                    <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}}>
                                                                                                                                        {name}
                                                                                                                                    </Typography>
                                                                                                                                </Grid>
                                                                                                                                <Grid item xs={3}>
                                                                                                                                    <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                                                        40 %
                                                                                                                                    </Typography>
                                                                                                                                </Grid>
                                                                                                                            </Grid>
                                                                                                                        </Flexbox>
                                                                                                                        <Flexbox flexDirection="column" height="20%">
                                                                                                                            <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                                                <Grid item xs={12}>
                                                                                                                                    <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                                                        4.5
                                                                                                                                    </Typography>
                                                                                                                                </Grid>
                                                                                                                            </Grid>
                                                                                                                        </Flexbox>
                                                                                                                    </div>
                                                                                                                </CardContent>
                                                                                                            </Card>
                                                                                                            
                                                                                                                ));
                                                                                                            }}
                                                                                                        </Query>
                                                                                                        
                                                                                                        </div>
                                                                                                    ));
                                                                                                }}
                                                                                            </Query>
                                                                                        </GridList>
                                                                                    </div>
                                                                    </Flexbox>
                                                                    <Flexbox element="footer" >
                                                                        <div style={{ paddingLeft: 10, paddingTop: 10, paddingRight: 10, paddingBottom: 10, width: 100 + "%", boxShadow: `0px -3px 8px #000000`}}>
                                                                            <Grid container alignItems="center" justify="space-evenly" direction="row">
                                                                                <Grid item>
                                                                                    { Accounts.userId() ? (
                                                                                        <Query
                                                                                            variables={{StoreId: _id2, userId: data.user._id}}
                                                                                            query={gql `
                                                                                                query Favorite($StoreId: String!, $userId: String!) {
                                                                                                favoritesHomeQuery(StoreId: $StoreId, userId: $userId) {
                                                                                                        _id
                                                                                                        StoreId
                                                                                                        userId
                                                                                                        TimeStamp
                                                                                                    }
                                                                                                }
                                                                                            `}
                                                                                            pollInterval={500}
                                                                                        >
                                                                                            {({ loading, error, data: data2 }) => {
                                                                                            if (loading) return (<IconButton>
                                                                                                                    <Favorite/>
                                                                                                                </IconButton>);
                                                                                            if (error) return `Error! ${error.message}`;
                                                                                            
                                                                                            return (
                                                                                                data2.favoritesHomeQuery.length != 0 ? (
                                                                                                    data2.favoritesHomeQuery.map(({ _id: _id3, StoreId, userId}) => (
                                                                                                    <div key={_id2}>
                                                                                                        <div>
                                                                                                            {((userId === Accounts.userId()) && (StoreId === _id2)) === true ? (
                                                                                                                <IconButton onClick={() => this.removeFavorites(_id3)}>
                                                                                                                    <Favorite style={{color: "#EC407A"}} />
                                                                                                                </IconButton>
                                                                                                            ):(null)}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    ))
                                                                                                ):(
                                                                                                    <IconButton onClick={() => this.submitFavorite(_id2)}>
                                                                                                        <Favorite />
                                                                                                    </IconButton>
                                                                                                )
                                                                                            )
                                                                                            }}
                                                                                        </Query>
                                                                                        ):(
                                                                                            <IconButton>
                                                                                                <Favorite/>
                                                                                            </IconButton>
                                                                                        )}
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Fab size="small" style={{backgroundColor: "#66BB6A", color: "#fff"}} onClick={() => window.location.href='tel:' + StorePhone}>
                                                                                        <Phone />
                                                                                    </Fab>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Fab size="small" style={{backgroundColor: "#42A5F5", color: "#fff"}} onClick={() => window.location.href="https://www.google.com/maps/search/?api=1&query=Eiffel%20Tower&query_place_id=" + StorePlaceId}>
                                                                                        <Directions />
                                                                                    </Fab>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Fab size="small" style={{backgroundColor: "#78909C", color: "#fff"}} onClick={() => window.location.href= Website}>
                                                                                        <Web />
                                                                                    </Fab>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    {this.state.bottom === true ? (
                                                                                        <IconButton onClick={() => this.toggleDrawer(false)}>
                                                                                            <ArrowDown/>
                                                                                        </IconButton>
                                                                                    ):(
                                                                                        <IconButton onClick={() =>this.toggleDrawer(true)}>
                                                                                            <ArrowUp/>
                                                                                        </IconButton>
                                                                                    )}
                                                                                </Grid>
                                                                            </Grid>
                                                                        </div>
                                                                    </Flexbox>
                                                                </Flexbox>
                                                            </CardContent>
                                                        </Card>
                                                    </div>
                                                </Grid>
                                            ));
                                        }}
                                    </Query>
                                </Flexbox>
                            </Flexbox>
                        </Flexbox>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

StoreModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(StoreModal);


const card2 = graphql(createFavorite, {
    name: "createFavorite",
    options: {
        refetchQueries: [
            'Favorites'
        ]
    }
})(withApollo(DealStyleWrap));

export default graphql(removeFavorite, {
    name: "removeFavorite",
    options: {
        refetchQueries: [
            'Favorites'
        ]
    }
})(withApollo(card2));;