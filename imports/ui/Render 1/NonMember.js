import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Typography } from '@material-ui/core';

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0
    }
});

class AdminWelcomePage extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 20}}>
                        <Typography variant="overline">Sorry, Our developers are baking currently.</Typography>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

AdminWelcomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdminWelcomePage);