import React, { Component } from 'react';
import { Accounts } from "meteor/accounts-base";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    paper: {
      position: 'absolute',
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
    },
    button: {
      margin: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    root: {
      backgroundColor: theme.palette.background.paper,
    },
    card: {
      minWidth: 275,
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 200,
    },
    menu: {
      width: 200,
    }
  });

class RegistorForm extends Component {
    constructor(props) {
        super(props);
    }

    registerUser = e => {
        e.preventDefault();        
        Accounts.createUser({
            email: this.email.value,
            password: this.password.value
        },
        error => {
            if(!error) {
                this.props.client.resetStore();
            }
            console.log(error);
        });
    };

    render() {
        const { classes, theme } = this.props;
        return (
            <React.Fragment>
                <form onSubmit={this.registerUser} className={classes.container} noValidate autoComplete="off">    
                    <Grid spacing={24} container direction="column" justify="center" alignItems="center">
                        <Grid item>    
                            <TextField
                            id="regemail"
                            label="User Name"
                            className={classes.textField}
                            margin="normal"
                            inputRef={input => this.email = input}
                            />
                        </Grid>    
                        <Grid item>
                            <TextField
                            id="regpassword"
                            label="Password"
                            className={classes.textField}
                            type="password"
                            margin="normal"
                            inputRef={input => this.password = input}
                            />
                        </Grid>
                    {/* <input type="email" ref={input => (this.email = input)} />
                    <input type="password" ref={input => (this.password = input)} /> */}
                        <Grid item>
                            <Button variant="contained" color="primary" type="submit">
                                Sign Up
                            </Button>
                        </Grid>
                    </Grid>
                </form>
            </React.Fragment>
        );
    }
}

const LoginModalWrapped = withStyles(styles)(RegistorForm);

export default LoginModalWrapped;