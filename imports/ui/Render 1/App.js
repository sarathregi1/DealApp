import React from 'react';
import HomePage from './Pages/HomePage';


const App = ({ loading, client }) => {
  if (loading) return null;
  return (
    <div>
      <HomePage client={client}/>
    </div>
  );  
};

export default App;