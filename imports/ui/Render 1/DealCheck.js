import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Flexbox from 'flexbox-react';
import Grid from '@material-ui/core/Grid';
import MediaQuery from 'react-responsive';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CheckIcon from '@material-ui/icons/Check';

const styles = theme => ({
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
    },
    card: {
        borderRadius: 20 + "px"
    },
});

const getDeal = gql `
    query Deals($StorePlaceId: String!, $address: String!, $DealType: String!) {
    dealCheckQuery(StorePlaceId: $StorePlaceId, address: $address, DealType: $DealType) {
            _id
            name
            TypeCuisine
            TimeOfDay
            FoodTitle
            PriceRange
            DealExpirationDate
            DealExpirationTime
            LocationAddress
            StorePlaceId
            DealDetails
            StoreName
            TimeStamp
        }
    }
`;
  
class DealCheckModal extends React.Component {
    state = {
      open: true,
    };
  
    handleCloseMain = () => {
        this.props.action()
    };

    handleCloseAll = () => {
        this.handleCloseMain()
        this.setState({ open: false });
    };

    handleClose = () => {
        this.setState({ open: false });
    };
  
    render() {
        const { classes } = this.props;
        const { activeStep } = this.props;
        const { StorePlaceId } = this.props;
        const { address } = this.props;
        const { DealType } = this.props
        return (
            <div>
                { activeStep === 1 && (StorePlaceId != '' || address != '') && DealType != '' ? (
                        <div>
                            <Query 
                                query={gql `
                                            query Deals($StorePlaceId: String!, $address: String!, $DealType: String!) {
                                            dealCheckQuery(StorePlaceId: $StorePlaceId, address: $address, DealType: $DealType) {
                                                    _id
                                                    name
                                                    TypeCuisine
                                                    TimeOfDay
                                                    FoodTitle
                                                    PriceRange
                                                    DealExpirationDate
                                                    DealExpirationTime
                                                    LocationAddress
                                                    StorePlaceId
                                                    DealDetails
                                                    StoreName
                                                    TimeStamp
                                                }
                                            }
                                        `} 
                                variables={{ StorePlaceId, address, DealType }}
                            >
                                {({ loading, error, data }) => {
                                    if (loading) return null;
                                    if (error) return `Error!: ${error}`;
                                    if (data.dealCheckQuery.length != 0) {
                                        return (
                                            <Modal
                                                aria-labelledby="simple-modal-title"
                                                aria-describedby="simple-modal-description"
                                                open={this.state.open}
                                                onClose={this.handleCloseAll}
                                                style={{width: 100 + 'vw', height: 100 + '%'}}
                                            >
                                                <div className={classes.paper} style={{width: 100 + 'vw', height: 100 + '%'}}>
                                                    <Flexbox flexDirection="column" height="100%">
                                                        <MediaQuery query="(min-width: 768px)">
                                                            <Flexbox flexDirection="column" height="10%">
                                                                <div style={{ textAlign: "right"}}>
                                                                    <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                                                        <CloseIcon/>
                                                                    </IconButton>
                                                                </div>
                                                            </Flexbox>
                                                        </MediaQuery>
                                                        <Flexbox flexDirection="column" height="90%">
                                                            <Flexbox flexDirection="column" height="10%" style={{margin: 10}}>
                                                                <Grid spacing={24} container justify="center" alignItems="center">
                                                                    <Grid item xs={12} style={{boxShadow: `0px 3px 8px #9E9E9E`, zIndex: 5}}>
                                                                        <Typography variant="h5" id="modal-title" style={{textAlign: "center"}}>
                                                                            Do these look similar to what you are posting?
                                                                        </Typography>
                                                                    </Grid>
                                                                </Grid>    
                                                            </Flexbox>
                                                            <Flexbox flexGrow={1} height="80%" style={{overflowY: "auto", overflowX: "hidden", marginTop: 10, backgroundColor: "#E3F2FD"}}>
                                                                <Grid container justify="center" alignItems="center" spacing={24} style={{overflowY: "auto", overflowX: "hidden"}}>
                                                                    {data.dealCheckQuery.map(({_id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                                                        <Grid item key={_id} xs={12} style={{padding: 20}}>
                                                                            <Card className={classes.card}>
                                                                                <CardContent>
                                                                                    <Typography variant="h6" id="modal-title">
                                                                                        {name}
                                                                                    </Typography>
                                                                                    <Typography variant="body1" id="simple-modal-description">
                                                                                        {LocationAddress}
                                                                                    </Typography>
                                                                                    <Typography variant="body1" id="simple-modal-description">
                                                                                        {DealDetails}
                                                                                    </Typography>
                                                                                </CardContent>
                                                                                <CardActions>
                                                                                    <Grid container direction="row" justify="flex-end" alignItems="center">
                                                                                        <Grid item>
                                                                                            <IconButton aria-label="Share" onClick={this.handleCloseAll}>
                                                                                                <CheckIcon />
                                                                                            </IconButton>
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                </CardActions>
                                                                            </Card>
                                                                        </Grid>
                                                                    ))}  
                                                                </Grid>
                                                            </Flexbox>
                                                        </Flexbox>
                                                        <MediaQuery query="(max-width: 768px)">
                                                            <Flexbox flexDirection="column" height="10%">
                                                                <MediaQuery query="(max-width: 767px)">
                                                                    <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>
                                                                        <CloseIcon/>
                                                                    </Button>
                                                                </MediaQuery>
                                                            </Flexbox> 
                                                        </MediaQuery>
                                                    </Flexbox>
                                                </div>
                                            </Modal>
                                        )
                                    }
                                    else {
                                        return (
                                            null
                                        )
                                    }
                                }}
                            </Query>
                        </div>
                ):(
                    null
                )}
            </div>
        );
    }
  }
  
  DealCheckModal.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  // We need an intermediary variable for handling the recursive nesting.
  const DealCheckModalWrapped = withStyles(styles)(DealCheckModal);
  
  export default DealCheckModalWrapped;