import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Accounts } from "meteor/accounts-base";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from './Snackbar';


function TabContainer({ children, dir }) {
    return (
      <div>
        {children}
      </div>
    );
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  card: {
    minWidth: 275,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  margin: {
    margin: theme.spacing.unit
  }
});

class LoginModal extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    login = e => {
        e.preventDefault();     
        Meteor.loginWithPassword(this.name.value, this.password.value, error => {
            if(!error) {
                // this.props.client.resetStore();
            }else {
                const SnackTypeT = "error";
                const ErorrType = error.error;
                if (ErorrType === 400) {
                    const SnackError = "Username and password required";
                    this.handleOpenSnack(SnackTypeT, SnackError);
                } else {
                    const SnackError = error.reason;
                    this.handleOpenSnack(SnackTypeT, SnackError);
                }
            }
        });
    };

    registerUser = e => {
        e.preventDefault();      
        Accounts.createUser({
            email: this.regemail.value,
            password: this.regpassword.value,
            profile: {role: "member"}
        },
        error => {
            if(!error) {
                // this.props.client.resetStore();
            }else {
                const SnackTypeT = "error";
                const ErorrType = error.error;
                if (ErorrType === 400) {
                    const SnackError = "Username and password required";
                    this.handleOpenSnack(SnackTypeT, SnackError);
                } else {
                    const SnackError = error.reason;
                    this.handleOpenSnack(SnackTypeT, SnackError);
                }
            }
        });
    };

    
    
    handleChange = (event, value) => {
        this.setState({ value });
    };

    handleChangeIndex = index => {
        this.setState({ value: index });
    };

    state = {
        open: false,
        value: 0,
        vertical: null,
        horizontal: null,
        SnackOpen: false,
        SnackType: "info",
        SnackError: null
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    

    handleClose = () => {
        this.setState({ open: false });
    };
    
    handleOpenSnack = (SnackTypeT, SnackError) => {
        this.setState({ SnackOpen: true, SnackType: SnackTypeT, SnackError });
    };

    handleCloseSnack = (event, reason) => {
        if (reason === "clickaway") {
          return;
        }
    
        this.setState({ SnackOpen: false });
    }


    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        return (
            <React.Fragment>
                {/* <MediaQuery query="(min-width: 1224px)"> */}
                    {/* <Button onClick={this.handleOpen} style={{ fontFamily: 'Raleway', color: "#FFFFFF", height: 64 }}>Login</Button> */}
                {/* </MediaQuery> */}
                {/* <MediaQuery query="(max-width: 1223px)">
                    <Button onClick={this.handleOpen} color="inherit" style={{ fontFamily: 'Raleway', textAlign: 'center', width: 100 + "%" }}>Login</Button>
                </MediaQuery> */}
                {/* <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + 'vh'}}
                > */}
                    <div style={{width: 100 + 'vw', height: 100 + 'vh'}} className={classes.paper}>
                        <Flexbox flexDirection="column" height="100vh">
                            <Flexbox flexDirection="column" height="90vh">
                                    <Flexbox flexGrow={1}>
                                        <Grid spacing={24} container justify="center" alignItems="center">
                                            <Grid item>
                                                <div>
                                                    <Card className={classes.card}>
                                                        <CardContent style={{padding: 0}}>
                                                            <div className={classes.root}>
                                                                <AppBar position="static" color="default">
                                                                    <Tabs
                                                                        value={this.state.value}
                                                                        onChange={this.handleChange}
                                                                        indicatorColor="primary"
                                                                        textColor="primary"
                                                                        fullWidth
                                                                        style={{ width: 100 + "%", textAlign: "center" }}
                                                                    >
                                                                        <Tab label="Login" />
                                                                        <Tab label="Sign Up" />
                                                                    </Tabs>
                                                                </AppBar>
                                                                <SwipeableViews
                                                                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                                                index={this.state.value}
                                                                onChangeIndex={this.handleChangeIndex}
                                                                >
                                                                <TabContainer dir={theme.direction}>
                                                                  <form onSubmit={this.login} className={classes.container} noValidate autoComplete="off">    
                                                                    <Grid spacing={24} container direction="column" justify="center" alignItems="center">
                                                                        <Grid item>    
                                                                            <TextField
                                                                            required
                                                                            id="name"
                                                                            label="User Name"
                                                                            className={classes.textField}
                                                                            margin="normal"
                                                                            inputRef={input => this.name = input}
                                                                            />
                                                                        </Grid>    
                                                                        <Grid item>
                                                                            <TextField
                                                                            required
                                                                            id="password"
                                                                            label="Password"
                                                                            className={classes.textField}
                                                                            type="password"
                                                                            margin="normal"
                                                                            inputRef={input => this.password = input}
                                                                            />
                                                                        </Grid>
                                                                      {/* <input type="email" ref={input => (this.email = input)} />
                                                                      <input type="password" ref={input => (this.password = input)} /> */}
                                                                        <Grid item>
                                                                            <Button variant="contained" color="primary" type="submit">
                                                                                Login User
                                                                            </Button>
                                                                        </Grid>
                                                                    </Grid>
                                                                  </form> 
                                                                </TabContainer>
                                                                <TabContainer dir={theme.direction}>
                                                                <form onSubmit={this.registerUser} className={classes.container} noValidate autoComplete="off">    
                                                                    <Grid spacing={24} container direction="column" justify="center" alignItems="center">
                                                                        <Grid item>    
                                                                            <TextField
                                                                            required
                                                                            id="regemail"
                                                                            label="User Name"
                                                                            className={classes.textField}
                                                                            margin="normal"
                                                                            inputRef={input => this.regemail = input}
                                                                            />
                                                                        </Grid>    
                                                                        <Grid item>
                                                                            <TextField
                                                                            required
                                                                            id="regpassword"
                                                                            label="Password"
                                                                            className={classes.textField}
                                                                            type="password"
                                                                            margin="normal"
                                                                            inputRef={input => this.regpassword = input}
                                                                            />
                                                                        </Grid>
                                                                    {/* <input type="email" ref={input => (this.email = input)} />
                                                                    <input type="password" ref={input => (this.password = input)} /> */}
                                                                        <Grid item>
                                                                            <Button variant="contained" color="primary" type="submit">
                                                                                Sign Up
                                                                            </Button>
                                                                        </Grid>
                                                                    </Grid>
                                                                </form>
                                                                </TabContainer>
                                                                </SwipeableViews>
                                                            </div>
                                                        </CardContent>
                                                    </Card>
                                                </div>
                                            </Grid>    
                                        </Grid>
                                    </Flexbox>
                                <Snackbar
                                    anchorOrigin={{
                                        vertical: "bottom",
                                        horizontal: "left"
                                    }}
                                    open={this.state.SnackOpen}
                                    autoHideDuration={6000}
                                    onClose={this.handleCloseSnack}
                                    >
                                    <MySnackbarContentWrapper
                                        onClose={this.handleCloseSnack}
                                        variant={this.state.SnackType}
                                        message={this.state.SnackError}
                                    />
                                </Snackbar>
                            </Flexbox>
                        </Flexbox>
                    </div>
                {/* </Modal> */}
            </React.Fragment>
        );
    }
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

LoginModal.propTypes = {
    classes: PropTypes.object.isRequired,
};


const LoginTabWrap = withStyles(styles, { withTheme: true })(LoginModal);

// We need an intermediary variable for handling the recursive nesting.
const LoginModalWrapped = withStyles(styles)(LoginTabWrap);

export default LoginModalWrapped;