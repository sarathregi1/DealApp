import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
import LocationSearch from './LocationSearch';
import LocationSearchMobile from './LocationSearchMobile';
import SearchIcon from '@material-ui/icons/Search';
import { Typography } from '@material-ui/core';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import Slider from '@material-ui/lab/Slider';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  card: {
    width: 50 + "vw",
    height: 90 + "vh"
  },
  cardmobile: {
    width: 80 + "vw"
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  input: {
    margin: theme.spacing.unit,
  },
  slider: {
    padding: '22px 0px',
  },
  sliderColor: {
    backgroundColor: "#26A69A",
  },
});

class DealAddModal extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        open: false,
        value: 0,
        searchOpen: false,
        searchValue: "Search",
        filterOpen: false
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
        this.setState({ searchValue: "Search"});
    };

    handleEnter = (event) => {
        if(event.key == 'Enter'){
            this.setState({searchOpen: false});
        }
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };
    
    handleSearchClose = () => {
        this.setState({ searchOpen: false , searchValue: "Search"});
    };
    
    handleFilterClose = () => {
        this.setState({ filterOpen: false });
    };


    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        const { value } = this.state;
        return (
            <React.Fragment>
                <Button onClick={this.handleOpen} style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} label="Favorites">
                    <Grid container justify="center" alignItems="center" direction="column">
                        <Grid item xs={12}>
                            <SearchIcon />
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="caption">
                                Search
                            </Typography>
                        </Grid>
                    </Grid>
                </Button>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div style={{width: 100 + 'vw', height: 100 + '%', backgroundColor: "rgba(0,0,0,.75)"}}>
                        <Flexbox flexDirection="column" height="100%">
                            <MediaQuery query="(min-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <div style={{ textAlign: "right"}}>
                                        <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                            <CloseIcon style={{color: "#FFFFFF"}}/>
                                        </IconButton>
                                    </div>
                                </Flexbox>
                            </MediaQuery>
                            <Flexbox flexDirection="column" height="60%">
                                <Flexbox flexGrow={1}>
                                    <Grid spacing={24} container justify="center" alignItems="center">
                                        <Grid item>
                                            <div style={{width: "100vw", height: "100vh"}}>
                                            </div>
                                        </Grid>    
                                    </Grid>
                                </Flexbox>
                            </Flexbox>
                            <Divider variant="middle" style={{marginBottom: 20, backgroundColor: "#FFFFFF"}}/>
                            <Flexbox flexDirection="column" height="10%">
                                <Grid container style={{paddingLeft: 16, paddingRight: 16}}>
                                    <Grid item xs={12}>
                                        <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                            Set Radius
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Grid container direction="row">
                                            <Grid item xs={2}>
                                                <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                    0 mi
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={8}>
                                                <Slider
                                                style={{position: "relative", bottom: 6, color: "#FFFFFF"}}
                                                classes={{ container: classes.slider, track: classes.sliderColor, thumb: classes.sliderColor }}
                                                value={value}
                                                aria-labelledby="label"
                                                onChange={this.handleChange}
                                                max={100}
                                                min={0}
                                                />
                                            </Grid>
                                            <Grid item xs={2} style={{textAlign: "right"}}>
                                                <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                    {value.toFixed(0)} mi
                                                 </Typography>
                                            </Grid>
                                        </Grid>
                                        
                                    </Grid>
                                </Grid>
                            </Flexbox>
                            <Divider variant="middle" style={{marginBottom: 20, backgroundColor: "#FFFFFF"}}/>
                            <Flexbox flexDirection="column" height="10%">
                                <Grid container direction="column" alignItems="center" justify="center">
                                    <Grid item xs={12}>
                                        <Fab variant="extended" style={{width: "80vw", backgroundColor: "#37474f"}} onClick={() => this.setState({filterOpen: true})}>
                                        {/* <div style={{width: "80vw", borderRadius: "50%", backgroundColor: "#37474f", borderRadius: 24, boxShadow: `0px 8px 50px #000000`, padding: 9}}> */}
                                            <Grid container alignItems="center" direction="column">
                                                <Grid item xs={12}>
                                                    <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                        Filters
                                                    </Typography>
                                                </Grid>
                                            </Grid>
                                        {/* </div> */}
                                        </Fab>
                                    </Grid>
                                </Grid>
                            </Flexbox>
                            <Flexbox flexDirection="column" height="10%">
                                <Grid container direction="column" alignItems="center" justify="center">
                                    <Grid item xs={12}>
                                        <div  style={{width: "80vw", borderRadius: "50%", backgroundColor: "#37474f", borderRadius: 24, boxShadow: `0px 8px 50px #000000`, padding: 9}}>
                                            <Grid container alignItems="center" direction="row" >
                                                <Grid item xs={10} onClick={() => this.setState({searchOpen: true})}>
                                                    {/* <InputBase className={classes.margin} defaultValue="Naked input" style={{color: "#FFFFFF", width: "70vw"}}/> */}
                                                    <Typography variant="caption" noWrap={true} style={{color: "#FFFFFF", fontSize: 15}}>
                                                        {this.state.searchValue}
                                                    </Typography>
                                                </Grid>
                                                <Grid item xs={2} style={{textAlign: "right"}}>
                                                    <SearchIcon style={{color: "#FFFFFF", fontSize: 15, position: "relative", top: 2}}/>
                                                </Grid>
                                            </Grid>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Flexbox>
                            <MediaQuery query="(max-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <MediaQuery query="(max-width: 767px)">
                                        <Button onClick={this.handleClose} style={{backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                    </MediaQuery>
                                </Flexbox> 
                            </MediaQuery>
                        </Flexbox>
                    </div>
                </Modal>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.searchOpen}
                onClose={this.handleSearchClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div style={{backgroundColor: "rgba(0,0,0)", width: "100vw", height: "100%"}}>
                        <Flexbox flexDirection="column" height="90%">
                            <Grid container alignItems="center" justify="center" direction="row">
                                <Grid item xs={12}>
                                    <Grid container direction="column" alignItems="center" justify="center">
                                        <Grid item xs={12} style={{paddingTop: "47%"}}>
                                            <Grid container direction="row">
                                                <Grid item xs={11}>
                                                    {this.state.searchValue === "Search" ? (
                                                        <InputBase onKeyPress={this.handleEnter} onKeyDown={e => {this.setState({ searchValue: e.target.value })}} className={classes.margin} autoFocus={true} placeholder="Search" style={{color: "#FFFFFF", fontSize: 25, width: "100%"}}/>
                                                    ):(
                                                        <InputBase onKeyPress={this.handleEnter} onKeyDown={e => {this.setState({ searchValue: e.target.value })}} className={classes.margin} autoFocus={true} defaultValue={this.state.searchValue} style={{color: "#FFFFFF", fontSize: 25, width: "100%"}}/>
                                                    )}
                                                </Grid>
                                                <Grid item xs={1}>
                                                    <IconButton onClick={() => this.setState({searchOpen: false})} style={{position: "relative", bottom: 5}}>
                                                        <SearchIcon style={{color: "#FFFFFF", fontSize: 25}}/>
                                                    </IconButton>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Flexbox>
                        <Flexbox flexDirection="column" height="10%">
                            <Grid container alignItems="center" justify="center">
                                <Grid item xs={12} style={{textAlign: "center"}}>
                                    {this.state.searchValue === "" ? (
                                        <IconButton onClick={() => this.setState({searchOpen: false, searchValue: "Search"})}>
                                            <CloseIcon style={{color: "#FFFFFF", paddingBottom: 10}}/>
                                        </IconButton>
                                    ):(
                                        <IconButton onClick={() => this.setState({searchOpen: false})}>
                                            <CloseIcon style={{color: "#FFFFFF", paddingBottom: 10}}/>
                                        </IconButton>
                                    )}
                                </Grid>
                            </Grid>
                        </Flexbox>
                    </div>
                </Modal>
                <Modal
                open={this.state.filterOpen}
                onClose={this.handleFilterClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div style={{backgroundColor: "rgba(0,0,0)", width: "100vw", height: "100%"}}>
                        <Flexbox flexDirection="column" height="40%">
                            
                        </Flexbox>
                        <Divider variant="middle" style={{ backgroundColor: "#FFFFFF"}}/>
                        <Flexbox flexDirection="column" height="50%">
                            <Grid container direction="column">
                                <Grid item xs={12}>
                                    ;ksdfjsjkasjks;jsjsj;s
                                </Grid>
                                <Grid item xs={12}>
                                    ;ksdfjsjkasjks;jsjsj;s
                                </Grid>
                                <Grid item xs={12}>
                                    ;ksdfjsjkasjks;jsjsj;s
                                </Grid>
                                <Grid item xs={12}>
                                    ;ksdfjsjkasjks;jsjsj;s
                                </Grid>
                                <Grid item xs={12}>
                                    ;ksdfjsjkasjks;jsjsj;s
                                </Grid>
                                <Grid item xs={12}>
                                    ;ksdfjsjkasjks;jsjsj;s
                                </Grid>
                            </Grid>
                        </Flexbox>
                        <Flexbox flexDirection="column" height="10%">
                            <Grid container alignItems="center" justify="center">
                                <Grid item xs={12} style={{textAlign: "center"}}>
                                    {this.state.searchValue === "" ? (
                                        <IconButton onClick={() => this.handleFilterClose()}>
                                            <CloseIcon style={{color: "#FFFFFF", paddingBottom: 10}}/>
                                        </IconButton>
                                    ):(
                                        <IconButton onClick={() => this.handleFilterClose()}>
                                            <CloseIcon style={{color: "#FFFFFF", paddingBottom: 10}}/>
                                        </IconButton>
                                    )}
                                </Grid>
                            </Grid>
                        </Flexbox>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

DealAddModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(DealAddModal);

// const DealModalWrapped = withStyles(styles)(DealStyleWrap);

export default DealStyleWrap