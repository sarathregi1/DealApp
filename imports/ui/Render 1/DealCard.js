import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Flexbox from 'flexbox-react';
import Add from '@material-ui/icons/Add';
import CircularProgress from '@material-ui/core/CircularProgress';
import Phone from '@material-ui/icons/Phone';
import Directions from '@material-ui/icons/Directions';
import Web from '@material-ui/icons/Web';
import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import ArrowUp from '@material-ui/icons/KeyboardArrowUp';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import MediaQuery from 'react-responsive';
import Button from '@material-ui/core/Button';
import ReactResizeDetector from 'react-resize-detector';
import Favorite from '@material-ui/icons/Favorite';
import StoreModal from './StoreModal';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
    card: {
        borderRadius: 20 + "px",
        height: 200,
        width: 200
    },
    card2: {
        borderRadius: 20 + "px",
        height: 100,
        width: 200
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
    },
      gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
      title: {
        color: theme.palette.primary.light,
    },
      titleBar: {
        background:
          'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
})

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

class HomePageQuery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {open: false, id: ""}
    }

    modalOpen = (_id2) => {
        this.setState({ open: true });
        this.setState({id: _id2})
    };

    handleOpen = (_id2) => {
        this.setState({ open: true });
        this.setState({id: _id2})
    };

    handleClose = () => {
        this.setState({ open: false });
    };
    
    render() {
        const { classes, theme } = this.props;
        return (
            <div>
                <div className={classes.root} >
                    <GridList className={classes.gridList} cols={2.5} cellHeight={20}>
                        <Query
                            query={gql `
                                query Deals {
                                deals {
                                        _id
                                        name
                                        TypeCuisine
                                        TimeOfDay
                                        FoodTitle
                                        PriceRange
                                        DealExpirationDate
                                        DealExpirationTime
                                        LocationAddress
                                        StorePlaceId
                                        DealDetails
                                        StoreName
                                        TimeStamp
                                    }
                                }
                            `}
                            pollInterval={500}
                        >
                            {({ loading: loadingTwo, error, data }) => {
                            const SnackTypeT = "error";
                            const SnackError = "Required field is empty"
                            if (loadingTwo) return <p>Loading...</p>;
                            if (error) return `Error : ${error.message}`;
                            if (data.deals.length == 0) {
                                return (
                                    <div style={{ overflow: "hidden"}}>
                                        <Grid container alignItems="center" justify="center">
                                            <Grid item>
                                                <Typography variant="overline" style={{margin: 35}}>
                                                    No Deals Found
                                                </Typography>
                                            </Grid>
                                        </Grid>    
                                    </div>
                                )
                            }
                                return data.deals.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                    <div style={{padding: 15}} key={_id}>
                                    <Query
                                    variables={{StorePlaceId: StorePlaceId, StoreAddress: LocationAddress}}
                                    query={
                                        gql `
                                        query storeHomeQuery($StorePlaceId: String!, $StoreAddress: String!) {
                                            storeHomeQuery(StorePlaceId: $StorePlaceId, StoreAddress: $StoreAddress) {
                                                _id
                                                StorePlaceId
                                                StoreName
                                                StorePhone
                                                StoreHours {
                                                close {
                                                    day
                                                    time
                                                    }
                                                open {
                                                    day
                                                    time
                                                    }
                                                }
                                                Website
                                                StoreAddress
                                                PriceLevel
                                                Photo
                                            }
                                        }
                                        `
                                    }
                                    pollInterval={500}
                                >
                                    {({ loading: loadingOne, error, data }) => {
                                    if (loadingOne) {
                                        return (
                                            <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                    <Grid container direction="column" alignItems="center" justify="center">
                                                        <Grid item xs={12}>
                                                            <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                <CircularProgress/>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                            </Flexbox>
                                        )
                                    }

                                    if (error) return <p>Error :(</p>;
                                        return data.storeHomeQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                        <Card key={_id2} className={classes.card} onClick={() => this.modalOpen(_id2)} style={{backgroundImage: "url(" + "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=" + Photo[0] + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0" + ")"}}>
                                            <CardContent style={{backgroundColor:  "rgba(18, 17, 16, 0.7)"}}>
                                                <div style={{height: 200 }}>
                                                    <Flexbox flexDirection="column" height="50%">
                                                        <Grid container direction="row" alignItems="stretch" justify="center">
                                                            <Grid item xs={9}>
                                                                <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}}>
                                                                    {name}
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={3}>
                                                                <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                    40 %
                                                                </Typography>
                                                            </Grid>
                                                        </Grid>
                                                    </Flexbox>
                                                    <Flexbox flexDirection="column" height="20%">
                                                        <Grid container direction="row" alignItems="stretch" justify="center">
                                                            <Grid item xs={12}>
                                                                <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}} noWrap={true}>
                                                                    {StoreName}
                                                                </Typography>
                                                            </Grid>
                                                        </Grid>
                                                    </Flexbox>
                                                    <Flexbox flexDirection="column" height="20%" style={{marginTop: 10}}>
                                                        <Grid container direction="row" alignItems="stretch" justify="center">
                                                            <Grid item xs={10}>
                                                                <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                    4.5
                                                                </Typography>
                                                            </Grid>
                                                            <Grid item xs={2}>
                                                                <Add style={{color: "#FFFFFF"}}/>
                                                            </Grid>
                                                        </Grid>
                                                    </Flexbox>
                                                </div>
                                            </CardContent>
                                        </Card>
                                        
                                            ));
                                        }}
                                    </Query>
                                    
                                    </div>
                                ));
                            }}
                        </Query>
                    </GridList>
                </div>
                <div>
                    <StoreModal id={this.state.id} openModal={this.state.open} action={() => this.handleClose()}/>
                </div>
                {/* <div>
                    <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.state.open}
                    onClose={this.handleClose}
                    style={{width: 100 + 'vw', height: 100 + 'vh'}}
                    >
                        <React.Fragment>
                            {/* <Button onClick={this.handleOpen} className={classes.button} style={{color: "#EF5350", fontFamily: 'Roboto-Light, sans-serif-light'}}>
                                More Info <Add style={{fontWeight: 100}}/>
                            </Button> 
                                <div>
                                    <Flexbox flexDirection="column" height="100vh">
                                        <Flexbox flexDirection="column" height="90vh">
                                            <Flexbox flexGrow={1}>
                                                <Query
                                                variables={{_id: this.state.id}}
                                                query={
                                                    gql `
                                                    query storeinfoQuery($_id: String!) {
                                                        storeinfoQuery(_id: $_id) {
                                                            _id
                                                            StorePlaceId
                                                            StoreName
                                                            StorePhone
                                                            StoreHours
                                                            Website
                                                            StoreAddress
                                                            PriceLevel
                                                            Photo
                                                        }
                                                        user {
                                                            _id
                                                        }
                                                    }
                                                    `
                                                }
                                                pollInterval={500}
                                                >
                                                    {({ loading: loadingOne, error, data }) => {
                                                    if (loadingOne) {
                                                        return (
                                                            <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                                    <Grid container direction="column" alignItems="center" justify="center">
                                                                        <Grid item xs={12}>
                                                                            <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                                <CircularProgress/>
                                                                            </div>
                                                                        </Grid>
                                                                    </Grid>
                                                            </Flexbox>
                                                        )
                                                    }

                                                    if (error) return <p>Error :(</p>;
                                                        return data.storeinfoQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                            <Grid item xs={12} sm={6} md={4} lg={3} key={_id2}>
                                                                <div>
                                                                    <Card style={{ borderRadius: 0, width: 100 + "vw"}}>
                                                                        <CardContent style={{padding: 0}}>
                                                                            <Flexbox flexDirection="column" minHeight="90vh">
                                                                                <Flexbox element="header" height="150px">
                                                                                    <div style={{height: 150, paddingBottom: 0, width: 100 + "vw"}}>
                                                                                        <GridList className={classes.gridList} cols={2.5} cellHeight={150} id={StorePlaceId}>
                                                                                            {Photo.map((ref) => (
                                                                                            <GridListTile key={ref} style={{padding: 0, paddingRight: 1, width: 40 + "%"}}>
                                                                                                <img src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&maxheight=200&photoreference=" + ref + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0"} alt={ref} />
                                                                                                <GridListTileBar
                                                                                                classes={{
                                                                                                    root: classes.titleBar,
                                                                                                    title: classes.title,
                                                                                                }}
                                                                                                />
                                                                                            </GridListTile>
                                                                                            ))}
                                                                                        </GridList>
                                                                                        {/* <MediaQuery query="(min-width: 768px)">
                                                                                            <Flexbox flexDirection="column" height="10vh">
                                                                                                <div style={{ textAlign: "right", position: "relative", bottom: "150"}}>
                                                                                                    <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                                                                                        <CloseIcon/>
                                                                                                    </IconButton>
                                                                                                </div>
                                                                                            </Flexbox>
                                                                                        </MediaQuery> 
                                                                                        <div style={{position: "relative", bottom: 150, borderRadius: 0, background: "linear-gradient(to bottom, rgba(0,0,0,1) 0%,rgba(0,0,0,0) 100%)" }}>
                                                                                            <Typography variant="overline" noWrap={true} style={{fontSize: 20, color: "#FFFFFF", paddingLeft: 10}}>
                                                                                                {StoreName}
                                                                                            </Typography>
                                                                                        </div>
                                                                                        <div style={{position: "relative", bottom: 70}} >
                                                                                            <Grid container direction="row" alignItems="center" spacing={24}>
                                                                                                <Grid item xs>
                                                                                                    <div style={{textAlign: "center"}}>
                                                                                                        <Fab mini style={{backgroundColor: "#fff"}}  onClick={() => sideScroll(document.getElementById(StorePlaceId),'left',25,100,10)}>
                                                                                                            <ArrowLeft />
                                                                                                        </Fab>
                                                                                                    </div>
                                                                                                </Grid>
                                                                                                <Grid item xs>
                                                                                                    
                                                                                                </Grid>
                                                                                                <Grid item xs>
                                                                                                    <div style={{textAlign: "center"}}>
                                                                                                        <Fab mini style={{backgroundColor: "#fff"}} onClick={() => sideScroll(document.getElementById(StorePlaceId),'right',10,150,10)}>
                                                                                                            <ArrowRight />
                                                                                                        </Fab>
                                                                                                    </div>
                                                                                                </Grid>
                                                                                            </Grid>
                                                                                        </div>
                                                                                    </div>
                                                                                </Flexbox>


                                                                                {/* asdf;ljalsdfj 


                                                                                <Flexbox flexGrow={1} style={{height: 100 + "%"}}>
                                                                                <ReactResizeDetector handleWidth handleHeight>
                                                                                {(width, height) => 
                                                                                
                                                                                    <Grid container direction="column" justify="flex-start" alignItems="center" style={{overflow: "hidden", height: 100 + "%" }}>
                                                                                        <Grid item> 
                                                                                            <div style={{width: 100 + "vw", height: (height - 130 + "px")}}>
                                                                                                <div style={{padding: 30, height: 100 + "%"}}>
                                                                                                alsdfjlasdfj {height} 
                                                                                                </div>
                                                                                            </div>
                                                                                        </Grid>
                                                                                        <Grid item>
                                                                                            <div style={{width: 100 + "vw", height: 130, backgroundColor: "#37474f"}}>
                                                                                            <GridList className={classes.gridList} cols={2.5} cellHeight={20}>
                                                                                                <Query
                                                                                                    query={gql `
                                                                                                        query Deals {
                                                                                                        deals {
                                                                                                                _id
                                                                                                                name
                                                                                                                TypeCuisine
                                                                                                                TimeOfDay
                                                                                                                FoodTitle
                                                                                                                PriceRange
                                                                                                                DealExpirationDate
                                                                                                                DealExpirationTime
                                                                                                                LocationAddress
                                                                                                                StorePlaceId
                                                                                                                DealDetails
                                                                                                                StoreName
                                                                                                                TimeStamp
                                                                                                            }
                                                                                                        }
                                                                                                    `}
                                                                                                    pollInterval={500}
                                                                                                >
                                                                                                    {({ loading: loadingTwo, error, data }) => {
                                                                                                    const SnackTypeT = "error";
                                                                                                    const SnackError = "Required field is empty"
                                                                                                    if (loadingTwo) return <p>Loading...</p>;
                                                                                                    if (error) return `Error : ${error.message}`;
                                                                                                    if (data.deals.length == 0) {
                                                                                                        return (
                                                                                                            <div style={{ overflow: "hidden"}}>
                                                                                                                <Grid container alignItems="center" justify="center">
                                                                                                                    <Grid item>
                                                                                                                        <Typography variant="overline" style={{margin: 35}}>
                                                                                                                            No Deals Found
                                                                                                                        </Typography>
                                                                                                                    </Grid>
                                                                                                                </Grid>    
                                                                                                            </div>
                                                                                                        )
                                                                                                    }
                                                                                                        return data.deals.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                                                                                            <div style={{padding: 15}} key={_id}>
                                                                                                            <Query
                                                                                                            variables={{StorePlaceId: StorePlaceId, StoreAddress: LocationAddress}}
                                                                                                            query={
                                                                                                                gql `
                                                                                                                query storeHomeQuery($StorePlaceId: String!, $StoreAddress: String!) {
                                                                                                                    storeHomeQuery(StorePlaceId: $StorePlaceId, StoreAddress: $StoreAddress) {
                                                                                                                        _id
                                                                                                                        StorePlaceId
                                                                                                                        StoreName
                                                                                                                        StorePhone
                                                                                                                        StoreHours
                                                                                                                        Website
                                                                                                                        StoreAddress
                                                                                                                        PriceLevel
                                                                                                                        Photo
                                                                                                                    }
                                                                                                                }
                                                                                                                `
                                                                                                            }
                                                                                                            pollInterval={500}
                                                                                                        >
                                                                                                            {({ loading: loadingOne, error, data }) => {
                                                                                                            if (loadingOne) {
                                                                                                                return (
                                                                                                                    <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                                                                                            <Grid container direction="column" alignItems="center" justify="center">
                                                                                                                                <Grid item xs={12}>
                                                                                                                                    <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                                                                                        <CircularProgress/>
                                                                                                                                    </div>
                                                                                                                                </Grid>
                                                                                                                            </Grid>
                                                                                                                    </Flexbox>
                                                                                                                )
                                                                                                            }

                                                                                                            if (error) return <p>Error :(</p>;
                                                                                                                return data.storeHomeQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                                                                                <Card key={_id2} className={classes.card2} style={{backgroundImage: "url(" + "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=" + Photo[0] + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0" + ")"}}>
                                                                                                                    <CardContent style={{backgroundColor:  "rgba(18, 17, 16, 0.7)", paddingTop: 10}}>
                                                                                                                        <div style={{height: 100 }}>
                                                                                                                            <Flexbox flexDirection="column" height="50%">
                                                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                                                    <Grid item xs={9}>
                                                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}}>
                                                                                                                                            {name}
                                                                                                                                        </Typography>
                                                                                                                                    </Grid>
                                                                                                                                    <Grid item xs={3}>
                                                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                                                            40 %
                                                                                                                                        </Typography>
                                                                                                                                    </Grid>
                                                                                                                                </Grid>
                                                                                                                            </Flexbox>
                                                                                                                            <Flexbox flexDirection="column" height="20%">
                                                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                                                    <Grid item xs={12}>
                                                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                                                            4.5
                                                                                                                                        </Typography>
                                                                                                                                    </Grid>
                                                                                                                                </Grid>
                                                                                                                            </Flexbox>
                                                                                                                        </div>
                                                                                                                    </CardContent>
                                                                                                                </Card>
                                                                                                                
                                                                                                                    ));
                                                                                                                }}
                                                                                                            </Query>
                                                                                                            
                                                                                                            </div>
                                                                                                        ));
                                                                                                    }}
                                                                                                </Query>
                                                                                            </GridList>
                                                                                            </div>
                                                                                        </Grid>
                                                                                    </Grid>
                                                                                    }
                                                                                    </ReactResizeDetector>
                                                                                </Flexbox>
                                                                                <Flexbox element="footer" >
                                                                                    <div style={{ paddingLeft: 10, paddingTop: 10, paddingRight: 10, paddingBottom: 10, width: 100 + "%", boxShadow: `0px -3px 8px #000000`}}>
                                                                                        {/* <Grid container alignItems="center" justify="flex-start" style={{marginBottom: 10, marginTop: 20}}>
                                                                                            <Grid item>
                                                                                                <Typography variant="h5" style={{fontFamily: 'Roboto-Light, sans-serif-light' }}>
                                                                                                    {StoreName}
                                                                                                </Typography>
                                                                                            </Grid>
                                                                                        </Grid> 
                                                                                        <Grid container alignItems="center" justify="space-evenly" direction="row">
                                                                                            <Grid item>
                                                                                                <IconButton>
                                                                                                    <Favorite/>
                                                                                                </IconButton>
                                                                                            </Grid>
                                                                                            <Grid item>
                                                                                                <Fab mini style={{backgroundColor: "#66BB6A", color: "#fff"}} onClick={() => window.location.href='tel:' + StorePhone}>
                                                                                                    <Phone />
                                                                                                </Fab>
                                                                                            </Grid>
                                                                                            <Grid item>
                                                                                                <Fab mini style={{backgroundColor: "#42A5F5", color: "#fff"}} onClick={() => window.location.href="https://www.google.com/maps/search/?api=1&query=Eiffel%20Tower&query_place_id=" + StorePlaceId}>
                                                                                                    <Directions />
                                                                                                </Fab>
                                                                                            </Grid>
                                                                                            <Grid item>
                                                                                                <Fab mini style={{backgroundColor: "#78909C", color: "#fff"}} onClick={() => window.location.href= Website}>
                                                                                                    <Web />
                                                                                                </Fab>
                                                                                            </Grid>
                                                                                            <Grid item>
                                                                                                <IconButton>
                                                                                                    <ArrowUp/>
                                                                                                </IconButton>
                                                                                            </Grid>
                                                                                        </Grid>
                                                                                    </div>
                                                                                </Flexbox>
                                                                            </Flexbox>
                                                                        </CardContent> 
                                                                        {/* <CardActions style={{paddingTop: 0}}>
                                                                            <Grid container direction="row" justify="flex-end" alignItems="center">
                                                                                <Grid item>
                                                                                    { Accounts.userId() ? (
                                                                                    <Query
                                                                                        variables={{StoreId: _id, userId: data.user._id}}
                                                                                        query={gql `
                                                                                            query Favorite($StoreId: String!, $userId: String!) {
                                                                                            favoritesHomeQuery(StoreId: $StoreId, userId: $userId) {
                                                                                                    _id
                                                                                                    StoreId
                                                                                                    userId
                                                                                                    TimeStamp
                                                                                                }
                                                                                            }
                                                                                        `}
                                                                                        pollInterval={500}
                                                                                    >
                                                                                        {({ loading, error, data: data2 }) => {
                                                                                        if (loading) return "Loading...";
                                                                                        if (error) return `Error! ${error.message}`;
                                                                                        
                                                                                        return (
                                                                                            data2.favoritesHomeQuery.length != 0 ? (
                                                                                                data2.favoritesHomeQuery.map(({ _id: _id2, StoreId, userId}) => (
                                                                                                <div key={_id}>
                                                                                                    <div>
                                                                                                        {((userId === Accounts.userId()) && (StoreId === _id)) === true ? (
                                                                                                            <IconButton>
                                                                                                                <Favorite style={{color: "#EC407A"}} onClick={() => this.removeFavorites(_id2)}/>
                                                                                                            </IconButton>
                                                                                                        ):(null)}
                                                                                                    </div>
                                                                                                </div>
                                                                                                ))
                                                                                            ):(
                                                                                                <IconButton>
                                                                                                    <Favorite onClick={() => this.submitFavorite(_id)}/>
                                                                                                </IconButton>
                                                                                            )
                                                                                        )
                                                                                        }}
                                                                                    </Query>
                                                                                    ):(
                                                                                        <IconButton>
                                                                                            <Favorite/>
                                                                                        </IconButton>
                                                                                    )}
                                                                                </Grid>
                                                                            </Grid>
                                                                    </Card>
                                                                </div>
                                                            </Grid>
                                                        ));
                                                    }}
                                                </Query>
                                            </Flexbox>
                                        </Flexbox>
                                        <MediaQuery query="(max-width: 768px)">
                                            <Flexbox flexDirection="column" height="10vh">
                                                <MediaQuery query="(max-width: 767px)">
                                                    <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                                </MediaQuery>
                                            </Flexbox> 
                                        </MediaQuery>
                                    </Flexbox>
                                </div>
                        </React.Fragment>
                    </Modal>
                </div> */}
            </div>
        )
    }
}

HomePageQuery.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(HomePageQuery);
