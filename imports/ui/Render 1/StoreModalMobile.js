import React from 'react';
import Modal from '@material-ui/core/Modal';
import { Typography } from '@material-ui/core';
import gql from "graphql-tag";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Query } from "react-apollo";
import IconButton from '@material-ui/core/IconButton';
import Favorite from '@material-ui/icons/Favorite';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Divider from '@material-ui/core/Divider';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Directions from '@material-ui/icons/Directions';
import Web from '@material-ui/icons/Web';
import Add from '@material-ui/icons/Add';
import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import CircularProgress from '@material-ui/core/CircularProgress';
import CloseIcon from '@material-ui/icons/Close';
import { withApollo } from "react-apollo";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import { Accounts } from "meteor/accounts-base";
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from './Snackbar';
import ReactResizeDetector from 'react-resize-detector';
import ArrowUp from '@material-ui/icons/KeyboardArrowUp';
import ArrowDown from '@material-ui/icons/KeyboardArrowDown';
import { graphql } from 'react-apollo';
import classnames from 'classnames';
import DealCardInfo from './DealCardInfo';
import Fab from '@material-ui/core/Fab';
import Fade from '@material-ui/core/Fade';
import Star from '@material-ui/icons/StarBorder';
import Phone from '@material-ui/icons/Phone';
import LocationOn from '@material-ui/icons/LocationOn';
import RightArrow from '@material-ui/icons/KeyboardArrowRight';
import LinearProgress from '@material-ui/core/LinearProgress';
import FilterIcon from '@material-ui/icons/FilterList';
import moment from 'moment';
import { Sunday } from '../DailyIcons/Icons';

const styles = theme => ({
    card: {
        borderRadius: 20 + "px",
        height: 200,
        width: 200
    },
    card2: {
        borderRadius: 20 + "px",
        height: 100,
        width: 200
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
          'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
        marginLeft: 'auto',
        [theme.breakpoints.up('sm')]: {
            marginRight: -8,
        },
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    paper: {
        padding: theme.spacing.unit,
        textAlign: 'center',
        color: theme.palette.text.secondary,
        paddingLeft: 0
    },
    bar: {
        backgroundColor: "#FFB300"
    },
    barSecondary: {
        backgroundColor: "#FFECB3"
    },
    purpleAvatar: {
        margin: 10,
        color: '#fff',
        backgroundColor: "#ba3",
        width: 40,
        height: 40,
    },
});

const star5 = "/5star.svg";
const star4 = "/4star.svg";
const star3 = "/3star.svg";
const star2 = "/2star.svg";
const star1 = "/1star.svg";

const createFavorite = gql`
    mutation createFavorite($userId: String!, $StoreId: String!, $TimeStamp: String!) {
        createFavorite(userId: $userId, StoreId: $StoreId, TimeStamp: $TimeStamp) {
            _id
        }
    },
    
`;

const removeFavorite = gql`
    mutation removeFavorite($_id: String!, $userId: String!) {
        removeFavorite(_id: $_id, userId: $userId) {
            _id
        }
    },
`;

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}

class StoreModal extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        open: false,
        value: 0,
        id: "",
        dealId: '',
        bottom: false,
        expanded: true,
        drawer: false,
        dealListHeight: 48,
        displayDealList: "none"
    };

    toggleDrawer = () => {
        if (this.state.bottom == false) {
            this.setState({bottom: true});
        }
        if (this.state.bottom == true) {
            setTimeout(() => {
                this.setState({bottom: false});
            }, 100);
        }
        if (this.state.drawer == false) {
           setTimeout(() => {
                this.setState({drawer: true});
            }, 100);
        }
        if (this.state.drawer == true) {
            this.setState({drawer: false});
        }
    };
    

    handleExpandClick = () => {
        if (this.state.expanded == true) {
            this.setState(state => ({ expanded: !state.expanded, dealListHeight: 178, displayDealList: "contents" }));
        }
        if (this.state.expanded == false) {
            this.setState(state => ({ expanded: !state.expanded, dealListHeight: 48, displayDealList: "none" }));
        }
        
    };

    handleOpen = (_id2) => {
        this.setState({ open: true });
        this.setState({id: _id2});
    };

    handleClose = () => {
        this.setState({bottom: false, dealId: '', expanded: true, drawer: false, dealListHeight: 48, displayDealList: "none"});
        this.props.action();
        
    };

    submitFavorite = StoreId => {
        // this.setState({userId, StoreId});
        const user = Accounts.userId();
        // console.log(user + " tttt " + StoreId)
        this.props
        .createFavorite({
            variables:{
                userId: user,
                StoreId: StoreId,
                TimeStamp: new Date()

            }
        });
        this.props.snack({sType: "success", sError: "Added to favorites"});
    };

    removeFavorites = _id => {
        // this.setState({userId, StoreId});
        const user = Accounts.userId();
        // console.log(user + " tttt " + StoreId)
        this.props
        .removeFavorite({
            variables:{
                _id: _id,
                userId: user
            }
        });
        this.props.snack({sType: "error", sError: "Removed favorite"});
    };

    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        return (
            <React.Fragment>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.props.openModal}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div>
                        <Flexbox flexDirection="column" height="100%">
                            <Flexbox flexDirection="column" height="90vh">
                                <Flexbox flexGrow={1}>
                                    <Query
                                    variables={{_id: this.props.id}}
                                    query={
                                        gql `
                                        query storeinfoQuery($_id: String!) {
                                            storeinfoQuery(_id: $_id) {
                                                _id
                                                StorePlaceId
                                                StoreName
                                                StorePhone
                                                StoreHours {
                                                close {
                                                    day
                                                    time
                                                    }
                                                open {
                                                    day
                                                    time
                                                    }
                                                }
                                                Website
                                                StoreAddress
                                                PriceLevel
                                                Photo
                                            }
                                            user {
                                                _id
                                            }
                                        }
                                        `
                                    }
                                    >
                                        {({ loading: loadingOne, error, data }) => {
                                        if (loadingOne) {
                                            return (
                                                <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                        <Grid container direction="column" alignItems="center" justify="center">
                                                            <Grid item xs={12}>
                                                                <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                    <CircularProgress/>
                                                                </div>
                                                            </Grid>
                                                        </Grid>
                                                </Flexbox>
                                            )
                                        }

                                        if (error) return <p>Error :(</p>;
                                            return data.storeinfoQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                <Grid item xs={12} sm={6} md={4} lg={3} key={_id2}>
                                                    <div>
                                                        <Card style={{ borderRadius: 0, width: 100 + "vw"}}>
                                                            <CardContent style={{padding: 0}}>
                                                                <Flexbox flexDirection="column" height="90vh">
                                                                    <Flexbox element="header">
                                                                        <div style={{paddingBottom: 0, width: 100 + "vw"}}>
                                                                            
                                                                            {/* <GridList className={classes.gridList} cols={2.5} cellHeight={150} id={StorePlaceId} style={{ boxShadow: `0px 3px 10px rgb(0, 0, 0, .2)`, backgroundColor: "#37474f"}}>
                                                                                {Photo.map((ref) => (
                                                                                    <GridListTile key={ref} style={{padding: 0, paddingRight: 1, width: 40 + "%"}}>
                                                                                        <img src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&maxheight=200&photoreference=" + ref + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0"} alt={ref} />
                                                                                        <GridListTileBar
                                                                                        classes={{
                                                                                            root: classes.titleBar,
                                                                                            title: classes.title,
                                                                                        }}
                                                                                        />
                                                                                    </GridListTile>
                                                                                ))}
                                                                            </GridList> */}

                                                                            <div style={{ borderRadius: 0, background: "linear-gradient(to bottom, rgba(0,0,0,1) 0%,rgba(0,0,0,0) 100%)" }}>
                                                                                <Grid container direction="row">
                                                                                    <Grid item xs={6}>
                                                                                        <Typography variant="overline" noWrap={true} style={{fontSize: 20, color: "#FFFFFF", paddingLeft: 10}}>
                                                                                            {StoreName}
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                    <Grid item xs={6}>
                                                                                        <Typography variant="overline" noWrap={true} style={{color: "#FFFFFF", textAlign: "right", paddingRight: 10, marginTop: "5%", fontSize: ".9rem"}}>
                                                                                            <span style={{position: "relative", top: 1}}><Star style={{fontSize: ".9rem"}}/></span> 4.5 | <span style={{color: "#FFAB00"}}>Closing Soon</span>
                                                                                        </Typography>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </div>
                                                                            {/* <div style={{position: "relative", bottom: 70}} >
                                                                                <Grid container direction="row" alignItems="center" spacing={24}>
                                                                                    <Grid item xs>
                                                                                        <div style={{textAlign: "center"}}>
                                                                                            <Fab size="small" style={{backgroundColor: "#fff", zIndex: 1500}}  onClick={() => sideScroll(document.getElementById(StorePlaceId),'left',25,100,10)}>
                                                                                                <ArrowLeft />
                                                                                            </Fab>
                                                                                        </div>
                                                                                    </Grid>
                                                                                    <Grid item xs>
                                                                                        
                                                                                    </Grid>
                                                                                    <Grid item xs>
                                                                                        <div style={{textAlign: "center"}}>
                                                                                            <Fab size="small" style={{backgroundColor: "#fff", zIndex: 1500}} onClick={() => sideScroll(document.getElementById(StorePlaceId),'right',10,150,10)}>
                                                                                                <ArrowRight />
                                                                                            </Fab>
                                                                                        </div>
                                                                                    </Grid>
                                                                                </Grid>
                                                                            </div> */}
                                                                        </div>
                                                                    </Flexbox>

                                                                    {/* asdf;ljalsdfj */}

                                                                    <Flexbox flexGrow={1}>
                                                                        <ReactResizeDetector handleWidth handleHeight>
                                                                            {(width, height) =>
                                                                                <div>
                                                                                    <div style={{width: 100 + "vw"}}>
                                                                                        <div>
                                                                                        {this.state.dealId == '' ? (
                                                                                                <div>
                                                                                                    <Query
                                                                                                        variables={{_id: this.props.dealId}}
                                                                                                        query={gql `
                                                                                                            query dealCardQuery($_id: String!) {
                                                                                                            dealCardQuery(_id: $_id) {
                                                                                                                _id
                                                                                                                name
                                                                                                                TypeCuisine
                                                                                                                TimeOfDay
                                                                                                                TimeOfDayTo
                                                                                                                FoodTitle
                                                                                                                PriceRange
                                                                                                                DealExpirationDate
                                                                                                                DealExpirationTime
                                                                                                                DealExpirationDateStart
                                                                                                                DealExpirationTimeStart
                                                                                                                LocationAddress
                                                                                                                StorePlaceId
                                                                                                                DealDetails
                                                                                                                StoreName
                                                                                                                TimeStamp
                                                                                                                HasCoupon
                                                                                                                DayWorks
                                                                                                                DealExp
                                                                                                                CouponCode
                                                                                                                CouponDetails
                                                                                                                s
                                                                                                                m
                                                                                                                tu
                                                                                                                w
                                                                                                                th
                                                                                                                f
                                                                                                                sa
                                                                                                                }
                                                                                                            }
                                                                                                        `}
                                                                                                    >
                                                                                                        {({ loading: loadingTwo, error, data }) => {
                                                                                                        const SnackTypeT = "error";
                                                                                                        const SnackError = "Required field is empty"
                                                                                                        if (loadingTwo) return (null);
                                                                                                        if (error) return `Error : ${error.message}`;
                                                                                                        if (data.dealCardQuery.length == 0) {
                                                                                                            return (
                                                                                                                <div style={{ overflow: "hidden"}}>
                                                                                                                    <Grid container alignItems="center" justify="center">
                                                                                                                        <Grid item>
                                                                                                                            <Typography variant="overline" style={{margin: 35}}>
                                                                                                                                No Deals Found
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                    </Grid>    
                                                                                                                </div>
                                                                                                            )
                                                                                                        }
                                                                                                            return data.dealCardQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, DealExpirationDateStart, DealExpirationTimeStart, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp, HasCoupon, DayWorks, DealExp, CouponCode, CouponDetails, s, m, tu, w, th, f, sa }) => (
                                                                                                                <div key={_id}>
                                                                                                                    {/* <Typography variant="h6" >
                                                                                                                        Deal Type: {name}
                                                                                                                    </Typography> */}
                                                                                                                    <DealCardInfo height={height} name={name} _id={_id} TypeCuisine={TypeCuisine} TimeOfDay={TimeOfDay} FoodTitle={FoodTitle} PriceRange={PriceRange} DealExpirationDate={DealExpirationDate} DealExpirationTime={DealExpirationTime} DealExpirationDateStart={DealExpirationDateStart} DealExpirationTimeStart={DealExpirationTimeStart} LocationAddress={LocationAddress} StorePlaceId={StorePlaceId} DealDetails={DealDetails} StoreName={StoreName} TimeStamp={TimeStamp} HasCoupon={HasCoupon} DayWorks={DayWorks} DealExp={DealExp} CouponCode={CouponCode} CouponDetails={CouponDetails} s={s} m={m} tu={tu} w={w} th={th} f={f} sa={sa}/>
                                                                                                                </div>
                                                                                                            ));
                                                                                                        }}
                                                                                                    </Query>
                                                                                                </div>
                                                                                            ):(
                                                                                                <div>
                                                                                                    <Query
                                                                                                        variables={{_id: this.state.dealId}}
                                                                                                        query={gql `
                                                                                                            query dealCardQuery($_id: String!) {
                                                                                                            dealCardQuery(_id: $_id) {
                                                                                                                _id
                                                                                                                name
                                                                                                                TypeCuisine
                                                                                                                TimeOfDay
                                                                                                                TimeOfDayTo
                                                                                                                FoodTitle
                                                                                                                PriceRange
                                                                                                                DealExpirationDate
                                                                                                                DealExpirationTime
                                                                                                                DealExpirationDateStart
                                                                                                                DealExpirationTimeStart
                                                                                                                LocationAddress
                                                                                                                StorePlaceId
                                                                                                                DealDetails
                                                                                                                StoreName
                                                                                                                TimeStamp
                                                                                                                HasCoupon
                                                                                                                DayWorks
                                                                                                                DealExp
                                                                                                                CouponCode
                                                                                                                CouponDetails
                                                                                                                s
                                                                                                                m
                                                                                                                tu
                                                                                                                w
                                                                                                                th
                                                                                                                f
                                                                                                                sa
                                                                                                                }
                                                                                                            }
                                                                                                        `}
                                                                                                    >
                                                                                                        {({ loading: loadingTwo, error, data }) => {
                                                                                                        const SnackTypeT = "error";
                                                                                                        const SnackError = "Required field is empty"
                                                                                                        if (loadingTwo) return (null);
                                                                                                        if (error) return `Error : ${error.message}`;
                                                                                                        if (data.dealCardQuery.length == 0) {
                                                                                                            return (
                                                                                                                <div style={{ overflow: "hidden"}}>
                                                                                                                    <Grid container alignItems="center" justify="center">
                                                                                                                        <Grid item>
                                                                                                                            <Typography variant="overline" style={{margin: 35}}>
                                                                                                                                No Deals Found
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                    </Grid>    
                                                                                                                </div>
                                                                                                            )
                                                                                                        }
                                                                                                            return data.dealCardQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, DealExpirationDateStart, DealExpirationTimeStart, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp, HasCoupon, DayWorks, DealExp, CouponCode, CouponDetails, s, m, tu, w, th, f, sa}) => (
                                                                                                                <div key={_id}>
                                                                                                                    {/* <Typography variant="h6" >
                                                                                                                        Deal Type: {name}
                                                                                                                    </Typography> */}
                                                                                                                    <DealCardInfo height={height} name={name} _id={_id} TypeCuisine={TypeCuisine} TimeOfDay={TimeOfDay} FoodTitle={FoodTitle} PriceRange={PriceRange} DealExpirationDate={DealExpirationDate} DealExpirationTime={DealExpirationTime} DealExpirationDateStart={DealExpirationDateStart} DealExpirationTimeStart={DealExpirationTimeStart} LocationAddress={LocationAddress} StorePlaceId={StorePlaceId} DealDetails={DealDetails} StoreName={StoreName} TimeStamp={TimeStamp} HasCoupon={HasCoupon} DayWorks={DayWorks} DealExp={DealExp} CouponCode={CouponCode} CouponDetails={CouponDetails} s={s} m={m} tu={tu} w={w} th={th} f={f} sa={sa}/>
                                                                                                                </div>
                                                                                                            ));
                                                                                                        }}
                                                                                                    </Query>
                                                                                                </div>
                                                                                            )}
                                                                                        </div>
                                                                                    </div>
                                                                                    {/* <Fade in={this.state.drawer} timeout={3000}> */}
                                                                                        <div>
                                                                                        <Modal
                                                                                            anchor="bottom"
                                                                                            open={this.state.bottom}
                                                                                            onClose={this.toggleDrawer}
                                                                                            style={{height: 200, width: "100vw"}}
                                                                                            hideBackdrop={true}
                                                                                            disableBackdropClick={true}
                                                                                        >
                                                                                            <div style={{width: "100vw", height: (height + 53  + "px"), backgroundColor: "#FFFFFF", boxShadow: `0px -3px 8px #000000`}} >
                                                                                                <div style={{height: (height + 53  + "px"), overflowY: "scroll", overflowX: "hidden"}}>
                                                                                                    <Grid container direction="column" style={{padding: 20, paddingBottom: 0}}>
                                                                                                        <Grid item xs={12}>
                                                                                                            <Typography variant="overline" style={{fontSize: 26, textAlign: "left", lineHeight: "1"}}>
                                                                                                                {StoreName}
                                                                                                            </Typography>
                                                                                                        </Grid>
                                                                                                        <Grid item xs={12}>
                                                                                                            <Typography variant="overline" noWrap={true} style={{ fontSize: ".75rem", position: "relative", bottom: 5}}>
                                                                                                                <span style={{position: "relative", top: 1}}><Star style={{fontSize: ".75rem"}}/></span> 4.5 | <span style={{color: "#FFAB00"}}>Closing Soon</span>
                                                                                                            </Typography>
                                                                                                        </Grid>
                                                                                                        <Grid item xs={12} style={{paddingBottom: 5}}>
                                                                                                            <Grid container direction="row">
                                                                                                                <Grid item xs={1}>
                                                                                                                    <Phone style={{fontSize: 15, position: "relative", top: 2, color: "rgba(0, 0, 0, 0.54)"}}/>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={11}>
                                                                                                                    <Typography variant="caption" style={{fontSize: 15}}>
                                                                                                                        {StorePhone}
                                                                                                                    </Typography>
                                                                                                                </Grid>
                                                                                                            </Grid>
                                                                                                        </Grid>
                                                                                                        <Grid item xs={12} style={{paddingBottom: 16}}>
                                                                                                            <Grid container direction="row">
                                                                                                                <Grid item xs={1}>
                                                                                                                    <LocationOn style={{fontSize: 15, position: "relative", top: 2, color: "rgba(0, 0, 0, 0.54)"}}/>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={11}>
                                                                                                                    <Typography variant="caption" style={{fontSize: 15}}>
                                                                                                                        {StoreAddress}
                                                                                                                    </Typography>
                                                                                                                </Grid>
                                                                                                            </Grid>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                    <Divider variant="middle"/>
                                                                                                    <Grid container direction="column" style={{paddingRight: 16, paddingLeft: 16}}>
                                                                                                        <Grid item xs={12}>
                                                                                                            <ExpansionPanel style={{boxShadow: "none"}}>
                                                                                                                <ExpansionPanelSummary style={{paddingRight: 0, paddingLeft: 0}}>
                                                                                                                    {/* <Typography variant="overline" style={{fontSize: 15, lineHeight: 1}}>Store Hours</Typography> */}
                                                                                                                    <Grid container direction="row" style={{paddingRight: 0}}>
                                                                                                                        <Grid item xs={11}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "left"}}>
                                                                                                                                Store Hours
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={1} style={{textAlign: "right"}}>
                                                                                                                            <ExpandMoreIcon style={{fontSize: 15, position: "relative", right: 5}}/>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </ExpansionPanelSummary>
                                                                                                                <ExpansionPanelDetails>
                                                                                                                    <Typography variant="caption" style={{fontSize: 15}}>
                                                                                                                        {/* {StoreHours[0]} */}
                                                                                                                        {
                                                                                                                            StoreHours.map((i) =>
                                                                                                                                <div key={i.open.day + Math.random()}>
                                                                                                                                    {i.open.day == 0 ? (
                                                                                                                                        "Sunday:     "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 1 ? (
                                                                                                                                        "Monday:     "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 2 ? (
                                                                                                                                        "Tuesday:    "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 3 ? (
                                                                                                                                        "Wednesday:  "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 4 ? (
                                                                                                                                        "Thursday:   "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 5 ? (
                                                                                                                                        "Friday:     "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 6 ? (
                                                                                                                                        "Saturday:   "
                                                                                                                                    ):(null)}
                                                                                                                                    {moment(i.open.time, "HH:mm").format("hh:mm a")} - {moment(i.close.time, "HH:mm").format("hh:mm a")}
                                                                                                                                </div>
                                                                                                                            )
                                                                                                                        }
                                                                                                                    </Typography>
                                                                                                                </ExpansionPanelDetails>
                                                                                                            </ExpansionPanel>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                    <Divider variant="middle"/>
                                                                                                    <Grid container direction="column" style={{paddingRight: 16, paddingLeft: 16}}>
                                                                                                        <Grid item xs={12} style={{textAlign: "center"}}>
                                                                                                            <Button style={{borderRadius: 0, width: "100%", height: 48, paddingLeft: 0, paddingRight: 0}}>
                                                                                                                <Grid container direction="row">
                                                                                                                    <Grid item xs={11} >
                                                                                                                        <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "left"}}>
                                                                                                                            More Info
                                                                                                                        </Typography>
                                                                                                                    </Grid>
                                                                                                                    <Grid item xs={1} style={{textAlign: "right"}}>
                                                                                                                        <RightArrow style={{fontSize: 15, position: "relative", right: 5}}/>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                            </Button>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                    <Divider variant="middle"/>
                                                                                                    <Grid container direction="row" style={{paddingRight: 16, paddingLeft: 16, paddingTop: 16}}>
                                                                                                        <Grid item xs={12}>
                                                                                                            <Typography variant="overline" style={{fontSize: 18, lineHeight: 1, textAlign: "center"}}>
                                                                                                                Reviews
                                                                                                            </Typography>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                    <Grid container direction="row" style={{padding: 16, paddingRight: 0}}>
                                                                                                        <Grid item xs={7} style={{borderRight: "1px solid #ccc", paddingRight: 10}}>
                                                                                                            <Grid container direction="column">
                                                                                                                <Grid item xs={12}>
                                                                                                                    <Grid container direction="row">
                                                                                                                        <Grid item xs={3}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "center", color: "rgba(0, 0, 0, 0.54)"}}>
                                                                                                                                5 <span style={{position: "relative", top: 2}}><img src={star5} style={{width: 15}} /></span>
                                                                                                                            </Typography> 
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={9} className={classes.paper}>
                                                                                                                            <LinearProgress variant="determinate" value={50} classes={{barColorPrimary: classes.bar, colorPrimary: classes.barSecondary}} style={{backgroundColor: "#FFECB3"}}/>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={12}>
                                                                                                                    <Grid container direction="row">
                                                                                                                        <Grid item xs={3}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "center", color: "rgba(0, 0, 0, 0.54)"}}>
                                                                                                                                4 <span style={{position: "relative", top: 2}}><img src={star4} style={{width: 15}} /></span>
                                                                                                                            </Typography> 
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={9} className={classes.paper}>
                                                                                                                            <LinearProgress variant="determinate" value={50} classes={{barColorPrimary: classes.bar, colorPrimary: classes.barSecondary}} style={{backgroundColor: "#FFECB3"}}/>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={12}>
                                                                                                                    <Grid container direction="row">
                                                                                                                        <Grid item xs={3}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "center", color: "rgba(0, 0, 0, 0.54)"}}>
                                                                                                                                3 <span style={{position: "relative", top: 2}}><img src={star3} style={{width: 15}} /></span>
                                                                                                                            </Typography> 
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={9} className={classes.paper}>
                                                                                                                            <LinearProgress variant="determinate" value={50} classes={{barColorPrimary: classes.bar, colorPrimary: classes.barSecondary}} style={{backgroundColor: "#FFECB3"}}/>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={12}>
                                                                                                                    <Grid container direction="row">
                                                                                                                        <Grid item xs={3}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "center", color: "rgba(0, 0, 0, 0.54)"}}>
                                                                                                                                2 <span style={{position: "relative", top: 2}}><img src={star2} style={{width: 15}} /></span>
                                                                                                                            </Typography> 
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={9} className={classes.paper}>
                                                                                                                            <LinearProgress variant="determinate" value={50} classes={{barColorPrimary: classes.bar, colorPrimary: classes.barSecondary}} style={{backgroundColor: "#FFECB3"}}/>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={12}>
                                                                                                                    <Grid container direction="row">
                                                                                                                        <Grid item xs={3}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 15, lineHeight: 1, textAlign: "center", color: "rgba(0, 0, 0, 0.54)"}}>
                                                                                                                                1 <span style={{position: "relative", top: 2}}><img src={star1} style={{width: 15}} /></span>
                                                                                                                            </Typography> 
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={9} className={classes.paper}>
                                                                                                                            <LinearProgress variant="determinate" value={50} classes={{barColorPrimary: classes.bar, colorPrimary: classes.barSecondary}} style={{backgroundColor: "#FFECB3"}}/>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                            </Grid>
                                                                                                        </Grid>
                                                                                                        <Grid item xs={5}>
                                                                                                            <Grid container direction="row" style={{paddingTop: 30.5}}>
                                                                                                                <Grid item xs={12}>
                                                                                                                    <Grid container direction="column">
                                                                                                                        <Grid item xs={12} style={{textAlign: "center"}}>
                                                                                                                            <span><img src={star4} style={{width: 50}} /></span>
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={12}>
                                                                                                                            <Typography variant="overline" style={{fontSize: 50, lineHeight: 1, textAlign: "center", color: "rgba(0, 0, 0, 0.54)"}}>
                                                                                                                                4.5
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                            </Grid>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                    <Grid container direction="column">
                                                                                                        <Grid xs={12} item>
                                                                                                            <ExpansionPanel style={{boxShadow: "none"}}>
                                                                                                                <ExpansionPanelSummary style={{paddingRight: 0, paddingLeft: 0}}>
                                                                                                                    {/* <Typography variant="overline" style={{fontSize: 15, lineHeight: 1}}>Store Hours</Typography> */}
                                                                                                                    <Grid container direction="row" style={{paddingRight: 0}}>
                                                                                                                        <Grid item xs={12} style={{textAlign: "right"}}>
                                                                                                                            <Button>
                                                                                                                                Filters <FilterIcon style={{padding: 7, fontSize: 15, position: "relative", right: 5}}/>
                                                                                                                            </Button>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </ExpansionPanelSummary>
                                                                                                                <ExpansionPanelDetails>
                                                                                                                    <Typography variant="caption" style={{fontSize: 15}}>
                                                                                                                        {/* {StoreHours[0]} */}
                                                                                                                        {
                                                                                                                            StoreHours.map((i) =>
                                                                                                                                <div key={i.open.day + Math.random()}>
                                                                                                                                    {i.open.day == 0 ? (
                                                                                                                                        "Sunday:     "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 1 ? (
                                                                                                                                        "Monday:     "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 2 ? (
                                                                                                                                        "Tuesday:    "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 3 ? (
                                                                                                                                        "Wednesday:  "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 4 ? (
                                                                                                                                        "Thursday:   "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 5 ? (
                                                                                                                                        "Friday:     "
                                                                                                                                    ):(null)}
                                                                                                                                    {i.open.day == 6 ? (
                                                                                                                                        "Saturday:   "
                                                                                                                                    ):(null)}
                                                                                                                                    {moment(i.open.time, "HH:mm").format("hh:mm a")} - {moment(i.close.time, "HH:mm").format("hh:mm a")}
                                                                                                                                </div>
                                                                                                                            )
                                                                                                                        }
                                                                                                                    </Typography>
                                                                                                                </ExpansionPanelDetails>
                                                                                                            </ExpansionPanel>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                    <Grid container direction="column" style={{padding: 16}}>
                                                                                                        <Grid item xs={12}>
                                                                                                            <Grid container direction="row">
                                                                                                                <Grid item xs={3}>
                                                                                                                    <Avatar className={classes.purpleAvatar}>OP</Avatar>
                                                                                                                </Grid>
                                                                                                                <Grid item xs={9}>
                                                                                                                    <Grid container direction="column">
                                                                                                                        <Grid item xs={12}>
                                                                                                                            <Grid container direction="row">
                                                                                                                                <Grid item xs={6}>
                                                                                                                                    <Grid container direction="column">
                                                                                                                                        <Grid item xs={12}>
                                                                                                                                            <Typography variant="h6" style={{fontSize: 15, lineHeight: 1.2}}>
                                                                                                                                                name
                                                                                                                                            </Typography>
                                                                                                                                        </Grid>
                                                                                                                                        <Grid item xs={12}>
                                                                                                                                            <Typography variant="h6" style={{fontSize: 10, color: "rgba(0,0,0,.54)"}}>
                                                                                                                                                12/14/18
                                                                                                                                            </Typography>
                                                                                                                                        </Grid>
                                                                                                                                    </Grid>
                                                                                                                                </Grid>
                                                                                                                                <Grid item xs={6}>
                                                                                                                                    <Grid item xs={12} style={{textAlign: "right"}}>
                                                                                                                                        <Typography variant="h6" style={{fontSize: 15, lineHeight: 1, color: "rgba(0,0,0,.54)"}}>
                                                                                                                                            5 <span><img src={star5} style={{width: 15, position: "relative", top: 1}} /></span>
                                                                                                                                        </Typography> 
                                                                                                                                    </Grid>
                                                                                                                                </Grid>
                                                                                                                            </Grid>
                                                                                                                        </Grid>
                                                                                                                        <Grid item xs={12} style={{paddingTop: 10}}>
                                                                                                                            <Typography variant="caption" style={{fontSize: 15}}>
                                                                                                                                al;sdfjlasdfl; jklasjdfljaslkdfj l;ajsd;lfjl;asdjf asljf 
                                                                                                                            </Typography>
                                                                                                                        </Grid>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                            </Grid>
                                                                                                        </Grid>
                                                                                                    </Grid>
                                                                                                </div>
                                                                                            </div>
                                                                                        </Modal>
                                                                                        </div>
                                                                                    {/* </Fade> */}
                                                                                    
                                                                                </div>
                                                                            }
                                                                        </ReactResizeDetector>
                                                                    </Flexbox>
                                                                    <Flexbox element="footer" maxHeight="178px">
                                                                        {/* <Card style={{borderRadius: 0, backgroundColor: "#37474f"}}>
                                                                            <CardActions style={{width: "100vw", padding: 0}}>
                                                                                <IconButton
                                                                                    style={{color: "#FFFFFF"}}
                                                                                    className={classnames(classes.expand, {
                                                                                    [classes.expandOpen]: this.state.expanded,
                                                                                    })}
                                                                                    onClick={this.handleExpandClick}
                                                                                    aria-expanded={this.state.expanded}
                                                                                    aria-label="Show more"
                                                                                >
                                                                                    <ExpandMoreIcon />
                                                                                </IconButton>
                                                                            </CardActions>
                                                                        </Card> */}
                                                                        {/* <Slide in={this.state.expanded} direction="up" mountOnEnter unmountOnExit> */}
                                                                        <div style={{width: 100 + "vw", height: this.state.dealListHeight, backgroundColor: "#37474f"}}>
                                                                            <Card style={{borderRadius: 0, backgroundColor: "#37474f"}}>
                                                                                <CardActions style={{width: "100vw", padding: 0, paddingLeft: 10, paddingRight: 10}}>
                                                                                    <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                        More deals at this location.
                                                                                    </Typography>
                                                                                    <IconButton
                                                                                        style={{color: "#FFFFFF"}}
                                                                                        className={classnames(classes.expand, {
                                                                                        [classes.expandOpen]: this.state.expanded,
                                                                                        })}
                                                                                        onClick={this.handleExpandClick}
                                                                                        aria-expanded={this.state.expanded}
                                                                                        aria-label="Show more"
                                                                                    >
                                                                                        <ExpandMoreIcon />
                                                                                    </IconButton>
                                                                                </CardActions>
                                                                            </Card>
                                                                            <div style={{display: this.state.displayDealList}}>
                                                                            <GridList className={classes.gridList} cols={2.5} cellHeight={20}>
                                                                                <Query
                                                                                    variables={{StorePlaceId: StorePlaceId, address: StoreAddress}}
                                                                                    query={gql `
                                                                                        query dealCardListQuery($StorePlaceId: String!, $address: String!) {
                                                                                        dealCardListQuery(StorePlaceId: $StorePlaceId, address: $address) {
                                                                                                _id
                                                                                                name
                                                                                                TypeCuisine
                                                                                                TimeOfDay
                                                                                                FoodTitle
                                                                                                PriceRange
                                                                                                DealExpirationDate
                                                                                                DealExpirationTime
                                                                                                LocationAddress
                                                                                                StorePlaceId
                                                                                                DealDetails
                                                                                                StoreName
                                                                                                TimeStamp
                                                                                            }
                                                                                        }
                                                                                    `}
                                                                                >
                                                                                    {({ loading: loadingTwo, error, data }) => {
                                                                                    const SnackTypeT = "error";
                                                                                    const SnackError = "Required field is empty"
                                                                                    if (loadingTwo) return <p>Loading...</p>;
                                                                                    if (error) return `Error : ${error.message}`;
                                                                                    if (data.dealCardListQuery.length == 0) {
                                                                                        return (
                                                                                            <div style={{ overflow: "hidden"}}>
                                                                                                <Grid container alignItems="center" justify="center">
                                                                                                    <Grid item>
                                                                                                        <Typography variant="overline" style={{margin: 35}}>
                                                                                                            No Deals Found
                                                                                                        </Typography>
                                                                                                    </Grid>
                                                                                                </Grid>    
                                                                                            </div>
                                                                                        )
                                                                                    }
                                                                                        return data.dealCardListQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                                                                            <div style={{padding: 15}} key={_id}>
                                                                                            <Query
                                                                                            variables={{StorePlaceId: StorePlaceId, StoreAddress: LocationAddress}}
                                                                                            query={
                                                                                                gql `
                                                                                                query storeHomeQuery($StorePlaceId: String!, $StoreAddress: String!) {
                                                                                                    storeHomeQuery(StorePlaceId: $StorePlaceId, StoreAddress: $StoreAddress) {
                                                                                                        _id
                                                                                                        StorePlaceId
                                                                                                        StoreName
                                                                                                        StorePhone
                                                                                                        StoreHours {
                                                                                                        close {
                                                                                                            day
                                                                                                            time
                                                                                                            }
                                                                                                        open {
                                                                                                            day
                                                                                                            time
                                                                                                            }
                                                                                                        }
                                                                                                        Website
                                                                                                        StoreAddress
                                                                                                        PriceLevel
                                                                                                        Photo
                                                                                                    }
                                                                                                }
                                                                                                `
                                                                                            }
                                                                                        >
                                                                                            {({ loading: loadingOne, error, data }) => {
                                                                                            if (loadingOne) {
                                                                                                return (
                                                                                                    <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                                                                            <Grid container direction="column" alignItems="center" justify="center">
                                                                                                                <Grid item xs={12}>
                                                                                                                    <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                                                                        <CircularProgress/>
                                                                                                                    </div>
                                                                                                                </Grid>
                                                                                                            </Grid>
                                                                                                    </Flexbox>
                                                                                                )
                                                                                            }

                                                                                            if (error) return <p>Error :(</p>;
                                                                                                return data.storeHomeQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                                                                <Card key={_id2} className={classes.card2} onClick={() => this.setState({dealId: _id})} style={{backgroundImage: "url(" + "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=" + Photo[0] + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0" + ")"}}>
                                                                                                    <CardContent style={{backgroundColor:  "rgba(18, 17, 16, 0.7)", paddingTop: 10}}>
                                                                                                        <div style={{height: 100 }}>
                                                                                                            <Flexbox flexDirection="column" height="50%">
                                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                                    <Grid item xs={9}>
                                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}}>
                                                                                                                            {name}
                                                                                                                        </Typography>
                                                                                                                    </Grid>
                                                                                                                    <Grid item xs={3}>
                                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                                            40 %
                                                                                                                        </Typography>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                            </Flexbox>
                                                                                                            <Flexbox flexDirection="column" height="20%">
                                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                                    <Grid item xs={12}>
                                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                                            4.5
                                                                                                                        </Typography>
                                                                                                                    </Grid>
                                                                                                                </Grid>
                                                                                                            </Flexbox>
                                                                                                        </div>
                                                                                                    </CardContent>
                                                                                                </Card>
                                                                                                
                                                                                                    ));
                                                                                                }}
                                                                                            </Query>
                                                                                            
                                                                                            </div>
                                                                                        ));
                                                                                    }}
                                                                                </Query>
                                                                            </GridList>
                                                                            </div>
                                                                        </div>
                                                                        {/* </Slide> */}
                                                                    </Flexbox>
                                                                    <Flexbox element="footer">
                                                                        <div style={{ paddingLeft: 10, paddingTop: 10, paddingRight: 10, paddingBottom: 10, width: 100 + "%"}}>
                                                                            <Grid container alignItems="center" justify="space-evenly" direction="row">
                                                                                <Grid item>
                                                                                    { Accounts.userId() ? (
                                                                                        <Query
                                                                                            variables={{StoreId: _id2, userId: data.user._id}}
                                                                                            query={gql `
                                                                                                query Favorite($StoreId: String!, $userId: String!) {
                                                                                                favoritesHomeQuery(StoreId: $StoreId, userId: $userId) {
                                                                                                        _id
                                                                                                        StoreId
                                                                                                        userId
                                                                                                        TimeStamp
                                                                                                    }
                                                                                                }
                                                                                            `}
                                                                                            pollInterval={500}
                                                                                        >
                                                                                            {({ loading, error, data: data2 }) => {
                                                                                            if (loading) return (<IconButton>
                                                                                                                    <Favorite/>
                                                                                                                </IconButton>);
                                                                                            if (error) return `Error! ${error.message}`;
                                                                                            
                                                                                            return (
                                                                                                data2.favoritesHomeQuery.length != 0 ? (
                                                                                                    data2.favoritesHomeQuery.map(({ _id: _id3, StoreId, userId}) => (
                                                                                                    <div key={_id2}>
                                                                                                        <div>
                                                                                                            {((userId === Accounts.userId()) && (StoreId === _id2)) === true ? (
                                                                                                                <IconButton onClick={() => this.removeFavorites(_id3)}>
                                                                                                                    <Favorite style={{color: "#EC407A"}} />
                                                                                                                </IconButton>
                                                                                                            ):(null)}
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    ))
                                                                                                ):(
                                                                                                    <IconButton onClick={() => this.submitFavorite(_id2)}>
                                                                                                        <Favorite />
                                                                                                    </IconButton>
                                                                                                )
                                                                                            )
                                                                                            }}
                                                                                        </Query>
                                                                                        ):(
                                                                                            <IconButton>
                                                                                                <Favorite/>
                                                                                            </IconButton>
                                                                                        )}
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Fab size="small" style={{backgroundColor: "#66BB6A", color: "#fff"}} onClick={() => window.location.href='tel:' + StorePhone}>
                                                                                        <Phone />
                                                                                    </Fab>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Fab size="small" style={{backgroundColor: "#42A5F5", color: "#fff"}} onClick={() => window.location.href="https://www.google.com/maps/search/?api=1&query=Eiffel%20Tower&query_place_id=" + StorePlaceId}>
                                                                                        <Directions />
                                                                                    </Fab>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <Fab size="small" style={{backgroundColor: "#78909C", color: "#fff"}} onClick={() => window.location.href= Website}>
                                                                                        <Web />
                                                                                    </Fab>
                                                                                </Grid>
                                                                                <Grid item>
                                                                                    <IconButton
                                                                                        className={classnames(classes.expand, {
                                                                                        [classes.expandOpen]: this.state.bottom,
                                                                                        })}
                                                                                        onClick={this.toggleDrawer}
                                                                                        aria-expanded={this.state.bottom}
                                                                                        aria-label="Show more"
                                                                                    >
                                                                                        <ArrowUp />
                                                                                    </IconButton>
                                                                                    {/* {this.state.bottom === true ? (
                                                                                        <IconButton onClick={() => this.toggleDrawer(false)}>
                                                                                            <ArrowDown/>
                                                                                        </IconButton>
                                                                                    ):(
                                                                                        <IconButton onClick={() =>this.toggleDrawer(true)}>
                                                                                            <ArrowUp/>
                                                                                        </IconButton>
                                                                                    )} */}
                                                                                </Grid>
                                                                            </Grid>
                                                                        </div>
                                                                    </Flexbox>
                                                                </Flexbox>
                                                            </CardContent> 
                                                        </Card>
                                                    </div>
                                                </Grid>
                                            ));
                                        }}
                                    </Query>
                                </Flexbox>
                            </Flexbox>
                            <MediaQuery query="(max-width: 768px)">
                                <Flexbox flexDirection="column" height="10vh">
                                    <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                </Flexbox> 
                            </MediaQuery>
                        </Flexbox>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

StoreModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(StoreModal);


const card2 = graphql(createFavorite, {
    name: "createFavorite",
    options: {
        refetchQueries: [
            'Favorites'
        ]
    }
})(withApollo(DealStyleWrap));

export default graphql(removeFavorite, {
    name: "removeFavorite",
    options: {
        refetchQueries: [
            'Favorites'
        ]
    }
})(withApollo(card2));;