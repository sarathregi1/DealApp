import React from "react";
import gql from "graphql-tag";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Query } from "react-apollo";
import { graphql } from 'react-apollo';
import IconButton from '@material-ui/core/IconButton';
import Favorite from '@material-ui/icons/Favorite';
import Grid from '@material-ui/core/Grid';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Divider from '@material-ui/core/Divider';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import Phone from '@material-ui/icons/Phone';
import Directions from '@material-ui/icons/Directions';
import Web from '@material-ui/icons/Web';
import Add from '@material-ui/icons/Add';
import ArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withApollo } from "react-apollo";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import { Accounts } from "meteor/accounts-base";
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from './Snackbar';
import StoreModal from './StoreModal';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
    card: {
        borderRadius: 20 + "px"
    },
    card2: {
        borderRadius: 20 + "px"
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      marginBottom: 16,
    },
    pos: {
      marginBottom: 12,
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        flexWrap: 'nowrap',
        transform: 'translateZ(0)',
        
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    chip: {
        margin: theme.spacing.unit,
    },
});

const createFavorite = gql`
    mutation createFavorite($userId: String!, $StoreId: String!, $TimeStamp: String!) {
        createFavorite(userId: $userId, StoreId: $StoreId, TimeStamp: $TimeStamp) {
            _id
        }
    },
    
`;

const removeFavorite = gql`
    mutation removeFavorite($_id: String!, $userId: String!) {
        removeFavorite(_id: $_id, userId: $userId) {
            _id
        }
    },
`;

function sideScroll(element,direction,speed,distance,step){
    scrollAmount = 0;
    var slideTimer = setInterval(function(){
        if(direction == 'left'){
            element.scrollLeft -= step;
        } else {
            element.scrollLeft += step;
        }
        scrollAmount += step;
        if(scrollAmount >= distance){
            window.clearInterval(slideTimer);
        }
    }, speed);
}
  
class HomeQuery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {loaded: false, arr: [], SnackOpen: false, SnackType: "info", SnackError: null}
    }

    submitFavorite = StoreId => {
        // this.setState({userId, StoreId});
        const user = Accounts.userId();
        // console.log(user + " tttt " + StoreId)
        this.props
        .createFavorite({
            variables:{
                userId: user,
                StoreId: StoreId,
                TimeStamp: new Date()

            }
        })
    };

    removeFavorites = _id => {
        // this.setState({userId, StoreId});
        const user = Accounts.userId();
        // console.log(user + " tttt " + StoreId)
        this.props
        .removeFavorite({
            variables:{
                _id: _id,
                userId: user
            }
        })
    };

    handleOpenSnack = (SnackTypeT, SnackError) => {
        this.setState({ SnackOpen: true, SnackType: SnackTypeT, SnackError });
    };

    handleCloseSnack = (event, reason) => {
        if (reason === "clickaway") {
          return;
        }
        this.setState({ SnackOpen: false });
    }

    // handleCheckChange = name => event => {
    //     this.setState({ [name]: event.target.checked });
    // };

    render() {
        const { classes, theme } = this.props;
        return (
            <React.Fragment>
                <Flexbox flexDirection="column" minHeight="80vh" width="95vw">
                    <Flexbox  display="flex" flexWrap="wrap" justifyContent="center" flexGrow={1}>
                        <Grid container direction="row" alignItems="center" justify="flex-start" spacing={24}>
                            <Query
                                query={
                                    gql `
                                    query Store {
                                        stores {
                                            _id
                                            StorePlaceId
                                            StoreName
                                            StorePhone
                                            StoreHours {
                                            close {
                                                day
                                                time
                                                }
                                            open {
                                                day
                                                time
                                                }
                                            }
                                            Website
                                            StoreAddress
                                            PriceLevel
                                            Photo
                                        }
                                        user {
                                            _id
                                        }
                                        favorites {
                                            _id
                                            userId
                                            StoreId
                                            TimeStamp
                                        }
                                    }
                                    `
                                }
                                pollInterval={500}
                            >
                                {({ loading: loadingOne, error, data }) => {
                                if (loadingOne) {
                                    return (
                                        <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                <Grid container direction="column" alignItems="center" justify="center">
                                                    <Grid item xs={12}>
                                                        <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                            <CircularProgress/>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                        </Flexbox>
                                    )
                                }

                                if (error) return <p>Error :(</p>;
                                    return data.stores.map(({_id, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                        <Grid item xs={12} sm={6} md={4} lg={3} key={_id}>
                                            <div style={{margin: 20}}>
                                                <Card className={classes.card} style={{boxShadow: `0px 8px 8px #9E9E9E`}}>
                                                    <CardContent style={{padding: 0}}>
                                                        <div style={{paddingBottom: 0, height: 90}}>
                                                            <GridList className={classes.gridList} cols={2.5} cellHeight={90} id={_id}>
                                                                {Photo.map((ref) => (
                                                                <GridListTile key={ref} style={{padding: 0, paddingRight: 1, width: 25 + "%"}}>
                                                                    <img src={"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=" + ref + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0"} alt={ref} />
                                                                    <GridListTileBar
                                                                    classes={{
                                                                        root: classes.titleBar,
                                                                        title: classes.title,
                                                                    }}
                                                                    />
                                                                </GridListTile>
                                                                ))}
                                                            </GridList>
                                                            <div style={{position: "relative", bottom: 20}} >
                                                                <Grid container direction="row" alignItems="center" spacing={24}>
                                                                    <Grid item xs>
                                                                        <div style={{textAlign: "center"}}>
                                                                            <Fab size="small" style={{backgroundColor: "#fff"}}  onClick={() => sideScroll(document.getElementById(_id),'left',25,100,10)}>
                                                                                <ArrowLeft />
                                                                            </Fab>
                                                                        </div>
                                                                    </Grid>
                                                                    <Grid item xs>
                                                                        
                                                                    </Grid>
                                                                    <Grid item xs>
                                                                        <div style={{textAlign: "center"}}>
                                                                            <Fab size="small" style={{backgroundColor: "#fff"}} onClick={() => sideScroll(document.getElementById(_id),'right',10,150,10)}>
                                                                                <ArrowRight />
                                                                            </Fab>
                                                                        </div>
                                                                    </Grid>
                                                                </Grid>
                                                            </div>
                                                        </div>
                                                        <div style={{boxShadow: `0px 3px 8px #9E9E9E`, paddingLeft: 10, paddingTop: 10, paddingRight: 10, backgroundColor: "#90a4ae"}}>
                                                            <Grid container alignItems="center" justify="center" style={{marginBottom: 10, marginTop: 18}}>
                                                                <Grid item>
                                                                    <Typography variant="h5" style={{fontFamily: 'Roboto-Light, sans-serif-light', textAlign: "center" }}>
                                                                        {StoreName}
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>
                                                            <Grid container alignItems="center" justify="space-evenly" direction="row" style={{marginBottom: 10}}>
                                                                <Grid item>
                                                                    <Fab size="small" style={{backgroundColor: "#66BB6A", color: "#fff"}} onClick={() => window.location.href='tel:' + StorePhone}>
                                                                        <Phone />
                                                                    </Fab>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Fab size="small" style={{backgroundColor: "#42A5F5", color: "#fff"}} onClick={() => window.location.href="https://www.google.com/maps/search/?api=1&query=Eiffel%20Tower&query_place_id=" + StorePlaceId}>
                                                                        <Directions />
                                                                    </Fab>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Fab size="small" style={{backgroundColor: "#78909C", color: "#fff"}} onClick={() => window.location.href= Website}>
                                                                        <Web />
                                                                    </Fab>
                                                                </Grid>
                                                            </Grid>
                                                            <Divider/>
                                                            <Grid container justify="center" alignItems="center">
                                                                <Grid item>
                                                                    {/* <Button className={classes.button} style={{color: "#EF5350", fontFamily: 'Roboto-Light, sans-serif-light'}}>
                                                                        More Info <Add style={{fontWeight: 100}}/>
                                                                    </Button> */}
                                                                    <StoreModal StoreId={_id}/>
                                                                </Grid>
                                                            </Grid>
                                                       </div>
                                                       <div style={{padding: 10, paddingLeft: 0, paddingRight: 0, paddingBottom: 0}}>
                                                            <Grid container alignItems="center" justify="center">
                                                                <Grid item>
                                                                    <Typography variant="h6" style={{fontFamily: 'Roboto-Light, sans-serif-light' }}>
                                                                        Current Deals
                                                                    </Typography>
                                                                </Grid>
                                                            </Grid>    
                                                            <div className={classes.root} >
                                                                <GridList className={classes.gridList} cols={2.5} cellHeight={125} id={StoreName}>
                                                                    <Query
                                                                        variables={{StorePlaceId}}
                                                                        query={gql `
                                                                            query Deals($StorePlaceId: String!) {
                                                                            dealHomeQuery(StorePlaceId: $StorePlaceId) {
                                                                                    _id
                                                                                    name
                                                                                    TypeCuisine
                                                                                    TimeOfDay
                                                                                    FoodTitle
                                                                                    PriceRange
                                                                                    DealExpirationDate
                                                                                    DealExpirationTime
                                                                                    LocationAddress
                                                                                    StorePlaceId
                                                                                    DealDetails
                                                                                    StoreName
                                                                                    TimeStamp
                                                                                }
                                                                            }
                                                                        `}
                                                                        pollInterval={500}
                                                                    >
                                                                        {({ loading: loadingTwo, error, data }) => {
                                                                        const SnackTypeT = "error";
                                                                        const SnackError = "Required field is empty"
                                                                        if (loadingTwo) return <p>Loading...</p>;
                                                                        if (error) return `Error : ${error.message}`;
                                                                        if (data.dealHomeQuery.length == 0) {
                                                                            return (
                                                                                <div style={{ overflow: "hidden"}}>
                                                                                    <Grid container alignItems="center" justify="center">
                                                                                        <Grid item>
                                                                                            <Typography variant="overline" style={{margin: 35}}>
                                                                                                No Deals Found
                                                                                            </Typography>
                                                                                        </Grid>
                                                                                    </Grid>    
                                                                                </div>
                                                                            )
                                                                        }
                                                                            return data.dealHomeQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                                                                <div style={{padding: 15}} key={_id}>
                                                                                    <Card className={classes.card2}>
                                                                                        <CardContent>
                                                                                            <Typography variant="h6" >
                                                                                                {name}
                                                                                            </Typography>
                                                                                        </CardContent>
                                                                                    </Card>
                                                                                </div>
                                                                            ));
                                                                        }}
                                                                    </Query>
                                                                </GridList>
                                                            </div>
                                                        </div>
                                                    </CardContent> 
                                                    <CardActions style={{paddingTop: 0}}>
                                                        <Grid container direction="row" justify="flex-end" alignItems="center">
                                                            <Grid item>
                                                                { Accounts.userId() ? (
                                                                <Query
                                                                    variables={{StoreId: _id, userId: data.user._id}}
                                                                    query={gql `
                                                                        query Favorite($StoreId: String!, $userId: String!) {
                                                                        favoritesHomeQuery(StoreId: $StoreId, userId: $userId) {
                                                                                _id
                                                                                StoreId
                                                                                userId
                                                                                TimeStamp
                                                                            }
                                                                        }
                                                                    `}
                                                                    pollInterval={500}
                                                                >
                                                                    {({ loading, error, data: data2 }) => {
                                                                    if (loading) return "Loading...";
                                                                    if (error) return `Error! ${error.message}`;
                                                                    
                                                                    return (
                                                                        data2.favoritesHomeQuery.length != 0 ? (
                                                                            data2.favoritesHomeQuery.map(({ _id: _id2, StoreId, userId}) => (
                                                                            <div key={_id}>
                                                                                <div>
                                                                                    {((userId === Accounts.userId()) && (StoreId === _id)) === true ? (
                                                                                        <IconButton>
                                                                                            <Favorite style={{color: "#EC407A"}} onClick={() => this.removeFavorites(_id2)}/>
                                                                                        </IconButton>
                                                                                    ):(null)}
                                                                                </div>
                                                                            </div>
                                                                            ))
                                                                        ):(
                                                                            <IconButton>
                                                                                <Favorite onClick={() => this.submitFavorite(_id)}/>
                                                                            </IconButton>
                                                                        )
                                                                        
                                                                    )
                                                                    }}
                                                                </Query>
                                                                 ):(
                                                                    <IconButton>
                                                                        <Favorite/>
                                                                    </IconButton>
                                                                )}
                                                            </Grid>
                                                        </Grid>
                                                    </CardActions> 
                                                </Card>
                                                
                                            </div>
                                        </Grid>
                                    ));
                                }}
                            </Query>
                        </Grid>
                    </Flexbox>
                    <div style={{margin: 30}}>

                    </div>
                </Flexbox>
                <Snackbar
                    anchorOrigin={{
                        vertical: "top",
                        horizontal: "left"
                    }}
                    open={this.state.SnackOpen}
                    autoHideDuration={6000}
                    onClose={this.handleCloseSnack}
                    >
                    <MySnackbarContentWrapper
                        onClose={this.handleCloseSnack}
                        variant={this.state.SnackType}
                        message={this.state.SnackError}
                    />
                </Snackbar>
            </React.Fragment>
        );
    }
}   

HomeQuery.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
const card = withStyles(styles)(HomeQuery);

const card2 = graphql(createFavorite, {
    name: "createFavorite",
    options: {
        refetchQueries: [
            'Favorites'
        ]
    }
})(withApollo(card));

export default graphql(removeFavorite, {
    name: "removeFavorite",
    options: {
        refetchQueries: [
            'Favorites'
        ]
    }
})(withApollo(card2));;
