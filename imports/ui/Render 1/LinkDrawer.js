import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
import LocationSearch from './LocationSearch';
import LocationSearchMobile from './LocationSearchMobile';
import SearchIcon from '@material-ui/icons/Search';
import { Typography } from '@material-ui/core';
import ArrowUp from '@material-ui/icons/KeyboardArrowUp';
import MenuIcon from '@material-ui/icons/Menu';

const styles = theme => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  card: {
    width: 50 + "vw",
    height: 90 + "vh"
  },
  cardmobile: {
    width: 80 + "vw"
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  input: {
    margin: theme.spacing.unit,
  }
});

class DealAddModal extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        open: false,
        value: 0,
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        return (
            <React.Fragment>
                <MediaQuery query="(max-width: 1366px)">
                    <Button onClick={this.handleOpen} style={{width: 40, height: 56, position: "fixed", bottom: 30, backgroundColor: "#E7E7E7", borderTopLeftRadius: 45 * 2, borderTopRightRadius: 45 * 2}}>
                        <ArrowUp style={{position: "relative", bottom: 10, color: "#37474F"}}/>
                    </Button>
                </MediaQuery>
                <MediaQuery query="(min-width: 1367px)">
                    <IconButton onClick={this.handleOpen}>
                        <MenuIcon style={{color: "#FFFFFF"}}/>
                    </IconButton>
                </MediaQuery>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div style={{width: 100 + 'vw', height: 100 + '%'}}>
                        <Flexbox flexDirection="column" height="100%">
                            <MediaQuery query="(min-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <div style={{ textAlign: "right"}}>
                                        <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                            <CloseIcon style={{color: "#FFFFFF"}}/>
                                        </IconButton>
                                    </div>
                                </Flexbox>
                            </MediaQuery>
                            <Flexbox flexDirection="column" height="90%">
                                <Flexbox flexGrow={1}>
                                    <Grid spacing={24} container justify="center" alignItems="flex-start">
                                        <Grid item xs={3} style={{textAlign: "center" }}>
                                            <Button href="#/Admin" onClick={this.handleClose} style={{width: 100, height: 100, borderRadius: "50%", backgroundColor: "#ffa"}}>
                                                Admin
                                            </Button>
                                        </Grid> 
                                        <Grid item xs={3} style={{textAlign: "center" }}>
                                            <Button href="#/FAQ" onClick={this.handleClose} style={{width: 100, height: 100, borderRadius: "50%", backgroundColor: "#ffa"}}>
                                                FAQ
                                            </Button>
                                        </Grid>
                                        <Grid item xs={3} style={{textAlign: "center" }}>
                                            <Button href="#/About" onClick={this.handleClose} style={{width: 100, height: 100, borderRadius: "50%", backgroundColor: "#ffa"}}>
                                                About Us
                                            </Button>
                                        </Grid> 
                                        <Grid item xs={3} style={{textAlign: "center" }}>
                                            <Button href="#/BecomeAPartner" onClick={this.handleClose} style={{width: 100, height: 100, borderRadius: "50%", backgroundColor: "#ffa"}}>
                                                Partners
                                            </Button>
                                        </Grid>    
                                    </Grid>
                                </Flexbox>
                            </Flexbox>
                            <MediaQuery query="(max-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <MediaQuery query="(max-width: 767px)">
                                        <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                    </MediaQuery>
                                </Flexbox> 
                            </MediaQuery>
                        </Flexbox>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

DealAddModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(DealAddModal);

// const DealModalWrapped = withStyles(styles)(DealStyleWrap);

export default DealStyleWrap