import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import DealCard from './DealCard';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import ArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Button from '@material-ui/core/Button';
import StoreModal from './StoreModal';
import StoreModalMobile from './StoreModalMobile';
import GridList from '@material-ui/core/GridList';
import Flexbox from 'flexbox-react';
import Open from '@material-ui/icons/OpenInNew';
import CircularProgress from '@material-ui/core/CircularProgress';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import MediaQuery from 'react-responsive';
import Snackbar from '@material-ui/core/Snackbar';
import MySnackbarContentWrapper from './Snackbar';
import Star from '@material-ui/icons/StarBorder';

const styles = theme => ({
    card: {
        borderRadius: 20 + "px",
        height: 200,
        width: 200
    },
    card2: {
        borderRadius: 20 + "px",
        height: 100,
        width: 200
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
    },
      gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
      title: {
        color: theme.palette.primary.light,
    },
      titleBar: {
        background:
          'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
})

class HomePageQuery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {view: false, open: false, id: "", SnackOpen: false, SnackType: "info", SnackError: "", dealId: ''}
    }

    handleOpen = (_id2, _id) => {
        this.handDealId(_id);
        this.setState({ open: true });
        this.setState({id: _id2});
        
    };

    handDealId = (_id) => {
        // this.setState({dealId: _id});
        // console.log(this.state.dealId)
        this.setState({ dealId: _id }, () => {
            
        }); 
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleOpenSnack = (sType, sError) => {
        this.setState({ SnackOpen: true, SnackType: sType, SnackError: sError });
    };

    handleCloseSnack = (event, reason) => {
        if (reason === "clickaway") {
          return;
        }
        this.setState({ SnackOpen: false });
    }
    
    render() {
        const { classes, theme } = this.props;
        const { photoIndex, isOpen } = this.state;
        return (
            <div> 
                <div>
                    <Query
                        query={gql `
                            query dealtype {
                                dealtype {
                                    _id
                                    name
                                    TimeStamp                            
                                }
                            }
                        `}
                    >
                        {({ loading, error, data: data2 }) => {
                        if (loading) return "Loading...";
                        if (error) return `Error! ${error.message}`;
                        
                        return (
                                data2.dealtype.map(({ _id, name }) => (
                                    <div key={_id} style={{width: 100 + "vw"}}>
                                        <Grid container direction="column" alignItems="flex-start" justify="center" style={{width: 100 + "vw"}}>
                                            <Grid item xs={12}>
                                                <div style={{width: 100 + "vw", textAlign: "left"}}>
                                                    <Query
                                                        variables={{name: name}}
                                                        query={gql `
                                                            query dealsCatQuery($name: String!) {
                                                            dealsCatQuery(name: $name) {
                                                                    _id
                                                                    name
                                                                    TypeCuisine
                                                                    TimeOfDay
                                                                    FoodTitle
                                                                    PriceRange
                                                                    DealExpirationDate
                                                                    DealExpirationTime
                                                                    LocationAddress
                                                                    StorePlaceId
                                                                    DealDetails
                                                                    StoreName
                                                                    TimeStamp
                                                                }
                                                            }
                                                        `}
                                                    >
                                                        {({ loading: loadingTwo, error, data }) => {
                                                        const SnackTypeT = "error";
                                                        const SnackError = "Required field is empty"
                                                        if (loadingTwo) return <p>Loading...</p>;
                                                        if (error) return `Error : ${error.message}`;
                                                        if ( data.dealsCatQuery.length == 0 ) {
                                                            return (null)
                                                        }
                                                        if ( data.dealsCatQuery.length < 5) {
                                                            return (
                                                                <div style={{ overflow: "hidden"}}>
                                                                    <Grid container justify="center" alignItems="center">
                                                                        <Grid item xs={12}>
                                                                            <Typography variant="overline" style={{fontSize: 20, color: "#FFFFFF", margin: 20}}>
                                                                                {name}
                                                                            </Typography>
                                                                        </Grid>
                                                                    </Grid> 
                                                                </div>
                                                            )
                                                        }
                                                            return (
                                                                <div style={{ overflow: "hidden"}}>
                                                                    <Grid container justify="center" alignItems="center">
                                                                        <Grid item xs={9}>
                                                                            <Typography variant="overline" style={{fontSize: 20, color: "#FFFFFF", margin: 20}}>
                                                                                {name}
                                                                            </Typography>
                                                                        </Grid>
                                                                        <Grid item xs={3}>
                                                                            <div style={{textAlign: "right"}}>
                                                                                <Button style={{color: "#FFFFFF"}}>
                                                                                    View More <ArrowRight/>
                                                                                </Button>
                                                                            </div>
                                                                        </Grid>
                                                                    </Grid>   
                                                                </div>
                                                            )
                                                        
                                                        }}
                                                    </Query>
                                                </div>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <GridList className={classes.gridList} cols={2.5} cellHeight={20}>
                                                    <Query
                                                        variables={{name: name}}
                                                        query={gql `
                                                            query dealsCatQuery($name: String!) {
                                                            dealsCatQuery(name: $name) {
                                                                    _id
                                                                    name
                                                                    TypeCuisine
                                                                    TimeOfDay
                                                                    FoodTitle
                                                                    PriceRange
                                                                    DealExpirationDate
                                                                    DealExpirationTime
                                                                    LocationAddress
                                                                    StorePlaceId
                                                                    DealDetails
                                                                    StoreName
                                                                    TimeStamp
                                                                }
                                                            }
                                                        `}
                                                    >
                                                        {({ loading: loadingTwo, error, data }) => {
                                                        const SnackTypeT = "error";
                                                        const SnackError = "Required field is empty"
                                                        if (loadingTwo) return <p>Loading...</p>;
                                                        if (error) return `Error : ${error.message}`;
                                                        if (data.dealsCatQuery.length == 0) {
                                                            return (
                                                                // <div style={{ overflow: "hidden"}}>
                                                                //     <Grid container alignItems="center" justify="center">
                                                                //         <Grid item>
                                                                //             <Typography variant="overline" style={{margin: 35}}>
                                                                //                 No Deals Found
                                                                //             </Typography>
                                                                //         </Grid>
                                                                //     </Grid>    
                                                                // </div>
                                                                null
                                                            )
                                                        }
                                                            return data.dealsCatQuery.map(({ _id, name, TypeCuisine, TimeOfDay, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, LocationAddress, StorePlaceId, DealDetails, StoreName, TimeStamp}) => (
                                                                <div style={{padding: 15}} key={_id}>
                                                                    <Query
                                                                        variables={{StorePlaceId: StorePlaceId, StoreAddress: LocationAddress}}
                                                                        query={
                                                                            gql `
                                                                            query storeHomeQuery($StorePlaceId: String!, $StoreAddress: String!) {
                                                                                storeHomeQuery(StorePlaceId: $StorePlaceId, StoreAddress: $StoreAddress) {
                                                                                    _id
                                                                                    StorePlaceId
                                                                                    StoreName
                                                                                    StorePhone
                                                                                    StoreHours {
                                                                                    close {
                                                                                        day
                                                                                        time
                                                                                        }
                                                                                    open {
                                                                                        day
                                                                                        time
                                                                                        }
                                                                                    }
                                                                                    Website
                                                                                    StoreAddress
                                                                                    PriceLevel
                                                                                    Photo
                                                                                }
                                                                            }
                                                                            `
                                                                        }
                                                                    >
                                                                        {({ loading: loadingOne, error, data }) => {
                                                                            if (loadingOne) {
                                                                                return (
                                                                                    <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                                                            <Grid container direction="column" alignItems="center" justify="center">
                                                                                                <Grid item xs={12}>
                                                                                                    <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                                                        <CircularProgress/>
                                                                                                    </div>
                                                                                                </Grid>
                                                                                            </Grid>
                                                                                    </Flexbox>
                                                                                )
                                                                            }

                                                                            if (error) return <p>Error :(</p>;
                                                                            return data.storeHomeQuery.map(({_id: _id2, StoreName, StorePlaceId, StorePhone, StoreHours, Website, PriceLevel, StoreAddress, Photo}) => (
                                                                                <Card key={_id2} className={classes.card} onClick={() => this.handleOpen(_id2, _id)} style={{backgroundImage: "url(" + "https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=" + Photo[0] + "&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0" + ")"}}>
                                                                                    <CardContent style={{backgroundColor:  "rgba(18, 17, 16, 0.7)"}}>
                                                                                        <div style={{height: 200 }}>
                                                                                            <Flexbox flexDirection="column" height="50%">
                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                    <Grid item xs={9}>
                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}}>
                                                                                                            {name}
                                                                                                        </Typography>
                                                                                                    </Grid>
                                                                                                    <Grid item xs={3}>
                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF"}}>
                                                                                                            40 % 
                                                                                                        </Typography>
                                                                                                    </Grid>
                                                                                                </Grid>
                                                                                            </Flexbox>
                                                                                            <Flexbox flexDirection="column" height="20%">
                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                    <Grid item xs={12}>
                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF", fontSize: 20}} noWrap={true}>
                                                                                                            {StoreName}
                                                                                                        </Typography>
                                                                                                    </Grid>
                                                                                                </Grid>
                                                                                            </Flexbox>
                                                                                            <Flexbox flexDirection="column" height="20%" style={{marginTop: 10}}>
                                                                                                <Grid container direction="row" alignItems="stretch" justify="center">
                                                                                                    <Grid item xs={10}>
                                                                                                        <div style={{width: "80%"}}>
                                                                                                        <Typography variant="overline" style={{color: "#FFFFFF"}} noWrap={true}>
                                                                                                            {/* <span style={{position: "relative", top: 1}}><Star style={{fontSize: ".75rem"}}/></span> 4.5 | <span style={{color: "#00C853"}}>Open Now</span> */}
                                                                                                            {/* <span style={{position: "relative", top: 1}}><Star style={{fontSize: ".75rem"}}/></span> 4.5 | <span style={{color: "#D50000"}}>Closed</span> */}
                                                                                                            <span style={{position: "relative", top: 1}}><Star style={{fontSize: ".75rem"}}/></span> 4.5 | <span style={{color: "#FFAB00"}}>Closing Soon</span>
                                                                                                        </Typography>
                                                                                                        </div>
                                                                                                    </Grid>
                                                                                                    <Grid item xs={2}>
                                                                                                        <Open style={{color: "#FFFFFF"}}/>
                                                                                                    </Grid>
                                                                                                </Grid>
                                                                                            </Flexbox>
                                                                                        </div>
                                                                                    </CardContent>
                                                                                </Card>
                                                                            ));
                                                                        }}
                                                                    </Query>
                                                                </div>
                                                            ));
                                                        }}
                                                    </Query>
                                                </GridList>
                                            </Grid>
                                        </Grid>
                                    </div>
                                )
                            )
                        )    
                        }}
                    </Query>
                </div>
                <div>
                    <MediaQuery query="(max-width: 768px)">
                        <StoreModalMobile id={this.state.id} dealId={this.state.dealId} openModal={this.state.open} action={() => this.handleClose()} snack={({sType, sError}) => this.handleOpenSnack(sType, sError)}/>
                    </MediaQuery>
                    <MediaQuery query="(min-width: 769px)">
                        <StoreModal id={this.state.id} openModal={this.state.open} dealId={this.state.dealId} action={() => this.handleClose()} snack={({sType, sError}) => this.handleOpenSnack(sType, sError)}/>
                    </MediaQuery>
                </div>
                <div>
                <Snackbar
                    anchorOrigin={{
                        vertical: "top",
                        horizontal: "left"
                    }}
                    open={this.state.SnackOpen}
                    autoHideDuration={6000}
                    onClose={this.handleCloseSnack}
                    >
                    <MySnackbarContentWrapper
                        onClose={this.handleCloseSnack}
                        variant={this.state.SnackType}
                        message={this.state.SnackError}
                    />
                </Snackbar>
                </div>
            </div>
        )
    }
}

HomePageQuery.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(HomePageQuery);
