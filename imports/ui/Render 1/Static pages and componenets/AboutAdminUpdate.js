import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0
    }
});

const UPDATE_ABOUT = gql`
    mutation updateAbout($_id: String!, $aboutText: String!, $visible: String!) {
        updateAbout(_id: $_id, aboutText: $aboutText, visible: $visible) {
            _id
            aboutText
            visible
        }
    }
`;

class AdminWelcomePage extends Component {
    state = {
        open: false,
        aboutText: this.props.aboutText,
        visible: this.props.visible
    };
    render() {
        const { classes, theme } = this.props;
        return (
            <div>
                <Mutation mutation={UPDATE_ABOUT}>
                    {(updateAbout, { data }) => (
                        <div>
                            <form
                                onSubmit={e => {
                                    e.preventDefault();
                                    updateAbout({ variables: { _id: this.props._id, aboutText: this.state.aboutText, visible: this.state.visible} });
                                }}
                                style={{height: "70vh", width: "70vw"}}
                            >
                                    <Grid item xs={12}>
                                        <TextField fullWidth multiline label="Answer" value={this.state.aboutText} onChange={e => {this.setState({ aboutText: e.target.value })}} />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl fullWidth className={classes.formControl}>
                                            <InputLabel>Public Visiblity</InputLabel>
                                            <Select
                                                value={this.state.visible}
                                                onChange={e => {this.setState({ visible: e.target.value })}}
                                            >
                                                <MenuItem value="public">Public</MenuItem>
                                                <MenuItem value="offline">Offline</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <div style={{textAlign: "center", margin: 10}}>
                                            <Fab variant="extended" type="submit">Submit</Fab>
                                        </div>
                                    </Grid>
                            </form>
                        </div>
                    )}
                </Mutation>
            </div>
        )
    }
}

AdminWelcomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdminWelcomePage);