import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Mutation } from "react-apollo";
import Flexbox from 'flexbox-react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import AboutAdminUpdate from './AboutAdminUpdate';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0
    }
});

const UPDATE_ABOUT = gql`
    mutation updateFAQ($_id: String!, $question: String!, $answer: String!, $visible: String!) {
        updateFAQ(_id: $_id, question: $question, answer: $answer, visible: $visible) {
            _id
            question
            answer
            visible
        }
    }
`;

const ADD_ABOUT = gql`
    mutation createAbout($aboutText: String!, $visible: String!) {
        createAbout(aboutText: $aboutText, visible: $visible) {
            _id
            aboutText
            visible
        }
    }
`;

class AboutAdmin extends Component {
    state = {
        open: false,
        aboutText: "",
        visible: ""
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 20, overflowY: "auto"}}>
                        <Query
                            query={
                                gql `
                                query AboutQuery {
                                    AboutQuery {
                                        _id
                                        aboutText
                                        visible
                                        userId
                                        TimeStamp
                                    }
                                }
                                `
                            }
                            pollInterval={500}
                        >
                            {({ loading: loadingOne, error, data }) => {
                                if (loadingOne) {
                                    return (
                                        <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                <Grid container direction="column" alignItems="center" justify="center">
                                                    <Grid item xs={12}>
                                                        <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                            <CircularProgress/>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                        </Flexbox>
                                    )
                                }
                                if (data.AboutQuery.length == 0) {
                                    return (
                                        <div style={{ overflow: "hidden"}}>
                                            <Grid container alignItems="center" justify="center">
                                                <Grid item>
                                                    <Button onClick={() => this.handleOpen()}>Create About Page</Button>
                                                </Grid>
                                            </Grid>
                                            <Modal
                                                aria-labelledby="simple-modal-title"
                                                aria-describedby="simple-modal-description"
                                                open={this.state.open}
                                                onClose={this.handleClose}
                                                style={{width: "100vw", height: "100vh"}}
                                            >
                                                <div style={{width: 100 + 'vw', height: 100 + '%', backgroundColor: "#FFFFFF"}}>
                                                    <Flexbox flexDirection="column" height="100%">
                                                        <MediaQuery query="(min-width: 768px)">
                                                            <Flexbox flexDirection="column" height="10%">
                                                                <div style={{ textAlign: "right"}}>
                                                                    <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                                                        <CloseIcon/>
                                                                    </IconButton>
                                                                </div>
                                                            </Flexbox>
                                                        </MediaQuery>
                                                        <Flexbox flexDirection="column" height="90%">
                                                            <Flexbox flexGrow={1}>
                                                                <Grid spacing={24} container direction="column" justify="center" alignItems="center">
                                                                    <Mutation mutation={ADD_ABOUT}>
                                                                        {(createAbout, { data }) => (
                                                                            <div>
                                                                                <form
                                                                                    onSubmit={e => {
                                                                                        e.preventDefault();
                                                                                        if (this.state.aboutText === "" || this.state.visible === "") {
                                                                                            alert("all feilds must be filled out");
                                                                                        }
                                                                                        else {
                                                                                            createAbout({ variables: { aboutText: this.state.aboutText, visible: this.state.visible} });
                                                                                            this.handleClose();
                                                                                            this.setState({aboutText: "", visible: ""});
                                                                                        }
                                                                                    }}
                                                                                    style={{height: "80vh", width: "80vw"}}
                                                                                >
                                                                                        <Grid item xs={12}>
                                                                                            <TextField fullWidth multiline label="Answer" value={this.state.aboutText} onChange={e => {this.setState({ aboutText: e.target.value })}} />
                                                                                        </Grid>
                                                                                        <Grid item xs={12}>
                                                                                            <FormControl fullWidth className={classes.formControl}>
                                                                                                <InputLabel>Public Visiblity</InputLabel>
                                                                                                <Select
                                                                                                    value={this.state.visible}
                                                                                                    onChange={e => {this.setState({ visible: e.target.value })}}
                                                                                                >
                                                                                                    <MenuItem value="public">Public</MenuItem>
                                                                                                    <MenuItem value="offline">Offline</MenuItem>
                                                                                                </Select>
                                                                                            </FormControl>
                                                                                        </Grid>
                                                                                        <Grid item xs={12}>
                                                                                            <div style={{textAlign: "center", margin: 10}}>
                                                                                                <Fab variant="extended" type="submit">Submit</Fab>
                                                                                            </div>
                                                                                        </Grid>
                                                                                </form>
                                                                            </div>
                                                                        )}
                                                                    </Mutation>
                                                                </Grid>
                                                            </Flexbox>
                                                        </Flexbox>
                                                        <MediaQuery query="(max-width: 768px)">
                                                            <Flexbox flexDirection="column" height="10%">
                                                                <MediaQuery query="(max-width: 767px)">
                                                                    <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                                                </MediaQuery>
                                                            </Flexbox> 
                                                        </MediaQuery>
                                                    </Flexbox>
                                                </div>
                                            </Modal>    
                                        </div>
                                    )
                                }

                                if (error) return <p>Error :(</p>;
                                return data.AboutQuery.map(({_id, aboutText, visible, userId, TimeStamp}) => (
                                    <div key={_id}>
                                        <AboutAdminUpdate _id={_id} aboutText={aboutText} visible={visible}/>
                                    </div>
                                ));
                            }}
                        </Query>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

AboutAdmin.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AboutAdmin);