import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { Query } from "react-apollo";
import gql from "graphql-tag";

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0
    }
});

class AdminAbout extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 0}}>
                        <List style={{padding: 0}}>
                            <ListItem button component={Link} to='/AboutAdmin'>
                                <ListItemText primary="About Us Page" secondary="Edit static pages on the website about us page." />
                            </ListItem>
                            <ListItem button component={Link} to='/TeamAdmin'>
                                <ListItemText primary="Team Page" secondary="Edit static pages on team info page." />
                            </ListItem>
                        </List>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

AdminAbout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdminAbout);