import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import EditIcon from '@material-ui/icons/Edit';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  card: {
    width: 50 + "vw",
    height: 90 + "vh"
  },
  cardmobile: {
    width: 80 + "vw"
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  input: {
    margin: theme.spacing.unit,
  }
});

const UPDATE_FAQ = gql`
    mutation updateFAQ($_id: String!, $question: String!, $answer: String!, $visible: String!) {
        updateFAQ(_id: $_id, question: $question, answer: $answer, visible: $visible) {
            _id
            question
            answer
            visible
        }
    }
`;

class DealAddModal extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        open: false,
        value: 0,
        question: this.props.question,
        answer: this.props.answer,
        visible: this.props.visible
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        return (
            <React.Fragment>
                <Button onClick={this.handleOpen} size="small">Edit</Button>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div style={{width: 100 + 'vw', height: 100 + '%'}} className={classes.paper}>
                        <Flexbox flexDirection="column" height="100%">
                            <MediaQuery query="(min-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <div style={{ textAlign: "right"}}>
                                        <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                            <CloseIcon/>
                                        </IconButton>
                                    </div>
                                </Flexbox>
                            </MediaQuery>
                            <Flexbox flexDirection="column" height="90%">
                                <Flexbox flexGrow={1}>
                                    <Grid spacing={24} container justify="center" alignItems="center">

                                        <Mutation mutation={UPDATE_FAQ}>
                                            {(updateFAQ, { data }) => (
                                                <div>
                                                    <form
                                                        onSubmit={e => {
                                                            e.preventDefault();
                                                            if (this.state.question === "" || this.state.answer === "" || this.state.visible === "") {
                                                                alert("all feilds must be filled out");
                                                            }
                                                            else {
                                                                updateFAQ({ variables: { _id: this.props._id, question: this.state.question , answer: this.state.answer, visible: this.state.visible} });
                                                                this.handleClose();
                                                                this.setState({question: "", answer: "", visible: ""});
                                                            }
                                                        }}
                                                        style={{height: "80vh", width: "80vw"}}
                                                    >
                                                            <Grid item xs={12}>
                                                                <TextField fullWidth label="Question" value={this.state.question} onChange={e => {this.setState({ question: e.target.value })}}/>
                                                            </Grid>
                                                            <Grid item xs={12}>
                                                                <TextField fullWidth multiline label="Answer" value={this.state.answer} onChange={e => {this.setState({ answer: e.target.value })}} />
                                                            </Grid>
                                                            <Grid item xs={12}>
                                                                <FormControl fullWidth className={classes.formControl}>
                                                                    <InputLabel>Public Visiblity</InputLabel>
                                                                    <Select
                                                                        value={this.state.visible}
                                                                        onChange={e => {this.setState({ visible: e.target.value })}}
                                                                        
                                                                    >
                                                                        <MenuItem value="public">Public</MenuItem>
                                                                        <MenuItem value="offline">Offline</MenuItem>
                                                                    </Select>
                                                                </FormControl>
                                                            </Grid>
                                                            <Grid item xs={12}>
                                                                <div style={{textAlign: "center", margin: 10}}>
                                                                    <Fab variant="extended" type="submit">Update</Fab>
                                                                </div>
                                                            </Grid>
                                                    </form>
                                                </div>
                                            )}
                                        </Mutation>
                                    </Grid>
                                </Flexbox>
                            </Flexbox>
                            <MediaQuery query="(max-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <MediaQuery query="(max-width: 767px)">
                                        <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                    </MediaQuery>
                                </Flexbox> 
                            </MediaQuery>
                        </Flexbox>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

DealAddModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(DealAddModal);

// const DealModalWrapped = withStyles(styles)(DealStyleWrap);

export default DealStyleWrap