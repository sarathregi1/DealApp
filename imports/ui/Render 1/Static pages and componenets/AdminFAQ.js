import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import Modal from '@material-ui/core/Modal';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { Query } from "react-apollo";
import CircularProgress from '@material-ui/core/CircularProgress';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
import moment from 'moment';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import AdminFAQModal from './AdminFAQModal';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
    card: {
        height: "100%",
        width: "80vw",
        flexGrow: 1,
        margin: 0
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
});

const REMOVE_FAQ = gql`
    mutation removeFAQ($_id: String!) {
        removeFAQ(_id: $_id) {
            _id
        }
    }
`;

const ADD_FAQ = gql`
    mutation createFAQ($question: String!, $answer: String!, $visible: String!) {
        createFAQ(question: $question, answer: $answer, visible: $visible) {
            _id
            question
            answer
            visible
        }
    }
`;

class AdminFAQ extends Component {
    state = {
        expanded: null,
        open: false,
        question: "",
        answer: "",
        visible: "",
        openDialog: false
    };
    
    handleChange = panel => (event, expanded) => {
        this.setState({
            expanded: expanded ? panel : false,
        });
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleClickOpen = () => {
        this.setState({ openDialog: true });
    };

    handleDialogClose = () => {
        this.setState({ openDialog: false });
    };

    render() {
        const { classes, theme } = this.props;
        const { expanded } = this.state;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 0}}>
                        <Grid container direction="row" justify="flex-end">
                        
                            <Grid item xs={12}>
                                <div style={{textAlign: "right"}}>
                                <Button onClick={this.handleOpen}>
                                    <AddIcon/> Add FAQ
                                </Button>
                                </div>
                            </Grid>
                        </Grid>
                        <Modal
                            aria-labelledby="simple-modal-title"
                            aria-describedby="simple-modal-description"
                            open={this.state.open}
                            onClose={this.handleClose}
                            style={{width: "100vw", height: "100vh"}}
                        >
                            <div style={{width: 100 + 'vw', height: 100 + '%', backgroundColor: "#FFFFFF"}}>
                                <Flexbox flexDirection="column" height="100%">
                                    <MediaQuery query="(min-width: 768px)">
                                        <Flexbox flexDirection="column" height="10%">
                                            <div style={{ textAlign: "right"}}>
                                                <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                                    <CloseIcon/>
                                                </IconButton>
                                            </div>
                                        </Flexbox>
                                    </MediaQuery>
                                    <Flexbox flexDirection="column" height="90%">
                                        <Flexbox flexGrow={1}>
                                            <Grid spacing={24} container direction="column" justify="center" alignItems="center">
                                                <Mutation mutation={ADD_FAQ}>
                                                    {(createFAQ, { data }) => (
                                                        <div>
                                                            <form
                                                                onSubmit={e => {
                                                                    e.preventDefault();
                                                                    if (this.state.question === "" || this.state.answer === "" || this.state.visible === "") {
                                                                        alert("all feilds must be filled out");
                                                                    }
                                                                    else {
                                                                        createFAQ({ variables: { question: this.state.question , answer: this.state.answer, visible: this.state.visible} });
                                                                        this.handleClose();
                                                                        this.setState({question: "", answer: "", visible: ""});
                                                                    }
                                                                }}
                                                                style={{height: "80vh", width: "80vw"}}
                                                            >
                                                                    <Grid item xs={12}>
                                                                        <TextField fullWidth label="Question" value={this.state.question} onChange={e => {this.setState({ question: e.target.value })}}/>
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <TextField fullWidth multiline label="Answer" value={this.state.answer} onChange={e => {this.setState({ answer: e.target.value })}} />
                                                                    </Grid>
                                                                    <Grid item xs={12}>
                                                                        <FormControl fullWidth className={classes.formControl}>
                                                                            <InputLabel>Public Visiblity</InputLabel>
                                                                            <Select
                                                                                value={this.state.visible}
                                                                                onChange={e => {this.setState({ visible: e.target.value })}}
                                                                                
                                                                            >
                                                                                <MenuItem value="public">Public</MenuItem>
                                                                                <MenuItem value="offline">Offline</MenuItem>
                                                                            </Select>
                                                                        </FormControl>
                                                                    </Grid>
                                                                    {/* <TextField value={this.state.visible} onChange={e => {this.setState({ visible: e.target.value })}}/> */}
                                                                    <Grid item xs={12}>
                                                                        <div style={{textAlign: "center", margin: 10}}>
                                                                            <Fab variant="extended" type="submit">Submit</Fab>
                                                                        </div>
                                                                    </Grid>
                                                            </form>
                                                        </div>
                                                    )}
                                                </Mutation>
                                            </Grid>
                                        </Flexbox>
                                    </Flexbox>
                                    <MediaQuery query="(max-width: 768px)">
                                        <Flexbox flexDirection="column" height="10%">
                                            <MediaQuery query="(max-width: 767px)">
                                                <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                            </MediaQuery>
                                        </Flexbox> 
                                    </MediaQuery>
                                </Flexbox>
                            </div>
                        </Modal>
                        <div className={classes.root}>
                            <Query
                                query={
                                    gql `
                                    query FAQQuery {
                                        FAQQuery {
                                            _id
                                            question
                                            answer
                                            visible
                                            userId
                                            TimeStamp
                                        }
                                    }
                                    `
                                }
                                pollInterval={500}
                            >
                                {({ loading: loadingOne, error, data }) => {
                                    if (loadingOne) {
                                        return (
                                            <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                    <Grid container direction="column" alignItems="center" justify="center">
                                                        <Grid item xs={12}>
                                                            <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                <CircularProgress/>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                            </Flexbox>
                                        )
                                    }

                                    if (error) return <p>Error :(</p>;
                                    return data.FAQQuery.map(({_id, question, answer, visible, userId, TimeStamp}) => (
                                        <div key={_id}>
                                            <ExpansionPanel>
                                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                                    <div className={classes.column}>
                                                        <Typography variant="overline">{visible}</Typography>
                                                    </div>
                                                </ExpansionPanelSummary>
                                                <ExpansionPanelDetails className={classes.details}>
                                                    <Grid container direction="row" spacing={24}>
                                                        <Grid item xs={8}>
                                                            <Grid container direction="column">
                                                                <Grid item xs={12}>
                                                                    <Typography>Question: {question}</Typography>
                                                                </Grid>
                                                                <Grid item xs={12}>
                                                                    <Typography className={classes.secondaryHeading}>Answer: {answer}</Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item xs>
                                                            <Grid container direction="column">
                                                                <Grid item>
                                                                <Typography variant="overline"> Created By:</Typography>
                                                                    <Query
                                                                        variables={{_id: userId}}
                                                                        query={
                                                                            gql `
                                                                            query userInfoQuery($_id: String!) {
                                                                                userInfoQuery(_id: $_id) {
                                                                                    _id
                                                                                    email
                                                                                }
                                                                            }
                                                                            `
                                                                        }
                                                                    >
                                                                        {({ loading: loadingOne, error, data }) => {
                                                                            if (loadingOne) {
                                                                                return (
                                                                                    <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                                                            <Grid container direction="column" alignItems="center" justify="center">
                                                                                                <Grid item xs={12}>
                                                                                                    <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                                                                        <CircularProgress/>
                                                                                                    </div>
                                                                                                </Grid>
                                                                                            </Grid>
                                                                                    </Flexbox>
                                                                                )
                                                                            }

                                                                            if (error) return <p>Error :(</p>;
                                                                            return data.userInfoQuery.map(({_id, email}) => (
                                                                                <div key={_id}>
                                                                                    <Chip label={email} className={classes.chip} />
                                                                                </div>
                                                                            ));
                                                                        }}
                                                                    </Query>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography variant="overline" style={{color: "rgba(0, 0, 0, 0.54)"}}> Submitted: {moment({TimeStamp}).format('MMMM Do YYYY, h:mm:ss a')}</Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid>
                                                </ExpansionPanelDetails>
                                                <Divider />
                                                <ExpansionPanelActions>
                                                    <AdminFAQModal _id={_id} question={question} answer={answer} visible={visible}/>
                                                    {/* <Button key={_id} size="small" onClick={() => this.handleClickOpen()} style={{color: "#EF5350"}}>Delete</Button> */}
                                                    <Mutation mutation={REMOVE_FAQ}>
                                                        {(removeFAQ, { data }) => (
                                                            <div>
                                                                {/* <Dialog
                                                                key={_id}
                                                                open={this.state.openDialog}
                                                                onClose={this.handleDialogClose}
                                                                aria-labelledby="alert-dialog-title"
                                                                aria-describedby="alert-dialog-description"
                                                                >
                                                                    <DialogTitle id="alert-dialog-title">{"Are you sure you want to delete?"}</DialogTitle>
                                                                    <DialogActions>
                                                                        <Button onClick={this.handleDialogClose}>
                                                                            Cancel
                                                                        </Button>
                                                                        <form
                                                                            onSubmit={e => {
                                                                                e.preventDefault();
                                                                                removeFAQ({ variables: { _id: _id} });
                                                                            }}
                                                                        >
                                                                            <Button size="small" type="submit" style={{color: "#EF5350"}}>Confirm</Button>
                                                                        </form>
                                                                    </DialogActions>
                                                                </Dialog> */}
                                                                <form
                                                                    onSubmit={e => {
                                                                        e.preventDefault();
                                                                        removeFAQ({ variables: { _id: _id} });
                                                                    }}
                                                                >
                                                                    <Button size="small" type="submit" style={{color: "#EF5350"}}>Delete</Button>
                                                                </form>
                                                            </div>
                                                        )}
                                                    </Mutation>
                                                </ExpansionPanelActions>
                                            </ExpansionPanel>
                                        </div>
                                    ));
                                }}
                            </Query>
                        </div>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

AdminFAQ.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdminFAQ);