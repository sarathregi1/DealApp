import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import gql from "graphql-tag";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Flexbox from 'flexbox-react';
import { Query } from "react-apollo";
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    card: {
        height: "100%",
        width: "80vw",
        flexGrow: 1,
        margin: 0
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
});

class FAQ extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 20}}>
                        <div style={{padding: 10, paddingTop: 0}}>
                            <Typography variant="h3">FAQ</Typography>
                        </div>
                        <Query
                            query={
                                gql `
                                query FAQHomeQuery {
                                    FAQHomeQuery {
                                        _id
                                        question
                                        answer
                                    }
                                }
                                `
                            }
                            pollInterval={500}
                        >
                            {({ loading: loadingOne, error, data }) => {
                                if (loadingOne) {
                                    return (
                                        <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                <Grid container direction="column" alignItems="center" justify="center">
                                                    <Grid item xs={12}>
                                                        <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                            <CircularProgress/>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                        </Flexbox>
                                    )
                                }

                                if (error) return <p>Error :(</p>;
                                return data.FAQHomeQuery.map(({_id, question, answer}) => (
                                    <div key={_id} style={{padding: 10}}>
                                        <Grid container direction="column">
                                            <Grid item xs={12}>
                                                <Typography variant="h5">{question}</Typography>
                                            </Grid>
                                            <Grid item xs={12}>
                                                <Typography variant="subtitle1" className={classes.secondaryHeading}>{answer}</Typography>
                                            </Grid>
                                        </Grid>
                                    </div>
                                ));
                            }}
                        </Query>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

FAQ.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FAQ);