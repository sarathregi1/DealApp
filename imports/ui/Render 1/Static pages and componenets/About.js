import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import Flexbox from 'flexbox-react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0,
        overflowY:"scroll",
        position:"relative",
    }
});

class About extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 20}}>
                        <div style={{padding: 10, paddingTop: 0}}>
                            <Typography variant="h3">About Us</Typography>
                        </div>
                        <Query
                            query={
                                gql `
                                query AboutHomeQuery {
                                    AboutHomeQuery {
                                        _id
                                        aboutText
                                    }
                                }
                                `
                            }
                            pollInterval={500}
                        >
                            {({ loading: loadingOne, error, data }) => {
                                if (loadingOne) {
                                    return (
                                        <Flexbox flexDirection="column" height="100vh" width="100vw">
                                                <Grid container direction="column" alignItems="center" justify="center">
                                                    <Grid item xs={12}>
                                                        <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                            <CircularProgress/>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                        </Flexbox>
                                    )
                                }
                                if (data.AboutHomeQuery.length === 0) {
                                    return (
                                        <Flexbox flexDirection="column" height="80vh" width="80vw">
                                                <Grid container direction="column" alignItems="center" justify="center">
                                                    <Grid item xs={12}>
                                                        <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                            <Typography variant="overline" className={classes.secondaryHeading}>Sorry for the inconvenience our team is baking up a fix.</Typography>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                        </Flexbox>
                                    )
                                }

                                if (error) return <p>Error :(</p>;
                                return data.AboutHomeQuery.map(({_id, aboutText}) => (
                                    <div key={_id} style={{padding: 10}}>
                                        <Grid container direction="column">
                                            <Grid item xs={12}>
                                                <Typography variant="subtitle1" className={classes.secondaryHeading}>{aboutText}</Typography>
                                            </Grid>
                                        </Grid>
                                    </div>
                                ));
                            }}
                        </Query>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

About.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(About);