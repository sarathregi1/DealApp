import React, { Component } from 'react';
import { render } from 'react-dom';
import { graphql } from 'react-apollo';
import { withApollo } from 'react-apollo';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Login from '../ui/LoginModal';
import MediaQuery from 'react-responsive';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {ApolloClient} from 'apollo-client';
import { withClientState } from 'apollo-link-state';
import LinkDrawer from './LinkDrawer';
import { Query } from "react-apollo";
import CircularProgress from '@material-ui/core/CircularProgress';
import Flexbox from 'flexbox-react';
import Grid from '@material-ui/core/Grid';

const styles = {
    root: {
      width: '100%',
    },
    flex: {
      flex: 1,
    }
};

const image2 = "/Logo.svg";

// const cache = new InMemoryCache();
// const stateLink = withClientState({ cache, resolvers, defaults });

// const client = new ApolloClient({
//   cache,
//   link: stateLink,
// });

// const unsubscribe = client.onResetStore(stateLink.writeDefaults);
  
class SimpleAppBar extends Component { 
  // constructor(props) {
  //   super(props);
  // }
  render() {
    const { classes } = this.props;
    const { client } = this.props;
    return (
      <div className={classes.root}>
        <AppBar style={{zIndex: 10, position: "fixed", backgroundColor: "#26A69A", height: 64 }}>
          <Toolbar>
            <MediaQuery query="(min-width: 1367px)">
                <LinkDrawer/>
            </MediaQuery>
            <div className={classes.flex}>
                <MediaQuery query="(min-width: 1367px)">
                    <img src={image2} style={{width: 180, textAlign: "left", position: "relative", top: 5}}/>
                </MediaQuery>
                <MediaQuery query="(max-width: 1366px)">
                    <img src={image2} style={{width: 150, textAlign: "left", position: "relative", top: 5}}/>
                </MediaQuery>
            </div>
                
              <MediaQuery query="(min-width: 1367px)">
                <Button href="#/" style={{ fontFamily: 'Raleway', color: "#FFFFFF", height: 64 }}>
                    Home
                </Button>
                <Button href="#/Favorites" style={{ fontFamily: 'Raleway', color: "#FFFFFF", height: 64 }}>
                    Favorites
                </Button>
                <Button href="#/FindNear" style={{ fontFamily: 'Raleway', color: "#FFFFFF", height: 64 }}>
                    Nearby
                </Button>
                <Button href="#/Admin" style={{ fontFamily: 'Raleway', color: "#FFFFFF", height: 64 }}>
                    Admin
                </Button>
              </MediaQuery>
              <Query
                  query={
                      gql `
                          query User {
                          user {
                              _id
                              profile {
                              role
                              }
                          }
                          }
                      `
                  }
                  pollInterval={500}
              >
                  {({ loading: loadingOne, error, data }) => {
                      if (loadingOne) {
                          return (
                              <Flexbox flexDirection="column" height="100vh" width="100vw">
                                      <Grid container direction="column" alignItems="center" justify="center">
                                          <Grid item xs={12}>
                                              <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                  <CircularProgress/>
                                              </div>
                                          </Grid>
                                      </Grid>
                              </Flexbox>
                          )
                      }

                      if (error) return <p>Error :(</p>;
                      return (
                          <div>
                              { data.user._id ? (
                                <div>
                                  <Button style={{ fontFamily: 'Raleway', color: "#FFFFFF", height: 64 }} onClick={() => {
                                    Meteor.logout();
                                    client.resetStore();
                                    //unsubscribe
                                  }}>
                                    Logout
                                  </Button>
                                </div>
                                
                              ) : (
                                <Login client={client}/>
                              )}
                          </div>
                      )
                  }}
              </Query>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
  
const UserQuery = gql `
  query Users {
      user {
          _id
      }
  }
`;

SimpleAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const login = withStyles(styles)(SimpleAppBar);

export default login;
  
  