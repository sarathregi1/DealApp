import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MediaQuery from 'react-responsive';
import Flexbox from 'flexbox-react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import EditIcon from '@material-ui/icons/Edit';
import LocationSearch from './LocationSearch';
import LocationSearchMobile from './LocationSearchMobile';
import Fab from '@material-ui/core/Fab';

const styles = theme => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  card: {
    width: 50 + "vw",
    height: 90 + "vh"
  },
  cardmobile: {
    width: 80 + "vw"
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
  input: {
    margin: theme.spacing.unit,
  }
});

class DealAddModal extends React.Component {
    constructor(props) {
        super(props)
    }

    state = {
        open: false,
        value: 0
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { classes, theme } = this.props;
        const { client } = this.props;
        return (
            <React.Fragment>
                <MediaQuery query="(min-width: 1367px)">
                    <Fab onClick={this.handleOpen} style={{ backgroundColor: "#26A69A", color: "#FFFFFF", position: "fixed", right: 30, bottom: 30 }}>
                        <EditIcon/>
                    </Fab>
                </MediaQuery>
                <MediaQuery query="(max-width: 1366px)">
                    <Fab onClick={this.handleOpen} style={{ backgroundColor: "#26A69A", color: "#FFFFFF", position: "fixed", right: 20, bottom: 80 }}>
                        <EditIcon/>
                    </Fab>
                </MediaQuery>
                <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
                style={{width: 100 + 'vw', height: 100 + '%'}}
                >
                    <div style={{ width: 100 + 'vw', height: 100 + '%'}} className={classes.paper}>
                        <Flexbox flexDirection="column" height="100%">
                            <MediaQuery query="(min-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <div style={{ textAlign: "right"}}>
                                        <IconButton style={{ fontFamily: 'Raleway' }} onClick={this.handleClose}>
                                            <CloseIcon/>
                                        </IconButton>
                                    </div>
                                </Flexbox>
                            </MediaQuery>
                            <Flexbox flexDirection="column" height="90%">
                                <Flexbox flexGrow={1}>
                                    <Grid spacing={24} container justify="center" alignItems="center">
                                        <Grid item>
                                            <div>
                                                <LocationSearchMobile action={this.handleClose} client={client}/>
                                                {/* <LocationSearch action={this.handleClose} client={client}/> */}
                                            </div>
                                        </Grid>    
                                    </Grid>
                                </Flexbox>
                            </Flexbox>
                            <MediaQuery query="(max-width: 768px)">
                                <Flexbox flexDirection="column" height="10%">
                                    <MediaQuery query="(max-width: 767px)">
                                        <Button onClick={this.handleClose} style={{ backgroundColor: "#EF5350", fontFamily: 'Raleway', color: "#000000", height: 100 + "%", borderRadius: 0 }}>Close</Button>
                                    </MediaQuery>
                                </Flexbox> 
                            </MediaQuery>
                        </Flexbox>
                    </div>
                </Modal>
            </React.Fragment>
        );
    }
}

DealAddModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const DealStyleWrap = withStyles(styles, { withTheme: true })(DealAddModal);

// const DealModalWrapped = withStyles(styles)(DealStyleWrap);

export default DealStyleWrap