import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { BrowserRouter as Router, Link } from 'react-router-dom';

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0
    }
});

class Admin extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 0}}>
                        <List style={{padding: 0}}>
                            <ListItem button component={Link} to='/StaticPages'>
                                <ListItemText  primary="Static Pages" secondary="Edit static pages on the website." />
                            </ListItem>
                            <ListItem button>
                                <ListItemText primary="User Admin" secondary="Manage users" />
                            </ListItem>
                            <ListItem button>
                                <ListItemText primary="Partner Mangment" secondary="Mange partner accounts and interact with partners." />
                            </ListItem>
                            <ListItem button>
                                <ListItemText primary="User Mangment" secondary="Manage users and interact with users." />
                            </ListItem>
                        </List>
                    </CardContent>
                </Card>
                
            </div>
        )
    }
}

Admin.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Admin);