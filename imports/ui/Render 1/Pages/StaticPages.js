import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { BrowserRouter as Router, Link } from 'react-router-dom';

const styles = theme => ({
    card: {
        height: "80vh",
        width: "80vw",
        margin: 0
    }
});

class StaticPages extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div style={{margin: 20}}>
                <Card className={classes.card}>
                    <CardContent style={{padding: 0}}>
                        <List style={{padding: 0}}>
                            <ListItem button component={Link} to='/AdminWelcome'>
                                <ListItemText primary="Welcome Page" secondary="Edit static pages on the website home page." />
                            </ListItem>
                            <ListItem button component={Link} to='/AdminPartnerInfo'>
                                <ListItemText primary="Partner Info Page" secondary="Edit static pages on partner info page." />
                            </ListItem>
                            <ListItem button component={Link} to='/AdminAbout'>
                                <ListItemText primary="About Page" secondary="Edit info on about page." />
                            </ListItem>
                            <ListItem button component={Link} to='/AdminFAQ'>
                                <ListItemText primary="FAQ Page" secondary="Edit info on FAQ page." />
                            </ListItem>
                        </List>
                    </CardContent>
                </Card>
                
            </div>
        )
    }
}

StaticPages.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StaticPages);