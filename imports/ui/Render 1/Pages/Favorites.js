import React, { Component } from 'react';
import { Query } from "react-apollo";
import gql from "graphql-tag";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    card: {
        borderRadius: 20 + "px"
    },
    card2: {
        borderRadius: 20 + "px"
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      marginBottom: 16,
    },
    pos: {
      marginBottom: 12,
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background:
            'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    chip: {
        margin: theme.spacing.unit,
    },
});

class Favorites extends Component {
    render() {
        const { classes, theme } = this.props;
        return (
            <div>
                <Query
                    query={gql `
                        query Favorite {
                        favorites {
                                _id
                                userId
                                StoreId
                                TimeStamp
                            }
                        }
                    `}
                    pollInterval={500}
                >
                    {({ loading, error, data }) => {
                    if (loading) return <p>Loading...</p>;
                    if (error) return <p>Error :(</p>;
                    if (data.favorites.length == 0) {
                        return (
                            <div style={{ overflow: "hidden"}}>
                                <Grid container alignItems="center" justify="center">
                                    <Grid item xs={12} style={{paddingTop: 35 + "vh"}}>
                                        <Typography variant="overline" style={{margin: 35, color: "#fff"}}>
                                            You have no favorites
                                        </Typography>
                                    </Grid>
                                </Grid>    
                            </div>
                        )
                    }
                        return data.favorites.map(({ _id, userId, StoreId}) => (
                            <div style={{padding: 15}} key={_id}>
                                <Card className={classes.card2}>
                                    <CardContent>
                                        <Typography variant="h6" >
                                            {StoreId}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </div>
                        ));
                    }}
                </Query>
            </div>
        )
    }
}

export default withStyles(styles)(Favorites);