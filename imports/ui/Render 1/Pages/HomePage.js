import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { withApollo } from 'react-apollo';
import SimpleAppBar from '../NavigationBar';
import Flexbox from 'flexbox-react';
import HomePageContent from '../HomePageContent';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/Home';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import SearchIcon from '@material-ui/icons/Search';
import AdminIcon from '@material-ui/icons/Group';
import {BrowserRouter as Router, Route, NavLink, HashRouter} from 'react-router-dom';
import FavoritesPage from './Favorites';
import FindNearPage from './FindNear';
import MediaQuery from 'react-responsive';
import AdminPage from './Admin';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import SearcbMobile from '../Search';
import ArrowUp from '@material-ui/icons/KeyboardArrowUp';
import { relative } from 'path';
import LinkDrawer from '../LinkDrawer';
import StaticPages from './StaticPages';
import AdminWelcomePage from '../Static pages and componenets/AdminWelcmePage';
import AdminPartnerInfo from '../Static pages and componenets/AdminPartnerInfo';
import AdminAbout from '../Static pages and componenets/AdminAbout';
import AdminFAQ from '../Static pages and componenets/AdminFAQ';
import FAQ from '../Static pages and componenets/FAQ';
import About from '../Static pages and componenets/About';
import PartnerInfo from '../Static pages and componenets/PartnerInfo';
import AboutAdmin from '../Static pages and componenets/AboutAdmin';
import TeamAdmin from '../Static pages and componenets/TeamAdmin';
import NonMember from '../NonMember';
import { Query } from "react-apollo";
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';

const styles = {
    root: {
        width: 100 + "vw",
    },
};

const footerStyle = {
    backgroundColor: "#E7E7E7",
    fontSize: "20px",
    color: "white",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    position: "fixed",
    left: "0",
    bottom: "0",
    width: "100%"
};
  
const phantomStyle = {
    display: "block",
    width: 100 + "vw"
};

class HomePage extends Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { client } = this.props;
        const { classes } = this.props;
        const { value } = this.state
        return (
            <div style={{ width: 100 + "vw", backgroundColor: "#37474f" }}>
            <HashRouter>
                <Flexbox flexDirection="column" minHeight="100vh">
                    <Flexbox element="header" height="60px">
                        <SimpleAppBar client={client}/>
                    </Flexbox>
                    <Query
                        query={
                            gql `
                                query User {
                                user {
                                    _id
                                    profile {
                                        role
                                    }
                                }
                                }
                            `
                        }
                        pollInterval={500}
                    >
                        {({ loading: loadingOne, error, data }) => {
                            if (loadingOne) {
                                return (
                                    <Flexbox flexDirection="column" height="100vh" width="100vw">
                                            <Grid container direction="column" alignItems="center" justify="center">
                                                <Grid item xs={12}>
                                                    <div style={{height: 100 + "%", width: 100 + "%"}}>
                                                        <CircularProgress/>
                                                    </div>
                                                </Grid>
                                            </Grid>
                                    </Flexbox>
                                )
                            }

                            if (error) return <p>Error :(</p>;
                            if (data.user.profile === null) {
                                return (
                                    <div element="content" style={{overflowX: "hidden"}}>
                                        <Route exact path="/" component={NonMember}/>
                                    </div>
                                )
                            }
                            return (
                                <div>
                                    {data.user.profile.role === "admin" ? (
                                            <div>
                                                <Flexbox display="flex" flexWrap="wrap" justifyContent="space-around" flexGrow={1}>
                                                    <div element="content" style={{overflowX: "hidden",  paddingBottom: 70}}>
                                                        <Route exact path="/" component={HomePageContent}/>
                                                        <Route path="/Favorites" component={FavoritesPage}/>
                                                        <Route path="/FindNear" component={FindNearPage}/>
                                                        <Route path="/Admin" component={AdminPage}/>
                                                        <Route path="/StaticPages" component={StaticPages}/>
                                                        <Route path="/FAQ" component={FAQ}/>
                                                        <Route path="/AdminWelcome" component={AdminWelcomePage}/>
                                                        <Route path="/AdminFAQ" component={AdminFAQ}/>
                                                        <Route path="/AdminAbout" component={AdminAbout}/>
                                                        <Route path="/AdminPartnerInfo" component={AdminPartnerInfo}/>
                                                        <Route path="/About" component={About}/>
                                                        <Route path="/BecomeAPartner" component={PartnerInfo}/>
                                                        <Route path="/AboutAdmin" component={AboutAdmin}/>
                                                        <Route path="/TeamAdmin" component={TeamAdmin}/>
                                                    </div>
                                                    <MediaQuery query="(max-width: 1366px)">
                                                        <LinkDrawer/>
                                                    </MediaQuery>
                                                </Flexbox>
                                                <MediaQuery query="(max-width: 1366px)">
                                                    <Flexbox element="footer" height="60px">
                                                        <div style={phantomStyle}>
                                                            <div style={footerStyle}>
                                                                <Grid container direction="row" justify="center" alignItems="center">
                                                                    <Grid item xs>
                                                                        <Button style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} href="#/" >
                                                                            <Grid container justify="center" alignItems="center" direction="column">
                                                                                <Grid item xs={12}>
                                                                                    <HomeIcon />
                                                                                </Grid>
                                                                                <Grid item xs={12}>
                                                                                    <Typography variant="caption">
                                                                                        Home
                                                                                    </Typography>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Button>
                                                                    </Grid>
                                                                    <Grid item xs>
                                                                        <SearcbMobile/>
                                                                    </Grid>
                                                                    {/* <BottomNavigationAction style={{paddingLeft: 0, paddingRight: 0, backgroundColor: "#ff8a65", borderRadius: 50 + "%"}} href="#/" icon={<EditIcon />}/> */}
                                                                    <Grid item xs>
                                                                        <Fab style={{ backgroundColor: "#AB47BC", color: "#FFFFFF" }}>
                                                                            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAjISURBVGhD7ZpprF5VGYXLJEOAUgYhBC1IihBEKFUDyhwkhaKIlkYZpECFkvDDACJtsAUiGH5oaCAURUVE0BgQhyqtomILGnEAGpVJBjXMqGWMDLY+z3ad0/v1nPPd794W6g9XsrL3Xu/77uE75+zp3jH/xypYsWLFdsuXLz+W9FTSI0g3iakG+oZwcnz03S6mtQ86tBn8Ivw3HatB+SmS6XHT78RoNYyBxm4Wt7UDOrAxXJxOfQm+F06A0+Dvo98uk/9dbLvAA+CX4XJMt5JsnGrfeNCBc9PBT0Wqgfwm9Nnwj+FstZhroJ9uHeDcSG886MSj9oB0j0ijAvG3WVeKrz9obF94ObwZ3lR+R0C+dSDoB8NF4cGRG8B2Qer5AVwIbWPfmNccaGM9Kr4sjb0MHyS7zLKg3DWQKfCOcErkHhD+VmxLU88/4L3wlZRtc924rj6o7PxU7Ae9ZbQNyH8m+qheLUL9ju6EL8CTKJdOk98SXpW6zyvOqwsq2hy+CBdQ7zqRC9BcC0Y9EOL2SfzxkWrYFvqPoG2v/vRMJUelsSMj1UDrOxD0D8D7wka8QJ+QbAPGp/6jIo0e1DM9le0dqQbajNjeE6kHmPbD5scr3xe5BvZN0J1AJpDvedoC/d3WD+qFdVSgAhuqvoPTIquvS3kefC02H//MmAcCYQcR85jxgrwzYc8rRLmsMaTnkTS2PAOB4APhE1YkyF8Yk7YTo11LcgzpL6DbjXfGpS/wc0fwFHwIngDnQjEvLgWUL7QdQf5xeEBMg4GAQ+C/4F/hsXB8TAWUvwWfpv71Uh7/3+Z6V2f04+Az0Gl1KJ/VmXRaXPX1iTyYYg208dB67It96lyPekD9zhb3QH+xbSP3AP0K6OtU7KT7p2MzikNAeU94JXRjOJTXxf9s/ci6TrmW/LYEtgD7tvBp+Cf8Gt9TAzi+I43MitQAtr3hK/AReD3uy0h99FvFZVjguxi+Cm+Ad6bNU2JuBXb3a/rtHqkbOHmW0Ll1uqyA3dfPne2T8IeE7BrTQMB/a+K+Dv0BfBqnwr6/NH5H4mPfDo/UDZwmxvmMSAODmJ5plLwHqAo9r91oQB1npG8TI3UDP6fWv8D74Sehs8qwsxFxe6WRAyOVgcGLw86FrwtU5/fqd3YyeX+UB+Aj5Pvvv3BYH0enwufJ16Ds4WfHuLUC+37xbd0cjgbUNdM6hwLNfdkcsu2DweDMUbbmpD+HU+HOFCeR1r+yfnBcijXQqoGcQvK2LmLfPCGtwGccrKb17clPJ50Id4NueRahiW/D4tcDHGZpJZ0bqQeY1oHnY38hfneTTIpZu5cPL2nrB3xeJvlQwmqg+YNZpz7+6u4oGh+/GraL4lem7xoIrrTPwlsiNUCcs4rB7oIdkLPNo7D+hcnvDs+En+7Df8IbElJAfWPRHoOPp27bsK1PxKUHmPxRPee72G4YuXSgmnKnRmoA28/gvbhVZ4YpiflgcRgQ+DuRLEixgHK1wy7fl22Qd8f80+LQAmzTEvP+SD2/9i6RGsDmAjZ0INWcfnRxaAFmD1++307pG6iRbxvI0amrGojf632ktxaHFmDfLTErp3UKJ0R8V6QGsJ0WHxc/H/8T0MWw8+PFdo4xwny0P0O3QL5mJ0Nfaw9v1lW9WrZhTL3jXhXYyhaf9LhIRaxGNztSA5j9yObA5+J7F8leMbcC+zj8PiLJb6FGfhZ0Oi8g+9XoPrnqY38O9nzs5DdFuwDuZJm02q68vThUQHC78Xd4OPwVPq1bDvRylwXGRhoxiN0aOhX/Bt4dWX2sFaN9LlINtLdAZzN3z/bRvt4e80oguhF8MRV5tdl6L4teXcp5JjlGkt8/5h6ge5364bBx7kZbApem2HcgAt2n9kx87Gv7G4Gxmp/7zV5ei7oW9ACtcduBdnbM2s+MXAPNe6zFKQ47EIFtanwujtQExgXwb/gNtwvdAZ9DK1J+iLQxw6B7++4lnVw53wfE+A1tk+JAA8Hs1Oz6tTBSExj/AH+c4sBI3G0pDgxitqFj9Ss8ZCBXk0zqIvZfwvsT1oSdgXekOCzw3R4uofIC8t6W1Icr8p7qZoT1aZP8m+HChBnnmX+H2DwBDgv87iqVtQGjN4kvwfpx9wN+dsAjrzvmi6DXqd+LWftZadeGz4qs/p34+k26blhHWcVJt4LHo7tIt7GabL5SKmsDdh+bmB9Jzd2uZ4GfQK83L1En9bWwws8WR0Des/xrsHwPmF17dpLm4+N34zH5csuCfDXJlOvYfjAOiv6HKxx8P630UpItSG9O+WESN2plkKROrV4B1Vc45D26Oi2uH6kBbG5bHMg1kYybZxugc23C5j3xJTqRlkW0L/Bzn/PNBJRtOamraOMgg+7FgU/gGng9FFfFrN2ncU5YVmRBvvylChhjrHXcGHMD2GbCJ9MX+9Y8h3SBgMnQX7fzigabT8yOPA89BlyBVt8IUq7+GmUHTo9s3EaU50O/k1cpfw1uGnMD+Lhh9TuaHGlkIND9fv0KdIFO+B21Hj2Jd0PYurFE71nZu4DPN6h/WYojBxU4X99jRyO1Ah+PoTunODCIGXYgto2PZ5Pu6XY4UEl1E+9r0PlXV2y/hotSrIG2B3Qmk40/PaD1HQhN+wpemT58PPLIQbzT5/xU5PpyC2zb/C2FS1KsgeZVkjOUbPwxxxjYGAiaM6LTfTXZOJi+26aBQEWHQW/erXRO5BporQMZDsYYm2INtLlp6zo4ug+8C/4iVOo346Fnz8gFlBsDoewtuh3xaCuvVYu5gHJjIJTdqtvGiPduA4PB7Aq9sPa8clDkxkDIu9v1Nt9X6sbQ/KpxPQPRFh/b6Lw/WCOggX2gZ2sf/XdJvEDzKtPty8fg96F4GNZ/rjMPvb0XN8GPQmMeSB3WZZ1eM7X+KW+Ng/a8jCsbTBsfCjRfi8/Dxr5JDX4B9lzJCjQnE/+W0vo3mdcVNOrschj9cFN5EjyE/EYxd0IffY1JrJPJ2v0Pof8NjBnzH/KJgLgpbn5+AAAAAElFTkSuQmCC"></img>
                                                                        </Fab>
                                                                    </Grid>
                                                                    <Grid item xs>
                                                                        <Button style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} href="#/FindNear">
                                                                            <Grid container justify="center" alignItems="center" direction="column">
                                                                                <Grid item xs={12}>
                                                                                    <LocationOnIcon />
                                                                                </Grid>
                                                                                <Grid item xs={12}>
                                                                                    <Typography variant="caption">
                                                                                        Find
                                                                                    </Typography>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Button>
                                                                    </Grid>
                                                                    <Grid item xs>
                                                                        <Button style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} href="#/Favorites">
                                                                            <Grid container justify="center" alignItems="center" direction="column">
                                                                                <Grid item xs={12}>
                                                                                    <FavoriteIcon />
                                                                                </Grid>
                                                                                <Grid item xs={12}>
                                                                                    <Typography variant="caption">
                                                                                        Favorites
                                                                                    </Typography>
                                                                                </Grid>
                                                                            </Grid>
                                                                        </Button>
                                                                    </Grid>
                                                                </Grid>
                                                        </div>
                                                        </div>    
                                                    </Flexbox>
                                                </MediaQuery>
                                            </div>
                                        ):(
                                            <Flexbox display="flex" flexWrap="wrap" justifyContent="space-around" flexGrow={1}>
                                                    <div element="content" style={{overflowX: "hidden"}}>
                                                        <Route exact path="/" component={NonMember}/>
                                                    </div>
                                            </Flexbox>
                                        )}
                                    </div>
                            )
                        }}
                    </Query>
                    
                </Flexbox>
            </HashRouter>    
            </div>
        )
    }
}

const dealsQuery = gql `
  query Deals {
    deals {
      _id
      name
    }
    user {
      _id
    }
  }
  
`;

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
const HomePageStyled = withStyles(styles)(HomePage);


export default graphql(dealsQuery, {
  props: ({data}) =>({ ... data })
})(withApollo(HomePageStyled));

// import React, { Component } from 'react';
// import gql from 'graphql-tag';
// import { graphql } from 'react-apollo';
// import { withApollo } from 'react-apollo';
// import SimpleAppBar from '../NavigationBar';
// import Flexbox from 'flexbox-react';
// import HomePageContent from '../HomePageContent';
// import PropTypes from 'prop-types';
// import { withStyles } from '@material-ui/core/styles';
// import BottomNavigation from '@material-ui/core/BottomNavigation';
// import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
// import HomeIcon from '@material-ui/icons/Home';
// import FavoriteIcon from '@material-ui/icons/Favorite';
// import LocationOnIcon from '@material-ui/icons/LocationOn';
// import SearchIcon from '@material-ui/icons/Search';
// import AdminIcon from '@material-ui/icons/Group';
// import {BrowserRouter as Router, Route, NavLink, HashRouter} from 'react-router-dom';
// import FavoritesPage from './Favorites';
// import FindNearPage from './FindNear';
// import MediaQuery from 'react-responsive';
// import AdminPage from './Admin';
// import EditIcon from '@material-ui/icons/Edit';
// import Button from '@material-ui/core/Button';
// import Grid from '@material-ui/core/Grid';
// import { Typography } from '@material-ui/core';
// import SearcbMobile from '../SearchMobile';
// import ArrowUp from '@material-ui/icons/KeyboardArrowUp';
// import { relative } from 'path';
// import LinkDrawer from '../LinkDrawer';
// import StaticPages from './StaticPages';
// import AdminWelcomePage from '../Static pages and componenets/AdminWelcmePage';
// import AdminPartnerInfo from '../Static pages and componenets/AdminPartnerInfo';
// import AdminAbout from '../Static pages and componenets/AdminAbout';
// import AdminFAQ from '../Static pages and componenets/AdminFAQ';

// const styles = {
//     root: {
//         width: 100 + "vw",
//     },
// };

// const footerStyle = {
//     backgroundColor: "#E7E7E7",
//     fontSize: "20px",
//     color: "white",
//     borderTop: "1px solid #E7E7E7",
//     textAlign: "center",
//     position: "fixed",
//     left: "0",
//     bottom: "0",
//     width: "100%"
// };
  
// const phantomStyle = {
//     display: "block",
//     width: 100 + "vw"
// };

// class HomePage extends Component {
//     state = {
//         value: 0,
//     };

//     handleChange = (event, value) => {
//         this.setState({ value });
//     };

//     render() {
//         const { client } = this.props;
//         const { classes } = this.props;
//         const { value } = this.state
//         return (
//             <div style={{ width: 100 + "vw", backgroundColor: "#37474f" }}>
//             <HashRouter>
//                 <Flexbox flexDirection="column" minHeight="100vh">
//                     <Flexbox element="header" height="60px">
//                         <SimpleAppBar client={client}/>
//                     </Flexbox>
//                     <Flexbox display="flex" flexWrap="wrap" justifyContent="space-around" flexGrow={1}>
//                         {/* <HomePageContent/> */}
//                         <div element="content" style={{overflowX: "hidden"}}>
//                             <Route exact path="/" component={HomePageContent}/>
//                             <Route path="/Favorites" component={FavoritesPage}/>
//                             <Route path="/FindNear" component={FindNearPage}/>
//                             <Route path="/Admin" component={AdminPage}/>
//                             <Route path="/StaticPages" component={StaticPages}/>
//                             <Route path="/AdminWelcome" component={AdminWelcomePage}/>
//                             <Route path="/AdminFAQ" component={AdminFAQ}/>
//                             <Route path="/AdminAbout" component={AdminAbout}/>
//                             <Route path="/AdminPartnerInfo" component={AdminPartnerInfo}/>
//                         </div>
                        
//                         <LinkDrawer/>
//                     </Flexbox>
//                     <MediaQuery query="(max-width: 1366px)">
//                         <Flexbox element="footer" height="60px">
//                             <div style={phantomStyle}>
//                                 <div style={footerStyle}>
//                                     <Grid container direction="row" justify="center" alignItems="center">
//                                         <Grid item xs>
//                                             <Button style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} href="#/" >
//                                                 <Grid container justify="center" alignItems="center" direction="column">
//                                                     <Grid item xs={12}>
//                                                         <HomeIcon />
//                                                     </Grid>
//                                                     <Grid item xs={12}>
//                                                         <Typography variant="caption">
//                                                             Home
//                                                         </Typography>
//                                                     </Grid>
//                                                 </Grid>
//                                             </Button>
//                                         </Grid>
//                                         <Grid item xs>
//                                             <SearcbMobile/>
//                                         </Grid>
//                                         {/* <BottomNavigationAction style={{paddingLeft: 0, paddingRight: 0, backgroundColor: "#ff8a65", borderRadius: 50 + "%"}} href="#/" icon={<EditIcon />}/> */}
//                                         <Grid item xs>
//                                             <Fab style={{ backgroundColor: "#AB47BC", color: "#FFFFFF" }}>
//                                                 <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAjISURBVGhD7ZpprF5VGYXLJEOAUgYhBC1IihBEKFUDyhwkhaKIlkYZpECFkvDDACJtsAUiGH5oaCAURUVE0BgQhyqtomILGnEAGpVJBjXMqGWMDLY+z3ad0/v1nPPd794W6g9XsrL3Xu/77uE75+zp3jH/xypYsWLFdsuXLz+W9FTSI0g3iakG+oZwcnz03S6mtQ86tBn8Ivw3HatB+SmS6XHT78RoNYyBxm4Wt7UDOrAxXJxOfQm+F06A0+Dvo98uk/9dbLvAA+CX4XJMt5JsnGrfeNCBc9PBT0Wqgfwm9Nnwj+FstZhroJ9uHeDcSG886MSj9oB0j0ijAvG3WVeKrz9obF94ObwZ3lR+R0C+dSDoB8NF4cGRG8B2Qer5AVwIbWPfmNccaGM9Kr4sjb0MHyS7zLKg3DWQKfCOcErkHhD+VmxLU88/4L3wlZRtc924rj6o7PxU7Ae9ZbQNyH8m+qheLUL9ju6EL8CTKJdOk98SXpW6zyvOqwsq2hy+CBdQ7zqRC9BcC0Y9EOL2SfzxkWrYFvqPoG2v/vRMJUelsSMj1UDrOxD0D8D7wka8QJ+QbAPGp/6jIo0e1DM9le0dqQbajNjeE6kHmPbD5scr3xe5BvZN0J1AJpDvedoC/d3WD+qFdVSgAhuqvoPTIquvS3kefC02H//MmAcCYQcR85jxgrwzYc8rRLmsMaTnkTS2PAOB4APhE1YkyF8Yk7YTo11LcgzpL6DbjXfGpS/wc0fwFHwIngDnQjEvLgWUL7QdQf5xeEBMg4GAQ+C/4F/hsXB8TAWUvwWfpv71Uh7/3+Z6V2f04+Az0Gl1KJ/VmXRaXPX1iTyYYg208dB67It96lyPekD9zhb3QH+xbSP3AP0K6OtU7KT7p2MzikNAeU94JXRjOJTXxf9s/ci6TrmW/LYEtgD7tvBp+Cf8Gt9TAzi+I43MitQAtr3hK/AReD3uy0h99FvFZVjguxi+Cm+Ad6bNU2JuBXb3a/rtHqkbOHmW0Ll1uqyA3dfPne2T8IeE7BrTQMB/a+K+Dv0BfBqnwr6/NH5H4mPfDo/UDZwmxvmMSAODmJ5plLwHqAo9r91oQB1npG8TI3UDP6fWv8D74Sehs8qwsxFxe6WRAyOVgcGLw86FrwtU5/fqd3YyeX+UB+Aj5Pvvv3BYH0enwufJ16Ds4WfHuLUC+37xbd0cjgbUNdM6hwLNfdkcsu2DweDMUbbmpD+HU+HOFCeR1r+yfnBcijXQqoGcQvK2LmLfPCGtwGccrKb17clPJ50Id4NueRahiW/D4tcDHGZpJZ0bqQeY1oHnY38hfneTTIpZu5cPL2nrB3xeJvlQwmqg+YNZpz7+6u4oGh+/GraL4lem7xoIrrTPwlsiNUCcs4rB7oIdkLPNo7D+hcnvDs+En+7Df8IbElJAfWPRHoOPp27bsK1PxKUHmPxRPee72G4YuXSgmnKnRmoA28/gvbhVZ4YpiflgcRgQ+DuRLEixgHK1wy7fl22Qd8f80+LQAmzTEvP+SD2/9i6RGsDmAjZ0INWcfnRxaAFmD1++307pG6iRbxvI0amrGojf632ktxaHFmDfLTErp3UKJ0R8V6QGsJ0WHxc/H/8T0MWw8+PFdo4xwny0P0O3QL5mJ0Nfaw9v1lW9WrZhTL3jXhXYyhaf9LhIRaxGNztSA5j9yObA5+J7F8leMbcC+zj8PiLJb6FGfhZ0Oi8g+9XoPrnqY38O9nzs5DdFuwDuZJm02q68vThUQHC78Xd4OPwVPq1bDvRylwXGRhoxiN0aOhX/Bt4dWX2sFaN9LlINtLdAZzN3z/bRvt4e80oguhF8MRV5tdl6L4teXcp5JjlGkt8/5h6ge5364bBx7kZbApem2HcgAt2n9kx87Gv7G4Gxmp/7zV5ei7oW9ACtcduBdnbM2s+MXAPNe6zFKQ47EIFtanwujtQExgXwb/gNtwvdAZ9DK1J+iLQxw6B7++4lnVw53wfE+A1tk+JAA8Hs1Oz6tTBSExj/AH+c4sBI3G0pDgxitqFj9Ss8ZCBXk0zqIvZfwvsT1oSdgXekOCzw3R4uofIC8t6W1Icr8p7qZoT1aZP8m+HChBnnmX+H2DwBDgv87iqVtQGjN4kvwfpx9wN+dsAjrzvmi6DXqd+LWftZadeGz4qs/p34+k26blhHWcVJt4LHo7tIt7GabL5SKmsDdh+bmB9Jzd2uZ4GfQK83L1En9bWwws8WR0Des/xrsHwPmF17dpLm4+N34zH5csuCfDXJlOvYfjAOiv6HKxx8P630UpItSG9O+WESN2plkKROrV4B1Vc45D26Oi2uH6kBbG5bHMg1kYybZxugc23C5j3xJTqRlkW0L/Bzn/PNBJRtOamraOMgg+7FgU/gGng9FFfFrN2ncU5YVmRBvvylChhjrHXcGHMD2GbCJ9MX+9Y8h3SBgMnQX7fzigabT8yOPA89BlyBVt8IUq7+GmUHTo9s3EaU50O/k1cpfw1uGnMD+Lhh9TuaHGlkIND9fv0KdIFO+B21Hj2Jd0PYurFE71nZu4DPN6h/WYojBxU4X99jRyO1Ah+PoTunODCIGXYgto2PZ5Pu6XY4UEl1E+9r0PlXV2y/hotSrIG2B3Qmk40/PaD1HQhN+wpemT58PPLIQbzT5/xU5PpyC2zb/C2FS1KsgeZVkjOUbPwxxxjYGAiaM6LTfTXZOJi+26aBQEWHQW/erXRO5BporQMZDsYYm2INtLlp6zo4ug+8C/4iVOo346Fnz8gFlBsDoewtuh3xaCuvVYu5gHJjIJTdqtvGiPduA4PB7Aq9sPa8clDkxkDIu9v1Nt9X6sbQ/KpxPQPRFh/b6Lw/WCOggX2gZ2sf/XdJvEDzKtPty8fg96F4GNZ/rjMPvb0XN8GPQmMeSB3WZZ1eM7X+KW+Ng/a8jCsbTBsfCjRfi8/Dxr5JDX4B9lzJCjQnE/+W0vo3mdcVNOrschj9cFN5EjyE/EYxd0IffY1JrJPJ2v0Pof8NjBnzH/KJgLgpbn5+AAAAAElFTkSuQmCC"></img>
//                                             </Fab>
//                                         </Grid>
//                                         <Grid item xs>
//                                             <Button style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} href="#/FindNear">
//                                                 <Grid container justify="center" alignItems="center" direction="column">
//                                                     <Grid item xs={12}>
//                                                         <LocationOnIcon />
//                                                     </Grid>
//                                                     <Grid item xs={12}>
//                                                         <Typography variant="caption">
//                                                             Find
//                                                         </Typography>
//                                                     </Grid>
//                                                 </Grid>
//                                             </Button>
//                                         </Grid>
//                                         <Grid item xs>
//                                             <Button style={{paddingLeft: 0, paddingRight: 0, width: 100 + "%", height: 56, borderRadius: 0, color: "#37474F"}} href="#/Favorites">
//                                                 <Grid container justify="center" alignItems="center" direction="column">
//                                                     <Grid item xs={12}>
//                                                         <FavoriteIcon />
//                                                     </Grid>
//                                                     <Grid item xs={12}>
//                                                         <Typography variant="caption">
//                                                             Favorites
//                                                         </Typography>
//                                                     </Grid>
//                                                 </Grid>
//                                             </Button>
//                                         </Grid>
//                                     </Grid>
//                                </div>
//                             </div>    
//                         </Flexbox>
//                     </MediaQuery>
//                 </Flexbox>
//             </HashRouter>    
//             </div>
//         )
//     }
// }

// const dealsQuery = gql `
//   query Deals {
//     deals {
//       _id
//       name
//     }
//     user {
//       _id
//     }
//   }
  
// `;

// HomePage.propTypes = {
//     classes: PropTypes.object.isRequired,
// };
  
// const HomePageStyled = withStyles(styles)(HomePage);


// export default graphql(dealsQuery, {
//   props: ({data}) =>({ ... data })
// })(withApollo(HomePageStyled));
