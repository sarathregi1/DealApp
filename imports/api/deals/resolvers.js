import Deals from "./deals";

export default {
    Query: {
        deals(obj, args, { userId }) {
            return Deals.find({})
        },
        dealsCatQuery(obj, { name }) {
            return Deals.find({"name": {$eq: name}},{limit: 5})
        },
        dealHomeQuery: (parent, {StorePlaceId}) => (
            Deals.find( { "StorePlaceId": { $eq:  StorePlaceId } } )
        ),
        dealCardListQuery: (parent, {StorePlaceId, address}) => (
            Deals.find( { $or: [{"StorePlaceId": { $eq:  StorePlaceId }}, {"LocationAddress": { $eq: address }}] } )
        ),
        dealCardQuery: (parent, {_id}) => (
            Deals.find( { "_id": { $eq:  _id } } )
        ),
        dealCheckQuery: (parent, {StorePlaceId, address, DealType}) => (
            Deals.find( { $or: [{$and: [ { "StorePlaceId": { $eq: StorePlaceId } }, { "name": { $eq: DealType } } ]}, {$and: [ { "LocationAddress": { $eq: address } }, { "name": { $eq: DealType } } ]}] } )
            // Deals.aggregate([  $or: ["$match": { "StorePlaceId" : StorePlaceId}, {"name" : DealType}], ["$match": [{ "LocationAddress" : address }, {"name" : DealType}]]  ])
            // Deals.find( {[
            //     { $match: { $or: [ { "StorePlaceId": { StorePlaceId } }, { "LocationAddress": { address } } ] } }
            // ]} )
        ),
    },

    Mutation: {
        createDeal(obj, { name, TypeCuisine, TimeStamp, TimeOfDay, TimeOfDayTo, FoodTitle, PriceRange, DealExpirationDate, DealExpirationTime, DealExpirationDateStart, DealExpirationTimeStart, StoreName, LocationAddress, StorePlaceId, DealDetails, HasCoupon, DayWorks, DealExp, CouponCode, CouponDetails, s, m, tu, w, th, f, sa }, { userId } ) {
            if (userId) {
                const dealId = Deals.insert({
                    name,
                    userId,
                    TypeCuisine,
                    TimeOfDay,
                    TimeOfDayTo,
                    FoodTitle,
                    PriceRange,
                    DealExpirationDate,
                    DealExpirationTime,
                    DealExpirationDateStart,
                    DealExpirationTimeStart,
                    StoreName,
                    LocationAddress,
                    StorePlaceId,
                    DealDetails,
                    HasCoupon,
                    DayWorks,
                    DealExp,
                    CouponCode,
                    CouponDetails,
                    TimeStamp,
                    s,
                    m,
                    tu,
                    w,
                    th,
                    f,
                    sa
                });
                return Deals.findOne(dealId);
            }
            throw new Error("Unauthorized");
        }
    }
};