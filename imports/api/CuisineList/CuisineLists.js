import { Mongo } from 'meteor/mongo';

const CuisineLists = new Mongo.Collection("cuisinelists");

export default CuisineLists;