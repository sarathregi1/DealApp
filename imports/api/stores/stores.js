import { Mongo } from 'meteor/mongo';

const Stores = new Mongo.Collection("stores");

export default Stores;