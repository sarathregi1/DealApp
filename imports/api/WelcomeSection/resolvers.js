import WelcomeSection from "./WelcomeSection";

export default {
    Query: {
        welcomesection(obj, args, { userId }) {
            return WelcomeSection.find({})
        },
    },

    Mutation: {
        createWelcome(obj, { slideNumber, slideImgID, visible }, { userId } ) {
            if (userId) {
                const welcomeId = WelcomeSection.insert({
                    slideNumber,
                    slideImgID,
                    visible
                });
                return WelcomeSection.findOne(welcomeId);
            }
            throw new Error("Unauthorized");
        }
    }
};