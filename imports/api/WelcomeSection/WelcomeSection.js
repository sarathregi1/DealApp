import { Mongo } from 'meteor/mongo';

const WelcomeSection = new Mongo.Collection("welcomesection");

export default WelcomeSection;