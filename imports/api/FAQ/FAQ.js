import { Mongo } from 'meteor/mongo';

const FAQ = new Mongo.Collection("faq");

export default FAQ;