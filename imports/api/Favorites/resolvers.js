import Favorites from "./Favorites";

export default {
    Query: {
        favorites(obj, args, { userId }) {
            return Favorites.find({});
        },
        favoritesHomeQuery(obj, { StoreId, userId }) {
            return Favorites.find({$and: [ { "StoreId": { $eq: StoreId } }, { "userId": { $eq: userId } } ]});
        }
    },

    Mutation: {
        createFavorite(obj, { userId, StoreId, TimeStamp }, {  } ) {
            if (userId) {
                const favoritetypelistId = Favorites.update(
                    {$and: [ { "userId": { $eq: userId } }, { "StoreId": { $eq: StoreId } } ]},
                    {
                    userId,
                    StoreId,
                    TimeStamp
                    },
                    { upsert: true }
                );
                return Favorites.findOne(favoritetypelistId);
            }
            throw new Error("Unauthorized");
        },
        removeFavorite(obj, { _id, userId }, {  } ) {
            if (userId) {
                const favoritetypelistId = Favorites.remove({ $and: [ { "userId": { $eq: userId } }, { "_id": { $eq: _id } } ] });
                return Favorites.findOne(favoritetypelistId);
            }
            throw new Error("Unauthorized");
        }
    }
};