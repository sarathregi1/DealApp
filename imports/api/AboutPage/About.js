import { Mongo } from 'meteor/mongo';

const About = new Mongo.Collection("about");

export default About;