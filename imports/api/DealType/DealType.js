import { Mongo } from 'meteor/mongo';

const DealType = new Mongo.Collection("dealtype");

export default DealType;