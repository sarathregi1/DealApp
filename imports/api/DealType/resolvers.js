import DealType from "./DealType";
import deburr from 'lodash/deburr';

export default {
    Query: {
        dealtype(obj, args, { userId }) {
            return DealType.find({
            });
        },
        dealTypeLists(parent, {name}) {
            const inputValue = deburr(name.trim()).toLowerCase();
            return DealType.find( { "name": { $regex: ".*" + inputValue + ".*" } } )
        },
        dealTypeListsWithoutInput() {
            return DealType.find( { } )
        }
    },

    Mutation: {
        createDealTypeList(obj, { name, TimeStamp }, { userId } ) {
            if (userId) {
                const dealtypelistId = DealType.insert({
                    name,
                    TimeStamp
                });
                return DealType.findOne(dealtypelistId);
            }
            throw new Error("Unauthorized");
        }
    }
};