import { Accounts } from "meteor/accounts-base";

Meteor.methods({
  getData(placeId) {
    const url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='+placeId+'&fields=name,place_id,formatted_phone_number,website,opening_hours,photo,review&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0';
    try {
      return HTTP.get(url);
    } catch (error) {
      throw new Meteor.Error('oops', 'something broke');
    }
  },
  getPicture(placeId) {
    const url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='+placeId+'&fields=photo&key=AIzaSyD-4rZgxEYqGeXupIy2AICujL5Wrc4gwA0';
    try {
      return HTTP.get(url);
    } catch (error) {
      throw new Meteor.Error('oops', 'something broke');
    }
  },
  // registerUserMethod(email, password) {
  //   try {
  //     Accounts.createUser({
  //       email: email,
  //       password: password,
  //       profile: {role: "member"}
  //     })
  //   } catch (error) {
  //     throw error
  //   }
  // }
});